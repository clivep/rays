/*********************************************************************
* common.c
*    Common methods
*********************************************************************/

#include <stdio.h>
#include <string.h>

#include "common.h"

/***********************************************************
 * newFlexString
 *   Return an empty flexString 
 ***********************************************************/

flexString *newFlexString()
{
    flexString *flex_str;

    flex_str = _ALLOC(flexString);

    flex_str->len = 10;
    flex_str->str = _NALLOC( 10, char );

    return flex_str;
}

/***********************************************************
 * setFlexString
 *   Reset the flex string 
 ***********************************************************/

void setFlexString
(
    flexString  *flex_str,
    char        *init_str
)
{

    if ( flex_str == NULL )
        return;

    *flex_str->str = '\0';
    flex_str->len = 0;

    addFlexString( flex_str, init_str );
}

/***********************************************************
 * printfFlexString
 *   Set the flexstring from the sprintf args
 ***********************************************************/
void printfFlexString
( 
    flexString *flex_str, 
    char *format, 
    ... 
)
{
    int rval;
    int new_len;

    va_list args;
    va_start( args, format );

    rval = vsnprintf( flex_str->str, flex_str->len, format, args );
    if ( rval > flex_str->len ) {

        va_start( args, format );

        new_len = flex_str->len + rval + 1;

        flex_str->str = realloc( flex_str->str, new_len );
        flex_str->len = new_len;

        vsnprintf( flex_str->str, flex_str->len, format, args );
    }

    va_end( args );
}

/***********************************************************
 * printfFlexStringCat
 *   Append to the flexstring from the sprintf args
 ***********************************************************/
void printfFlexStringCat
( 
    flexString *flex_str, 
    char *format, 
    ... 
)
{
    int rval;
    int new_len;
    flexString *tmp_str = newFlexString();

    va_list args;
    va_start( args, format );

    rval = vsnprintf( tmp_str->str, tmp_str->len, format, args );
    if ( rval > tmp_str->len ) {

        va_start( args, format );

        new_len = tmp_str->len + rval + 1;

        tmp_str->str = realloc( tmp_str->str, new_len );
        tmp_str->len = new_len;

        vsnprintf( tmp_str->str, tmp_str->len, format, args );
    }

    va_end( args );

    addFlexString( flex_str, tmp_str->str );

    releaseFlexString( tmp_str );
}

/***********************************************************
 * initFlexString
 *   Return a flexString with the given string loaded
 ***********************************************************/

flexString *initFlexString
(
    char    *init_str
)
{
    flexString *flex_str;

    flex_str = newFlexString();
    addFlexString( flex_str, init_str );

    return flex_str;
}

/***********************************************************
 * addFlexString
 *   Release memory held for the given flexString
 ***********************************************************/

void addFlexString
(
    flexString *flex_str,
    char *str
)
{
    int new_len;

    if ( flex_str == NULL )
        return;

    new_len = strlen( flex_str->str ) + strlen( str ) + 1;
    if ( new_len > flex_str->len ) {

        flex_str->str = realloc( flex_str->str, new_len );
        flex_str->len = new_len;
    }

    strcat( flex_str->str, str );
}

/***********************************************************
 * releaseFlexString
 *   Release memory held for the given flexString
 ***********************************************************/

void releaseFlexString
(
    flexString *flex_str
)
{
    if ( flex_str == NULL )
        return;

    if ( flex_str->str != NULL )
        free( flex_str->str );

    free( flex_str );
}
/***********************************************************
 * newDouble
 *   Return a pointer to a copy of the given double value
 ***********************************************************/

double *newDouble
( 
    double val
)
{
    double *pnt;
    
    pnt = (double *)malloc( sizeof(double) );
    *pnt = val;

    return pnt;
}
