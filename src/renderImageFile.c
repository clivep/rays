/*********************************************************************
* renderImageFile.c
* Definitions to support image generation to a file
*********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <unistd.h>

#include "common.h"
#include "color.h"
#include "render.h"
#include "objects.h"
#include "renderImageFile.h"

static render_file_info render_info;
static flexString *str_buf;

/***********************************************************
* renderImageFileSetupCallbacks
*    Load the renderer callbacks list
***********************************************************/

void renderImageFileSetupCallbacks
(
   list *callback_list
)
{
    render_callbacks *callbacks;

    callbacks = _ALLOC(render_callbacks);
    if ( callbacks == NULL ) {
        printf( "FATAL calloc() failed in renderImageFileSetupCallbacks\n" );
        exit( -1 );
    }

    callbacks->renderPoint = renderImageFilePoint;
    callbacks->renderEndOfLine = renderImageFileEndOfLine;
    callbacks->renderEndOfImage = renderImageFileEndOfImage;
    callbacks->renderRelease = renderImageFileRelease;

    appendToList( callback_list, "file" , callbacks );
}

/***********************************************************
* renderImageFileInit
*    Prepare the image renderer
***********************************************************/

void renderImageFileInit
(
    image_info *image,
    char *image_name,
    char *type,
    char *command
)
{
    str_buf = newFlexString();

    render_info.temp_path = strdup( "/tmp/rays-XXXXXX" );
    render_info.type = strdup( type );
    render_info.conversion_command = strdup( command );
    render_info.image_path = strdup( image_name );
    render_info.image = image;

    render_info.file_desc = mkstemp( render_info.temp_path );
    if ( render_info.file_desc < 0 ) {
        printf( "Failed to open tmp file like %s", render_info.temp_path );
        exit( -1 );
    }

    printfFlexString( str_buf,
                "%%!PS\n"
                "%%%%BoundingBox: 0 0 %f %f\n"
                "%%%%EndComments:\n"
                "\n"
                "<< /PageSize [%f %f] >> setpagedevice\n"
                "\n"
                "/buf %d string def\n",
             image->width_points, image->height_points,
             image->width_points, image->height_points,
             (int)(image->width * image->resolution)
    );

    write( render_info.file_desc, str_buf->str, strlen( str_buf->str ) );

    printfFlexString( str_buf, 

                "/chrome_x_mul %f def\n"
                "/chrome_y_mul %f def\n"

                "/chrome_scn_x %f 2 div def\n"
                "/chrome_scn_y %f 2 div def\n",
            image->chrome_multiplier[ 0 ],
            image->chrome_multiplier[ 1 ],
            image->width_points, image->height_points
    );

    write( render_info.file_desc, str_buf->str, strlen( str_buf->str ) );

    setFlexString( str_buf, 

                "/chrome_color { 1 1 1 setrgbcolor } def\n"

                "/chrome_cnv {\n"
                "    chrome_y_mul mul chrome_scn_y add\n"
                "    exch\n"
                "    chrome_x_mul mul chrome_scn_x add\n"
                "    exch\n"
                "} def\n"

                "/chrome_stroke {\n"
                "    /sz exch def\n"
                "    gsave\n"
                "    1 setlinejoin 2 setlinecap\n"
                "    gsave\n"
                "        sz 3 mul setlinewidth chrome_color stroke \n"
                "    grestore \n"
                "    sz setlinewidth\n"
                "    stroke\n"
                "    grestore \n"
                "} def\n"

                "/chrome_point {\n"
                "    gsave\n"
                "    chrome_cnv moveto\n"
                "    3 3 rmoveto\n"
                "    0 -6 rlineto\n"
                "    -6 0 rlineto\n"
                "    0 6 rlineto\n"
                "    6 0 rlineto\n"
                "    1 chrome_stroke\n"
                "    grestore\n"
                "} def\n"

                "/chrome_line {\n"
                "    /uy exch def\n"
                "    /ux exch def\n"
                "    /ly exch def\n"
                "    /lx exch def\n"
                "    gsave\n"
                "    lx ly chrome_cnv moveto\n"
                "    ux uy chrome_cnv lineto\n"
                "    1 chrome_stroke\n"
                "    grestore\n"
                "} def\n"

                "/chrome_bbox {\n"
                "    /uy exch def\n"
                "    /ux exch def\n"
                "    /ly exch def\n"
                "    /lx exch def\n"
                "    /name exch def\n"
                "    gsave\n"
                "    lx ly chrome_cnv moveto\n"
                "    lx uy chrome_cnv lineto\n"
                "    ux uy chrome_cnv lineto\n"
                "    ux ly chrome_cnv lineto\n"
                "    lx ly chrome_cnv lineto\n"
                "    1 chrome_stroke\n"
                "    lx uy chrome_cnv moveto 12 -12 rmoveto\n"
                "    chrome_color /Helvetica findfont 12 scalefont setfont name show\n"
                "    grestore\n"
                "} def\n"
    );

    write( render_info.file_desc, str_buf->str, strlen( str_buf->str ) );

    printfFlexString( str_buf,
                "gsave %f %f scale\n"
                "%f %f 8 [%f 0 0 -%f 0 %f]\n"
                "{ currentfile buf readhexstring pop }\n"
                "false 3\n"
                "colorimage\n",
             image->width_points, image->height_points,
             image->width * image->resolution, image->height * image->resolution,
             image->width * image->resolution, image->height * image->resolution,
             image->height * image->resolution
    );

    write( render_info.file_desc, str_buf->str, strlen( str_buf->str ) );
}

/***********************************************************
* renderImageFilePoint
*    Render a point
***********************************************************/

void renderImageFilePoint
(
    rgb_color *point_color
)
{
    printfFlexString( str_buf,
                "%02X%02X%02X",
            (int)( point_color->red * 255 ),
            (int)( point_color->green * 255 ),
            (int)( point_color->blue * 255 )
    );

    write( render_info.file_desc, str_buf->str, strlen( str_buf->str ) );
}

/***********************************************************
* renderImageFileEndOfLine
*    Actions after the end of a scanning line
***********************************************************/

void renderImageFileEndOfLine()
{
    /* nothing to do */
    setFlexString( str_buf, "\n" );
    write( render_info.file_desc, str_buf->str, strlen( str_buf->str ) );
}

/***********************************************************
* renderImageFileEndOfImage
*    Actions to finalise the image
***********************************************************/

void renderImageFileEndOfImage
(
    list    *objects
)
{
    list_item *object_item;
    objectChrome objectChromeMethod;
    char *object_type;
    char *chrome;
   
    setFlexString( str_buf, "grestore\n\n" );
    write( render_info.file_desc, str_buf->str, strlen( str_buf->str ) );

    if ( objects != NULL ) {

        object_item = objects->head;
        while ( object_item != NULL ) {

            object_type = object_item->label;
            objectChromeMethod = getObjectChromeMethod( object_type );

            chrome = objectChromeMethod( object_item->data,
                                                &render_info.image->chrome );

            if ( chrome != NULL )
                write( render_info.file_desc, chrome, strlen( chrome ) );

            object_item = object_item->next;
        }
    }

//    setFlexString( str_buf, "showpage\n" );
//    write( render_info.file_desc, str_buf->str, strlen( str_buf->str ) );

    close( render_info.file_desc );

    // Convert the Postscript file to the destination format
    printfFlexString( str_buf, render_info.conversion_command,
                        render_info.temp_path, render_info.image_path );

    system( str_buf->str );
}

/***********************************************************
* renderImageFileRelease
*    Releae any renderer allocated memory
***********************************************************/

void renderImageFileRelease()
{
    remove( render_info.temp_path );

    free( render_info.temp_path );
    free( render_info.conversion_command );
    free( render_info.type );
    free( render_info.image_path );
}
