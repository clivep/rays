/**********************************************************************
* traceRay.h
*
* The ray tracing function
*
**********************************************************************/

#ifndef _TRACERAY_H
#define _TRACERAY_H

#include "lists.h"
#include "geometry.h"
#include "objects.h"

int traceRay( ray *sight_ray, list *object_list, char check_screen_region );

#endif
