/**********************************************************************
* spheres.h
*
* Definitions to support sphere object for ray tracing
*
**********************************************************************/

#ifndef _SPHERES_H
#define _SPHERES_H

#include "lists.h"
#include "geometry.h"
#include "objects.h"

typedef struct {
    char                    *name;
    pnt_vr                  *centre;
    double                  radius;
    region                  *screen_region;
    double                  refractive_index;
    char                    *surface_type;
    illumination_factors    *illumination_factors;
    void                    *surface_properties;
    image_chrome            *chrome;
} sphereObject;

void sphereSetupCallbacks( list *callback_list );
void sphereReleaseSurfaceRenderers();

void sphereLoadObject( list *object_info, list *error_list,
                        list *object_list, illumination_factors *default_factors,
                        screen_info *screen );
double sphereFindIntersection( void *object, ray *sight );
void sphereProcessContact( void *object, ray *sight,
                        pnt_vr *contact, list *object_list );
char *sphereChrome( void *object, image_chrome *chrome );
region *sphereScreenRegion( void *object );
void sphereRelease( void *object );

void sphereFindNormal( void *object, pnt_vr *contact, dir_vr *normal );

/***********************************************************
* surface renderers
***********************************************************/

void sphereSetupSurfaceRenderers();

/**********/
/* planet */

typedef struct {
    rgb_color   *color;
    dir_vr      *north_pole;
} sphere_surface_planet;

void *sphereSurfacePlanetLoad( list *property_list, list *error_list );
void sphereSurfacePlanetContact( void *object,
                                pnt_vr *contact, rgb_color *contact_color );
void sphereSurfacePlanetRelease( void *renderer_info );

/***********/
/* default */

typedef struct {
    rgb_color   *color;
} sphere_surface_default;

void *sphereSurfaceDefaultLoad( list *property_list, list *error_list );
void sphereSurfaceDefaultContact( void *object,
                                pnt_vr *contact, rgb_color *contact_color );
void sphereSurfaceDefaultRelease( void *renderer_info );

#endif
