/*********************************************************************
* illumnination.c
*    Methods to support illumninations for ray tracing
*********************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#include "lists.h"
#include "config.h"
#include "common.h"
#include "objects.h"
#include "geometry.h"
#include "illumination.h"

static illumination   *illumination_info;
static list           *_factor_schemes = NULL;

/***********************************************************
* loadConfigIllumination
*    Recover the illumination config settings
*    Return non-zero on error else 0
***********************************************************/

void loadConfigIllumination
(
    list    *key_val_list,
    list    *error_list,
    list    *objects_list
)
{
    short              in_lamp = 0;
    list               *lamp_key_vals;
    list_item          *item;
    config_list_item   *config_item;
    config_list_item   *new_config_item;
    short              found;

    illumination_info = _ALLOC(illumination);
    if ( illumination_info == NULL ) {
        printf( "FATAL calloc() failed in loadConfigIllumination\n" );
        exit( -1 );
    }

    illumination_info->objects = objects_list;

    lamp_key_vals = NULL;

    found = getConfigDouble( key_val_list, NULL, "illumination",
                                 "color_shift", &illumination_info->color_shift );
    if ( ! found )
        illumination_info->color_shift = 0.0;

    found = getConfigDouble( key_val_list, NULL, "illumination",
                                 "ambience", &illumination_info->ambience );
    if ( ! found )
        illumination_info->ambience = 0.5;

    illumination_info->lamps = initialiseList( freeLamp );

    for ( item = key_val_list->head; item != NULL; item = item->next ) {

        if ( strcmp( item->label, "illumination+lamp+end" ) == 0 ) {

            if ( lamp_key_vals != NULL ) {

                loadConfigLamp( lamp_key_vals, error_list );
                freeList( lamp_key_vals );
                lamp_key_vals = NULL;
            }

            in_lamp = 0;
            continue;
        }

        if ( strcmp( item->label, "illumination+lamp+start" ) == 0 ) {

            if ( lamp_key_vals != NULL )
                freeList( lamp_key_vals );

            lamp_key_vals = initialiseList( freeConfigLamp );

            in_lamp = 1;
            continue;
        }

        if ( in_lamp ) {
            config_item = item->data;
            new_config_item = newConfigItem( config_item->line_number,
                                                         config_item->value );
            appendToList( lamp_key_vals, item->label, new_config_item );
        }
    }

    if ( illumination_info->lamps->head == NULL )
        appendToList( error_list, "No lamps defined", NULL );
}

/***********************************************************
* loadConfigLamp
*    Load and validate any lamp details from the list
***********************************************************/

void loadConfigLamp
( 
    list *key_val_list,
    list *error_list
)
{
    char key_base[] = "illumination+lamp";
    list *lamp_list = illumination_info->lamps;
    lamp *lamp_info;

    if ( ( lamp_info = _ALLOC(lamp) ) == NULL ) {
        printf( "FATAL calloc() failed in loadConfigLamp\n" );
        exit( -1 );
    }

    lamp_info->centre = newPnt( 0, 0, 0 );

    getConfigDouble( key_val_list, error_list, key_base, "radius", &lamp_info->radius );

    getConfigDouble( key_val_list, error_list, key_base, "brightness", &lamp_info->brightness );
    illumination_info->total_brightness += lamp_info->brightness;

    getConfigDouble( key_val_list, error_list, key_base, "centre+i", &lamp_info->centre->i );
    getConfigDouble( key_val_list, error_list, key_base, "centre+j", &lamp_info->centre->j );
    getConfigDouble( key_val_list, error_list, key_base, "centre+k", &lamp_info->centre->k );

    getConfigInteger( key_val_list, NULL, key_base, "sample_size", &lamp_info->sample_size );

    appendToList( lamp_list, "lamp", lamp_info );
}

/***********************************************************
* freeConfigIllumination
*    Release the illumination objects
***********************************************************/
void freeConfigIllumination()
{
    list *lamp_list;

    if ( illumination_info == NULL ) 
        return;

    lamp_list = illumination_info->lamps;
    if ( lamp_list != NULL )
        freeList( lamp_list );

    free( illumination_info );
}

/***********************************************************
* loadConfigFactorSchemes
*    Load any custom property schemes from the config file
***********************************************************/

void loadConfigFactorSchemes
(
    list    *key_val_list,
    list    *error_list
)
{
    list_item *item;
    illumination_factors *factors = NULL;
    config_list_item *config_item;
    char *scheme_name;

    _factor_schemes = initialiseList( free );

    for ( item = key_val_list->head; item != NULL; item = item->next ) {

        if ( strncmp( item->label, "factor-scheme-defs", 18 ) )
            continue;

        config_item = (config_list_item *)item->data;

        if ( strcmp( item->label, "factor-scheme-defs+name" ) == 0 ) {

            if ( factors != NULL ) 
               appendToList( _factor_schemes, scheme_name, factors );

            if ( ( factors = _ALLOC(illumination_factors) ) == NULL ) {
                printf( "FATAL calloc() failed in loadConfigFactorSchemes\n" );
                exit( -1 );
            }

            scheme_name = (char *)config_item->value;

            continue;
        }

        if ( strcmp( &item->label[ 23 ], "+diffusion_index" ) == 0 ) {

            factors->diffusion_index = atof( config_item->value );
        }
        else if ( strcmp( &item->label[ 23 ], "+reflective_index" ) == 0 ) {

            factors->reflective_index = atof( config_item->value );
        }
        else if ( strcmp( &item->label[ 23 ], "+specular_exponent" ) == 0 ) {

            factors->specular_exponent = atof( config_item->value );
        }
        else if ( strcmp( &item->label[ 23 ], "+specular_index" ) == 0 ) {

            factors->specular_index = atof( config_item->value );
        }
    }

    if ( factors != NULL )
        appendToList( _factor_schemes, scheme_name, factors );
}

/***********************************************************
* findFactorScheme
*    Locate and return the given factor scheme name in the
*    global list
***********************************************************/
illumination_factors *findFactorScheme
(
    char *name
)
{
    illumination_factors *factors = NULL;
    list_item *item;

    if ( name == NULL )
        return NULL;

    for ( item = _factor_schemes->head; item != NULL; item = item->next ) {
    
        if ( strcmp( item->label, name ) == 0 ) {
            factors = (illumination_factors *)item->data;
            break;
        }
    }

    return factors;
}

/***********************************************************
* copyIlluminationFactors
*    Create a new illuinnation factors object from the given
*    source
***********************************************************/
illumination_factors *copyIlluminationFactors
(
    illumination_factors *src
)
{
    illumination_factors *new_factors;

    if ( ( new_factors = _ALLOC(illumination_factors) ) == NULL ) {
        printf( "FATAL calloc() failed in copyIlluminationFactors\n" );
        exit( -1 );
    }

    new_factors->diffusion_index = src->diffusion_index;
    new_factors->reflective_index = src->reflective_index;
    new_factors->specular_index = src->specular_index;
    new_factors->specular_exponent = src->specular_exponent;

    return new_factors;
}

/***********************************************************
* newIlluminationFactors
*    Allocate, populate and return a new illumination object
***********************************************************/
illumination_factors *newIlluminationFactors
(
    list                    *object_key_vals,
    illumination_factors    *default_factors
)
{
    list_item *item;
    config_list_item *config_item;
    double d;
    char *scheme_name;
    char str_buf[ 128 ];
    char key_base[] = "objects+object";
    illumination_factors *factor_scheme;
    illumination_factors *factors;

    factors = copyIlluminationFactors( default_factors );

    if ( ( item = findKeyInList( object_key_vals, key_base, "scheme-name" ) ) != NULL ) {

        config_item = (config_list_item *)item->data;
        scheme_name = config_item->value;

        if ( ( factor_scheme = findFactorScheme( scheme_name ) ) != NULL ) {

            factors = copyIlluminationFactors( factor_scheme );
        }
        else {

            sprintf( str_buf, "ln: %d Unknown property scheme definition: %s",
                                        config_item->line_number, scheme_name );
        }
    }

    if ( getConfigDouble( object_key_vals, NULL, key_base, "diffusion_index", &d ) )
        factors->diffusion_index = d;

    if ( getConfigDouble( object_key_vals, NULL, key_base, "reflective_index", &d ) )
        factors->reflective_index = d;

    if ( getConfigDouble( object_key_vals, NULL, key_base, "specular_index", &d ) )
        factors->specular_index = d;

    if ( getConfigDouble( object_key_vals, NULL, key_base, "specular_exponent", &d ) )
        factors->specular_exponent = d;

    if ( getConfigDouble( object_key_vals, NULL, key_base, "color_scramble", &d ) )
        factors->color_scramble = d;

    return factors;
}

/***********************************************************
* freeLamp
*    Release a lamp object
***********************************************************/
void freeLamp
(
    void *item
)
{
    lamp *lamp_info = (lamp *)item;

    if ( lamp_info == NULL )
        return;

    if ( lamp_info->centre != NULL)
        free( lamp_info->centre );

    free( lamp_info );
}

/***********************************************************
* freeConfigLamp
*    Release a config lamp object
***********************************************************/
void freeConfigLamp
(
    void *item
)
{
    config_list_item *config_item = (config_list_item *)item;

    if ( config_item != NULL ) {

        if ( config_item->value != NULL )
            free( config_item->value );

        free( config_item );
    }
}

/***********************************************************
* setContactPointColor
*    Find the perceived color of the given point
***********************************************************/

void setContactPointColor
( 
    void                    *source_object,
    pnt_vr                  *contact_pnt,
    rgb_color               *contact_color,
    ray                     *sight,
    dir_vr                  *normal,
    illumination_factors    *illumination_factors
)
{
    list *lamp_list = illumination_info->lamps;
    list_item *lamp_item;
    lamp *lamp_info;
    dir_vr lamp_dir;
    dir_vr reflected_dir;
    rgb_color viewed_color;
    double shadow_factor;
    double diffuse_factor;
    double specular_factor;
    double specular_exponent;
    double d;

// TODO consider lamp radius
  
    scrambleColor( contact_color, illumination_factors->color_scramble );

    // Start with the basic ambient color
    setColor( &viewed_color,
        illumination_info->ambience * contact_color->red,
        illumination_info->ambience * contact_color->green,
        illumination_info->ambience * contact_color->blue
    );

    shadow_factor = findShadowFactor( source_object,
                                        contact_pnt, contact_color );

    lamp_item = lamp_list->head;

    while ( lamp_item != NULL ) {

        lamp_info = (lamp *)lamp_item->data;

        setDir( &lamp_dir,
                lamp_info->centre->i - contact_pnt->i,
                lamp_info->centre->j - contact_pnt->j,
                lamp_info->centre->k - contact_pnt->k
        );

        normalise( &lamp_dir );

        diffuse_factor = sight->strength *
                        illumination_factors->diffusion_index *
                        ( lamp_info->brightness / illumination_info->total_brightness ) *
                        dot( normal, &lamp_dir );

        if ( diffuse_factor > 0 ) {

            addColor( &viewed_color,
                shadow_factor * diffuse_factor * contact_color->red,
                shadow_factor * diffuse_factor * contact_color->green,
                shadow_factor * diffuse_factor * contact_color->blue
            );
        }

        if ( illumination_factors->specular_index > 0 ) {

            d = 2 * dot( sight->dir, normal );
            setDir( &reflected_dir,
                    sight->dir->i - d * normal->i,
                    sight->dir->j - d * normal->j,
                    sight->dir->k - d * normal->k
            );
            normalise( &reflected_dir );

            specular_exponent = illumination_factors->specular_exponent;
            if ( specular_exponent < 1.0 )
                specular_exponent = 1.0;

            specular_factor = illumination_factors->specular_index *
                            pow( dot( &lamp_dir, &reflected_dir ), specular_exponent );

            if ( specular_factor > 0 ) {

                addColor( &viewed_color, specular_factor, specular_factor, specular_factor );
            }
        }

        lamp_item = lamp_item->next;
    }

    addColor( sight->color,
                sight->strength * viewed_color.red,
                sight->strength * viewed_color.green,
                sight->strength * viewed_color.blue
    );
}

/***********************************************************
* findShadowColor
*    Find the effect of any shadows.
***********************************************************/

double findShadowFactor
( 
    void        *source_object,
    pnt_vr      *contact_pnt,
    rgb_color   *contact_color
)
{
    list_item           *object_item;
    list                *lamp_list = illumination_info->lamps;
    list_item           *lamp_item;
    lamp                *lamp_info;
	objectIntersection  intersectionMethod;
    dir_vr              *lamp_dir;
    char                *object_type;
    ray                 *check_ray;
    double              distance;
    double              shadow_factor;
    double              factor;
    short               see_lamp_samples;
    short               see_lamp;
    short               lamp_sample_size;
    short               lamp_samples, x, y;
    double              dx, dy;
    dir_vr              *axis1;
    dir_vr              *axis2;
    dir_vr              *plane_dir;
    pnt_vr              *lamp_pnt;
    double              rad;

    shadow_factor = 0.0;    // in shadow

    lamp_item = lamp_list->head;

    while ( lamp_item != NULL ) {

        lamp_info = (lamp *)lamp_item->data;

        lamp_dir = newDir( 
             lamp_info->centre->i - contact_pnt->i,
             lamp_info->centre->j - contact_pnt->j,
             lamp_info->centre->k - contact_pnt->k
        );
        normalise( lamp_dir );

        if ( ( lamp_dir->i == 0 ) && ( lamp_dir->j == 0 ) )
            plane_dir = newDir( -1, 0, 0 );
        else
            plane_dir = newDir( 0, 0, 1 );
        
        axis1 = cross( plane_dir, lamp_dir );
        normalise( axis1 );

        axis2 = cross( axis1, lamp_dir );
        normalise( axis2 );
        
        free( plane_dir );
        free( lamp_dir );

        // there are 13 points of visibility to check - 1 at the 
        // lamps' centre, 4 in the inner ring and 8 in the outer

        see_lamp_samples = 0;
        lamp_samples = 0;

        rad = lamp_info->radius;
        lamp_sample_size = ( rad > 1 ) ? lamp_info->sample_size : 1;

        for ( x = 0; x < lamp_sample_size; x++ ) {

            for ( y = 0; y < lamp_sample_size; y++ ) {

                if ( ( x * x ) + ( y * y ) > ( lamp_sample_size * lamp_sample_size / 4.0 ) )
                    continue;

                lamp_samples++;

                dx = ( rad > 1 ) ? ( 2.0 * x / lamp_sample_size - 1.0 ) : 0.0;
                dy = ( rad > 1 ) ? ( 2.0 * y / lamp_sample_size - 1.0 ) : 0.0;

                lamp_pnt = newPnt( 
                            lamp_info->centre->i + rad * ( axis1->i * dx + axis2->i * dy ),
                            lamp_info->centre->j + rad * ( axis1->j * dx + axis2->j * dy ),
                            lamp_info->centre->k + rad * ( axis1->k * dx + axis2->k * dy )
                       );

                lamp_dir = newDir( 
                    lamp_pnt->i - contact_pnt->i,
                    lamp_pnt->j - contact_pnt->j,
                    lamp_pnt->k - contact_pnt->k
                );
                normalise( lamp_dir );

                free( lamp_pnt );

                check_ray = newRay( copyPnt( contact_pnt ), lamp_dir, newColor( 0, 0, 0 ), NULL );
                movePnt( check_ray->src, _SMALL_DIST, lamp_dir );

                see_lamp = 1;

                object_item = illumination_info->objects->head;
                while ( object_item != NULL ) {

                    object_type = object_item->label;

     //               if ( object_item->data != source_object ) {

                        intersectionMethod = getObjectIntersetionMethod( object_type );

                        distance = intersectionMethod( object_item->data, check_ray );

                        if ( distance > 0 ) {
                            see_lamp = 0;
                            break;
                        }
     //               }

                    object_item = object_item->next;
                }
    
                see_lamp_samples += see_lamp;
    
                rayRelease( check_ray );
            }
        }

        if ( see_lamp_samples > 0 ) {
            factor = ( lamp_info->brightness / illumination_info->total_brightness );
            factor *= (double)see_lamp_samples / lamp_samples;
            shadow_factor += factor;
        }

        free( axis1 );
        free( axis2 );

        lamp_item = lamp_item->next;
    }

    return shadow_factor;
}

/***********************************************************
* colorShift
*    Adjust he given color randomly 
***********************************************************/

void colorShift
( 
    rgb_color *color
)
{
    double cs = illumination_info->color_shift;
    double val = cs * _RND;

    // First modify the base color
    color->red = colorRange( color->red + val );
    color->green = colorRange( color->green + val );
    color->blue = colorRange( color->blue + val );

    // Now slightly scramble the color
    color->red = colorRange( color->red + ( cs * _RND ) );   
    color->green = colorRange( color->green + ( cs * _RND ) );
    color->blue = colorRange( color->blue + ( cs * _RND ) );
}
