/*********************************************************************
* objects.c
*    Methods to support generic objects for ray tracing
*********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "common.h"
#include "objects.h"
#include "config.h"

#include "cylinders.h"
#include "planes.h"
#include "rings.h"
#include "spheres.h"
#include "triangles.h"

static list *object_callback_list;
static char str_buf[ 1024 ];

/***********************************************************
* setupObjectCallbacks
*    Load all of the shape callbacks list
***********************************************************/

void setupObjectCallbacks()
{
    object_callback_list = initialiseList( free );

    cylinderSetupCallbacks( object_callback_list );
    planeSetupCallbacks( object_callback_list );
    ringSetupCallbacks( object_callback_list );
    sphereSetupCallbacks( object_callback_list );
    triangleSetupCallbacks( object_callback_list );
}

/***********************************************************
* releaseObjectCallbacks
*    Free all callbakcs made for the object
***********************************************************/

void releaseObjectCallbacks()
{
    freeList( object_callback_list );

    cylinderReleaseSurfaceRenderers();
    planeReleaseSurfaceRenderers();
    ringReleaseSurfaceRenderers();
    sphereReleaseSurfaceRenderers();
    triangleReleaseSurfaceRenderers();
}

/***********************************************************
* getObjectLoadMethod
*    Return the load method for the given object type
***********************************************************/

objectLoad getObjectLoadMethod
(
    char *object_type
)
{
    list_item *callback_item;
    object_callbacks *callbacks;

    callback_item = findInList( object_callback_list, object_type );

    if ( callback_item == NULL )
        return (objectLoad)NULL;

    callbacks = callback_item->data;

    return callbacks->objectLoad;
}

/***********************************************************
* getObjectChromeMethod
*    Return the chrome method for the given object type
***********************************************************/

objectChrome getObjectChromeMethod
(
    char *object_type
)
{
    list_item *callback_item;
    object_callbacks *callbacks;

    callback_item = findInList( object_callback_list, object_type );

    if ( callback_item == NULL )
        return (objectChrome)NULL;

    callbacks = callback_item->data;

    return callbacks->objectChrome;
}

/***********************************************************
* getObjectScreenRegionMethod
*    Return the screen region method for the given object type
***********************************************************/

objectScreenRegion getObjectScreenRegionMethod
(
    char *object_type
)
{
    list_item *callback_item;
    object_callbacks *callbacks;

    callback_item = findInList( object_callback_list, object_type );

    if ( callback_item == NULL )
        return (objectScreenRegion)NULL;

    callbacks = callback_item->data;

    return callbacks->objectScreenRegion;
}

/***********************************************************
* getObjectIntersetionMethod
*    Return the intersetion method for the given object type
***********************************************************/

objectIntersection getObjectIntersetionMethod
(
    char *object_type
)
{
    list_item *callback_item;
    object_callbacks *callbacks;

    callback_item = findInList( object_callback_list, object_type );

    if ( callback_item == NULL )
        return (objectIntersection)NULL;

    callbacks = callback_item->data;

    return callbacks->objectIntersection;
}

/***********************************************************
* getObjectContactMethod
*    Return the contact method for the given object type
***********************************************************/

objectContact getObjectContactMethod
(
    char *object_type
)
{
    list_item *callback_item;
    object_callbacks *callbacks;

    callback_item = findInList( object_callback_list, object_type );

    if ( callback_item == NULL )
        return (objectContact)NULL;

    callbacks = callback_item->data;

    return callbacks->objectContact;
}

/***********************************************************
* getObjectReleaseMethod
*    Return the release method for the given object type
***********************************************************/

objectRelease getObjectReleaseMethod
(
    char *object_type
)
{
    list_item *callback_item;
    object_callbacks *callbacks;

    callback_item = findInList( object_callback_list, object_type );

    if ( callback_item == NULL )
        return (objectRelease)NULL;

    callbacks = callback_item->data;

    return callbacks->objectRelease;
}

/***********************************************************
* newSurfaceRenderer
*    Create a new surfaceRenderer object
*    Return a pointer to the object or NULL on error
***********************************************************/

renderer_callbacks *newSurfaceRenderer
(
    rendererLoad    loader_method,
    rendererContact contact_method,
    rendererRelease release_method 
)
{
    renderer_callbacks *renderer = _ALLOC( renderer_callbacks );
    if ( renderer == NULL ) {
        printf( "FATAL calloc() failed in newSurfaceRenderer\n" );
        exit( -1 );
    }

    renderer->rendererLoad = loader_method;
    renderer->rendererContact = contact_method;
    renderer->rendererRelease = release_method;

    return renderer;
}

/***********************************************************
* getRendererLoadMethod
*    Return the load method for the given renderer type
***********************************************************/

rendererLoad getRendererLoadMethod
(
    list *renderer_callback_list,
    char *renderer
)
{
    list_item *callback_item;
    renderer_callbacks *callbacks;

    callback_item = findInList( renderer_callback_list, renderer );

    if ( callback_item == NULL )
        return (rendererLoad)NULL;

    callbacks = callback_item->data;

    return callbacks->rendererLoad;
}

/***********************************************************
* getRendererContactMethod
*    Return the contact method for the given renderer type
***********************************************************/

rendererContact getRendererContactMethod
(
    list *renderer_callback_list,
    char *renderer
)
{
    list_item *callback_item;
    renderer_callbacks *callbacks;

    callback_item = findInList( renderer_callback_list, renderer );

    if ( callback_item == NULL )
        return (rendererContact)NULL;

    callbacks = callback_item->data;

    return callbacks->rendererContact;
}

/***********************************************************
* getRendererReleaseMethod
*    Return the release method for the given renderer type
***********************************************************/

rendererRelease getRendererReleaseMethod
(
    list *renderer_callback_list,
    char *renderer
)
{
    list_item *callback_item;
    renderer_callbacks *callbacks;

    callback_item = findInList( renderer_callback_list, renderer );

    if ( callback_item == NULL )
        return (rendererRelease)NULL;

    callbacks = callback_item->data;

    return callbacks->rendererRelease;
}

/***********************************************************
* loadConfigImage
*    Recover the image values from the config list
*    Return non-zero on error else 0
***********************************************************/

void loadConfigImage
(
    list *key_val_list,
    list *error_list,
    image_info *image
)
{
    config_list_item *config_item;
    char key_base[] = "image";
    char *units_str, *units;
    double multiplier = 1.0;

    getConfigDouble( key_val_list, error_list, key_base, "height", &image->height );
    getConfigDouble( key_val_list, error_list, key_base, "width", &image->width );
    getConfigDouble( key_val_list, error_list, key_base, "resolution", &image->resolution );
    if (
        ( image->resolution < 1 ) ||
        ( (int)image->resolution != image->resolution ) 
    ) {

        config_item = (config_list_item * )key_val_list->head->data;
        sprintf( str_buf, "ln: %d Resolution needs to be an integer (>0)",
                                config_item->line_number );

        appendToList( error_list, str_buf, NULL );
    }

    units_str = getConfigValue( key_val_list, error_list, key_base, "units" );

    if ( units_str == NULL )
        units_str = "PIXEL";

    if ( ( units = strstr( _UNITS, units_str ) ) == NULL ) {

        config_item = (config_list_item * )key_val_list->head->data;
        sprintf( str_buf, "ln: %d Missing %s+%s value for image",
                                config_item->line_number, key_base, "units" );

        appendToList( error_list, str_buf, NULL );
    }
    else {

        multiplier = getUnitsMultiplier( units );

        image->units = units;

        image->height_points = image->height * multiplier;
        image->width_points = image->width * multiplier;
    }

    loadConfigChrome( "image", key_val_list, error_list, &image->chrome );
}

/***********************************************************
* loadConfigChrome
*    Load the chrome config settings
***********************************************************/

void loadConfigChrome
(
    char *key_base,
    list *key_val_list,
    list *error_list,
    image_chrome *chrome
)
{
    char *str;

    str = getConfigValue( key_val_list, NULL, key_base, "chrome+bounding_box" );
    chrome->bounding_box =
                        ( str && ( strcasecmp( str, "All" ) == 0 ) ) ? 'A' :
                        ( str && ( strcasecmp( str, "On" ) == 0 ) ) ? 'Y' : 'N';
    chrome->bbox_color = 
                getConfigColor( key_val_list, NULL, key_base, "chrome+bounding_box" );

    str = getConfigValue( key_val_list, NULL, key_base, "chrome+construction" );
    chrome->construction =
                        ( str && ( strcasecmp( str, "All" ) == 0 ) ) ? 'A' :
                        ( str && ( strcasecmp( str, "On" ) == 0 ) ) ? 'Y' : 'N';
    chrome->construction_color = 
                getConfigColor( key_val_list, NULL, key_base, "chrome+construction" );

    str = getConfigValue( key_val_list, NULL, key_base, "chrome+normal" );
    chrome->normal =
                        ( str && ( strcasecmp( str, "All" ) == 0 ) ) ? 'A' :
                        ( str && ( strcasecmp( str, "On" ) == 0 ) ) ? 'Y' : 'N';
    chrome->normal_color = 
                getConfigColor( key_val_list, NULL, key_base, "chrome+normal" );
}

/***********************************************************
* getUnitsMultiplier
*    Return the multiplier from the given unit to points
***********************************************************/

double getUnitsMultiplier
(
    char *units
)
{
    double multiplier = 1;

    if ( strncmp( units, "INCHES", 2 ) == 0 )
        multiplier = 72;
    else if ( strncmp( units, "CM", 2 ) == 0 )
        multiplier = 72 / 2.54;
    else if ( strncmp( units, "MM", 2 ) == 0 )
        multiplier = 72 / 25.4;

    return multiplier;
}

/***********************************************************
* loadConfigScreen
*    Recover the screen values from the config list
*    Return non-zero on error else 0
***********************************************************/

short loadConfigScreen
(
    list *key_val_list,
    list *error_list,
    screen_info *screen
)
{
    short retval = 0;
    pnt_vr *centre = screen->centre;
    pnt_vr *offset = screen->offset;
    char key_base[] = "screen";

    getConfigDouble( key_val_list, error_list, key_base, "centre+i", &centre->i );
    getConfigDouble( key_val_list, error_list, key_base, "centre+j", &centre->j );
    getConfigDouble( key_val_list, error_list, key_base, "centre+k", &centre->k );
    getConfigDouble( key_val_list, error_list, key_base, "eye_dist", &screen->eye_dist );

    getConfigInteger( key_val_list, error_list, key_base, "height", &screen->height );
    getConfigInteger( key_val_list, error_list, key_base, "width", &screen->width );

    // The optional offset params
    getConfigDouble( key_val_list, NULL, key_base, "offset+i", &offset->i );
    getConfigDouble( key_val_list, NULL, key_base, "offset+j", &offset->j );
    getConfigDouble( key_val_list, NULL, key_base, "offset+k", &offset->k );

    return retval;
}
