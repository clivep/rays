/*********************************************************************
* geometry.c
*    Methods to support geometric operations
*********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "common.h"
#include "geometry.h"
 
extern coord *pntToScreenCoord( pnt_vr *pnt );

/***********************************************************
* setDir
*    Populate the velocities of the given direction
***********************************************************/
 
void setDir
(
    dir_vr *dir,
    double i,
    double j,
    double k
)
{
    dir->i = i;
    dir->j = j;
    dir->k = k;
}
 
/***********************************************************
* setPnt
*    Populate the given point with the given coordinates
***********************************************************/
 
void setPnt
(
    pnt_vr *pt,
    double i,
    double j,
    double k
)
{
    pt->i = i;
    pt->j = j;
    pt->k = k;
}
 
/***********************************************************
* setPntFromPnt
*    Populate the coordinates of the given point
***********************************************************/
 
void setPntFromPnt
(
    pnt_vr *dst,
    pnt_vr *src
)
{
    dst->i = src->i;
    dst->j = src->j;
    dst->k = src->k;
}
 
/***********************************************************
* normalise
*    Normalise the given vector
***********************************************************/
 
void normalise
(
    dir_vr *dir
)
{
    double s = _SIZEVR( dir );
  
    dir->i = dir->i / s;
    dir->j = dir->j / s;
    dir->k = dir->k / s;
}
 
/***********************************************************
* dot
*    Return the dot product of the 2 given directions
***********************************************************/
 
double dot
(
    dir_vr *a,
    dir_vr *b
)
{
    return (a->i * b->i) + (a->j * b->j) + (a->k * b->k);
}
 
/***********************************************************
* cross
*    Return the cross product of the 2 given directions
*    The caller to free the returned vector
***********************************************************/
 
dir_vr *cross
(
    dir_vr *a,
    dir_vr *b
)
{
    dir_vr *c = newDir( 0, 0, 0 );
 
    if ( c != NULL ) {
  
        c->i = ( a->j * b->k ) - ( a->k * b->j );
        c->j = ( a->k * b->i ) - ( a->i * b->k );
        c->k = ( a->i * b->j ) - ( a->j * b->i );
    }
 
    return c;
}
 
/***********************************************************
* newRay
*    Create a ray object from the given point and direction
***********************************************************/
 
ray *newRay
(
    pnt_vr      *pnt,
    dir_vr      *dir,
    rgb_color   *color,
    coord       *screen_coord
)
{
    ray *new_ray = _ALLOC( ray );
    if ( new_ray == NULL ) {
        printf( "FATAL: malloc() failed in newRay\n" );
        exit( -1 );
    }
 
    new_ray->src = pnt;
    new_ray->dir = dir;
    new_ray->color = color;
    new_ray->screen_coord = screen_coord;

    new_ray->strength = 1.0;

    new_ray->contact = 0;

    new_ray->refractive_indices = initialiseList( free );
    appendDoubleToList( new_ray->refractive_indices, 1.0 );

    return new_ray;
}
 
/***********************************************************
* rayGetPoint
*    Find the point the given distance along the ray
***********************************************************/
 
void rayGetPoint
(
    ray    *sight, 
    double distance,
    pnt_vr *point
)
{
    point->i = sight->src->i + distance * sight->dir->i;
    point->j = sight->src->j + distance * sight->dir->j;
    point->k = sight->src->k + distance * sight->dir->k;
}

/***********************************************************
* rayRelease
*    Release the ray object
***********************************************************/
 
void rayRelease
(
   ray   *sight_ray
)
{
   if ( sight_ray == NULL )
      return;

   if ( sight_ray->src != NULL )
      free( sight_ray->src );

   if ( sight_ray->screen_coord != NULL )
      free( sight_ray->screen_coord );

   if ( sight_ray->dir != NULL )
      free( sight_ray->dir );

   if ( sight_ray->color != NULL )
      free( sight_ray->color );

   if ( sight_ray->refractive_indices != NULL )
    freeList( sight_ray->refractive_indices );

   free( sight_ray );
}

/***********************************************************
* newRegion
*    Create a new region and populate the boudaries
***********************************************************/
 
region *newRegion
(
    double   lx,
    double   ly,
    double   ux,
    double   uy
)
{
    region *rgn;

    if ( ( rgn = _ALLOC(region) ) == NULL ) {
        printf( "FATAL: calloc() failed in newRegion\n" );
        exit( -1 );
    }
 
    rgn->lx = lx;
    rgn->ly = ly;
    rgn->ux = ux;
    rgn->uy = uy;
 
    return rgn;
}

/***********************************************************
* updateRegion
*    Extend (if required) the region to include the point
***********************************************************/
 
void updateRegionWithPnt
(
    region  *rgn,
    pnt_vr  *pnt
)
{ 
    coord *screen_pnt;
    
    screen_pnt = pntToScreenCoord( pnt );
//TODO : screen_pnt is NULL if not screen-plane intersection

    rgn->lx = _MIN( rgn->lx, screen_pnt->x );
    rgn->ly = _MIN( rgn->ly, screen_pnt->y );
    rgn->ux = _MAX( rgn->ux, screen_pnt->x );
    rgn->uy = _MAX( rgn->uy, screen_pnt->y );

    free( screen_pnt );
}

/***********************************************************
* extendRegionWithOffsets
*    Extend (if required) the region to include the point
*    surrounded by the offsets
***********************************************************/
 
void extendRegionWithOffsets
( 
    region *rgn, 
    double x, 
    double y,
    double x_off, 
    double y_off 
)
{
    rgn->lx = _MIN( rgn->lx, ( x - _MOD( x_off ) ) );
    rgn->ly = _MIN( rgn->ly, ( y - _MOD( y_off ) ) );
    rgn->ux = _MAX( rgn->ux, ( x + _MOD( x_off ) ) );
    rgn->uy = _MAX( rgn->uy, ( y + _MOD( y_off ) ) );
}

/***********************************************************
* setupRegion
*    Setup the region from the gievn point
***********************************************************/
 
void setupRegionWithPnt
(
    region  *rgn,
    pnt_vr  *pnt
)
{ 
    coord *screen_pnt;
    
    screen_pnt = pntToScreenCoord( pnt );

    rgn->lx = screen_pnt->x;
    rgn->ux = screen_pnt->x;

    rgn->ly = screen_pnt->y;
    rgn->uy = screen_pnt->y;

    free( screen_pnt );
}

/***********************************************************
* newPnt
*    Create a new vector and populate the coordinates
***********************************************************/
 
pnt_vr *newPnt
(
    double   i,
    double   j,
    double   k
)
{
    pnt_vr *pnt;

    if ( ( pnt = _ALLOC(pnt_vr) ) == NULL ) {
        printf( "FATAL: malloc() failed in newPnt\n" );
        exit( -1 );
    }
 
    pnt->i = i;
    pnt->j = j;
    pnt->k = k;
 
    return pnt;
}
 
/***********************************************************
* newCoord
*    Create a coord object and populate the coordinates
***********************************************************/
 
coord *newCoord
(
    double   x,
    double   y
)
{
    coord *new_coord;

    if ( ( new_coord = _ALLOC(coord) ) == NULL ) {
        printf( "FATAL: malloc() failed in newCoord\n" );
        exit( -1 );
    }
 
    new_coord->x = x;
    new_coord->y = y;
 
    return new_coord;
}
 
/***********************************************************
* dirFromPnt
*    Create a new direction from the given point
***********************************************************/
 
dir_vr *dirFromPnt
(
    pnt_vr   *pnt
)
{
    return newDir( pnt->i, pnt->j, pnt->k );
}
 
/***********************************************************
* copyPnt
*    Duplicate the given point
***********************************************************/
 
pnt_vr *copyPnt
(
    pnt_vr   *pnt
)
{
    return newPnt( pnt->i, pnt->j, pnt->k );
}
 
/***********************************************************
* copyDir
*    Duplicate the given velocity
***********************************************************/
 
dir_vr *copyDir
(
    dir_vr   *dir
)
{
    return newDir( dir->i, dir->j, dir->k );
}
 
/***********************************************************
* movePnt
*    Move the given point along the given direction
***********************************************************/
 
void movePnt
(
    pnt_vr   *pnt,
    double   dist,
    dir_vr   *dir
)
{
    pnt->i += dist * dir->i;
    pnt->j += dist * dir->j;
    pnt->k += dist * dir->k;
}

/***********************************************************
* addPnt
*    Add the second point to the first
***********************************************************/
 
void addPnt
(
    pnt_vr   *pnt_1,
    pnt_vr   *pnt_2
)
{
    pnt_1->i += pnt_2->i;
    pnt_1->j += pnt_2->j;
    pnt_1->k += pnt_2->k;
}

/***********************************************************
* subPnt
*    Subtract the second point from the first
***********************************************************/
 
void subPnt
(
    pnt_vr   *pnt_1,
    pnt_vr   *pnt_2
)
{
    pnt_1->i -= pnt_2->i;
    pnt_1->j -= pnt_2->j;
    pnt_1->k -= pnt_2->k;
}
 
/***********************************************************
* newDir
*    Create a new vector and populate the coordinates
***********************************************************/
 
dir_vr *newDir
(
    double   i,
    double   j,
    double   k
)
{
    dir_vr *dir;

    if ( ( dir = _ALLOC(dir_vr) ) == NULL ) {
        printf( "FATAL: malloc() failed in newDir\n" );
        exit( -1 );
    }
 
    dir->i = i;
    dir->j = j;
    dir->k = k;
 
    return dir;
}

/***********************************************************
* pointDistance
*   Return the distance between 2 points
***********************************************************/

double pointDistance
( 
    pnt_vr *pnt1,
    pnt_vr *pnt2
)
{
    return sqrt( 
        ( ( pnt1->i - pnt2->i ) * ( pnt1->i - pnt2->i ) ) +
        ( ( pnt1->j - pnt2->j ) * ( pnt1->j - pnt2->j ) ) +
        ( ( pnt1->k - pnt2->k ) * ( pnt1->k - pnt2->k ) )
    );
}

/***********************************************************
* matrixSoln 
*   Solve the simulatneous eqns for ax + by + cz = s
*   Where x, y, z, and s are 3d vectors
***********************************************************/

dir_vr *matrixSoln
(
    dir_vr *x,
    dir_vr *y,
    dir_vr *z,
    dir_vr *s
)
{
    // shortcuts for the 3x3 matrix elements
    double a = x->i;
    double b = y->i;
    double c = z->i;

    double d = x->j;
    double e = y->j;
    double f = z->j;

    double g = x->k;
    double h = y->k;
    double i = z->k;

    double det = a * (e*i - f*h) - b * (d*i - f*g) + c * (d*h - e*g);

    if ( det == 0 )
        return NULL;

    return newDir( 
            ( ( e*i - f*h ) * s->i + ( c*h - b*i ) * s->j + ( b*f - c*e ) * s->k ) / det,
            ( ( f*g - d*i ) * s->i + ( a*i - c*g ) * s->j + ( c*d - a*f ) * s->k ) / det,
            ( ( d*h - e*g ) * s->i + ( b*g - a*h ) * s->j + ( a*e - b*d ) * s->k ) / det
    );
}
