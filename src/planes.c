/*********************************************************************
* plane.c
*    Methods to support planes for ray tracing
*
*   Surface Renderers:
*       default
*       checkerboard
*********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "lists.h"
#include "math.h"
#include "config.h"
#include "common.h"
#include "planes.h"
#include "traceRay.h"

static list *surface_renderers;
static flexString *str_buf = NULL;

extern coord *pntToScreenCoord( pnt_vr *pnt );

/***********************************************************
* planeSetupCallbacks
*    Load the shape callbacks list - setup the surface
*    renderer list
***********************************************************/

void planeSetupCallbacks
(
   list *callback_list
)
{
    object_callbacks *callbacks;

    callbacks = _ALLOC(object_callbacks);
    if ( callbacks == NULL ) {
        printf( "FATAL calloc() failed in planeSetupCallbacks\n" );
        exit( -1 );
    }

    callbacks->objectLoad = planeLoadObject;
    callbacks->objectIntersection = planeFindIntersection;
    callbacks->objectContact = planeProcessContact;
    callbacks->objectScreenRegion = planeScreenRegion;
    callbacks->objectChrome = planeChrome;
    callbacks->objectRelease = planeRelease;

    appendToList( callback_list, "plane" , callbacks );

    planeSetupSurfaceRenderers();
}

void planeReleaseSurfaceRenderers()
{
    if ( surface_renderers != NULL )
        freeList( surface_renderers );
}

/***********************************************************
* planeSetupSurfaceRenderers
*    Setup the available surface renderers
*    Return non-zero on error else 0
***********************************************************/

void planeSetupSurfaceRenderers()
{
    renderer_callbacks *renderer;
    surface_renderers = initialiseList( free );

    renderer = newSurfaceRenderer(
                            planeSurfaceDefaultLoad,
                            planeSurfaceDefaultContact,
                            planeSurfaceDefaultRelease
    );
    appendToList( surface_renderers, "default", renderer );

    renderer = newSurfaceRenderer(
                            planeSurfaceCheckerboardLoad,
                            planeSurfaceCheckerboardContact,
                            planeSurfaceCheckerboardRelease
    );
    appendToList( surface_renderers, "checkerboard", renderer );
}

/***********************************************************
* planeLoadObject
*    Load a plane object from the given object list
*    Return non-zero on error else 0
***********************************************************/

void planeLoadObject
(
    list                    *object_key_vals,
    list                    *error_list,
    list                    *object_list,
    illumination_factors    *default_factors,
    screen_info             *screen
)
{
    char key_base[] = "objects+object";
    char *surface_type;
    char *plane_name;
    list_item *renderer_item;
    renderer_callbacks *renderer;
    config_list_item *config_item;
    planeObject *plane;
    int init_entries;
    pnt_vr pnt;

    if ( str_buf == NULL )
        str_buf = newFlexString();

    init_entries = ( error_list == NULL ) ? 0 : error_list->entries;

    surface_type = getConfigValue( object_key_vals, error_list, key_base, "surface" );
    
    if ( surface_type == NULL )
        return;

    if ( ( renderer_item = findInList( surface_renderers, surface_type ) ) == NULL ) {
        config_item = (config_list_item * )object_key_vals->head->data;
        printfFlexString( str_buf,
                "ln: %d %s is not a supported plane surface type",
                                config_item->line_number, surface_type );

        appendToList( error_list, str_buf->str, NULL );

        return;
    }
    renderer = (renderer_callbacks *)renderer_item;

    if ( ( plane = _ALLOC(planeObject) ) == NULL ) {
        printf( "FATAL calloc() failed in planeLoadObject\n" );
        exit( -1 );
    }

    plane->surface_type = strdup( surface_type );

    // These parameters are mandatory
    plane->origin = newPnt( 0, 0, 0 );
    getConfigDouble( object_key_vals, error_list, key_base, "origin+i", &plane->origin->i );
    getConfigDouble( object_key_vals, error_list, key_base, "origin+j", &plane->origin->j );
    getConfigDouble( object_key_vals, error_list, key_base, "origin+k", &plane->origin->k );

    plane->axis1 = newDir( 0, 0, 0 );
    getConfigDouble( object_key_vals, error_list, key_base, "axis1+i", &plane->axis1->i );
    getConfigDouble( object_key_vals, error_list, key_base, "axis1+j", &plane->axis1->j );
    getConfigDouble( object_key_vals, error_list, key_base, "axis1+k", &plane->axis1->k );

    plane->axis2 = newDir( 0, 0, 0 );
    getConfigDouble( object_key_vals, error_list, key_base, "axis2+i", &plane->axis2->i );
    getConfigDouble( object_key_vals, error_list, key_base, "axis2+j", &plane->axis2->j );
    getConfigDouble( object_key_vals, error_list, key_base, "axis2+k", &plane->axis2->k );

    // These parameters may be defaulted
    plane_name = getConfigValueDefault( object_key_vals, NULL, key_base, "name", "plane" );
    plane->name = strdup( plane_name );

    plane->illumination_factors = newIlluminationFactors( object_key_vals, default_factors );

    // Setup the screen region for this object
    plane->screen_region = newRegion( 0, 0, 0, 0 );
    
    setupRegionWithPnt( plane->screen_region, plane->origin );

    setPntFromPnt( &pnt, plane->origin );
    movePnt( &pnt, 1.0, plane->axis1 );
    updateRegionWithPnt( plane->screen_region, &pnt );

    setPntFromPnt( &pnt, plane->origin );
    movePnt( &pnt, 1.0, plane->axis2 );
    updateRegionWithPnt( plane->screen_region, &pnt );

    setPntFromPnt( &pnt, plane->origin );
    movePnt( &pnt, 1.0, plane->axis1 );
    movePnt( &pnt, 1.0, plane->axis2 );
    updateRegionWithPnt( plane->screen_region, &pnt );

    // These parameters are derived
    plane->normal = cross( plane->axis1, plane->axis2 );
    normalise( plane->normal );

    rendererLoad loadMethod = getRendererLoadMethod( surface_renderers, surface_type );
    plane->surface_properties = loadMethod( object_key_vals, error_list );

    plane->chrome = _ALLOC( image_chrome );
    loadConfigChrome( key_base, object_key_vals, error_list, plane->chrome );

    appendToList( object_list, "plane", plane );
}

/***********************************************************
* planeFindIntersection
*    Find an interection point (if any) between the given
*    plane and ray
*    Return distance or -1 if no intersection
***********************************************************/

double planeFindIntersection
(
    void *object,
    ray *sight
)
{
    planeObject *plane = (planeObject *)object;
    double distance = -1;
    double axis1_dist, axis2_dist;
    dir_vr values;
    dir_vr *soln;

    setDir( &values,
        sight->src->i - plane->origin->i,
        sight->src->j - plane->origin->j,
        sight->src->k - plane->origin->k
    );

    soln = matrixSoln( plane->axis1, plane->axis2, sight->dir, &values );

    if ( soln != NULL ) {

        axis1_dist = soln->i;
        axis2_dist = soln->j;

        if ( ! (
            ( axis1_dist < 0 ) || ( axis1_dist > 1 ) ||
            ( axis2_dist < 0 ) || ( axis2_dist > 1 )
        ) )
            distance = -1 * soln->k;

        free( soln );
    }

    return distance;
}

/***********************************************************
* planeProcessContact
*    Set the perceived color of the contact point
***********************************************************/

void planeProcessContact
(
    void    *object,
    ray     *sight,
    pnt_vr  *contact,
    list    *object_list
)
{
    planeObject *plane = (planeObject *)object;
    rgb_color contact_color;
    rgb_color *color;
    rendererContact contactMethod;
    double d1;
    dir_vr *reflection_dir;
    double reflective_index;
    ray *reflected_ray;

    setColor( &contact_color, 0.0, 0.0, 0.0 );

    d1 = 2 * dot( sight->dir, plane->normal );

    contactMethod = getRendererContactMethod( surface_renderers, plane->surface_type );
    contactMethod( object, contact, &contact_color );

    setContactPointColor( object, contact, &contact_color, sight,
                                            plane->normal, plane->illumination_factors );

    reflective_index = plane->illumination_factors->reflective_index;

    if ( reflective_index > 0.0 ) {

        // Find the reflected ray
        reflection_dir = newDir(
            sight->dir->i - d1 * plane->normal->i,
            sight->dir->j - d1 * plane->normal->j,
            sight->dir->k - d1 * plane->normal->k
        );

        color = newColor( 0.0, 0.0, 0.0 );

        reflected_ray = newRay( copyPnt( contact ), reflection_dir, color, NULL );
        movePnt( reflected_ray->src, _SMALL_DIST, reflection_dir );

        reflected_ray->strength = sight->strength * reflective_index;

        traceRay( reflected_ray, object_list, 'N' );

        addColor( sight->color,
                        reflected_ray->color->red,
                        reflected_ray->color->green,
                        reflected_ray->color->blue
        );

        if ( reflected_ray != NULL )
            rayRelease( reflected_ray );
    }

    sight->strength = 0.0;  // if not refracting or reflecting then thats it...
}

/***********************************************************
 * planeScreenRegion
 *    Return the screen region string for the object
 ***********************************************************/

region *planeScreenRegion
(
    void *object
)
{
    planeObject *plane = (planeObject *)object;

    return plane->screen_region;
}

/***********************************************************
* planeChrome
*    Return the crome string for the object
***********************************************************/

char *planeChrome
(
    void *object,
    image_chrome *chrome
)
{
    coord *axis_pnt[ 3 ];   // origin, axis1, axis2
    coord *origin_pnt;
    coord *normal_pnt;
    pnt_vr tmp_pnt;
    planeObject *plane = (planeObject *)object;
    image_chrome *plane_chrome = plane->chrome;

    setFlexString( str_buf, "" );

    if (
        ( chrome->bounding_box == 'A' ) ||
        ( ( chrome->bounding_box == 'Y' ) && ( plane_chrome->bounding_box == 'Y' ) )
    ) {

        if ( plane_chrome->bbox_color != NULL )
            addFlexString( str_buf, psColor( plane_chrome->bbox_color ) );
        else if ( chrome->bbox_color != NULL )
            addFlexString( str_buf, psColor( chrome->bbox_color ) );

        printfFlexStringCat( str_buf, "(%s) %f %f %f %f chrome_bbox\n",
                                    plane->name,
                                    plane->screen_region->lx,
                                    plane->screen_region->ly,
                                    plane->screen_region->ux,
                                    plane->screen_region->uy
        );
    }

    if (
        ( chrome->construction == 'A' ) ||
        ( ( chrome->construction == 'Y' ) && ( plane_chrome->construction == 'Y' ) )
    ) {

        if ( plane_chrome->construction_color != NULL )
            addFlexString( str_buf, psColor( plane_chrome->construction_color ) );
        else if ( chrome->construction_color != NULL )
            addFlexString( str_buf, psColor( chrome->construction_color ) );

        origin_pnt = pntToScreenCoord( plane->origin );

        setPntFromPnt( &tmp_pnt, plane->origin );
        movePnt( &tmp_pnt, 1.0, plane->axis1 );
        axis_pnt[ 0 ] = pntToScreenCoord( &tmp_pnt );

        setPntFromPnt( &tmp_pnt, plane->origin );
        movePnt( &tmp_pnt, 1.0, plane->axis2 );
        axis_pnt[ 1 ] = pntToScreenCoord( &tmp_pnt );

        printfFlexStringCat( str_buf,
                            "%f %f %f %f chrome_line\n"
                            "%f %f %f %f chrome_line\n",
                                    origin_pnt->x, origin_pnt->y,
                                    axis_pnt[ 0 ]->x, axis_pnt[ 0 ]->y,
                                    origin_pnt->x, origin_pnt->y,
                                    axis_pnt[ 1 ]->x, axis_pnt[ 1 ]->y
        );
    }

    if (
        ( chrome->normal == 'A' ) ||
        ( ( chrome->normal == 'Y' ) && ( plane_chrome->normal == 'Y' ) )
    ) {

        if ( plane_chrome->normal_color != NULL )
            addFlexString( str_buf, psColor( plane_chrome->normal_color ) );
        else if ( chrome->normal_color != NULL )
            addFlexString( str_buf, psColor( chrome->normal_color ) );

        setPntFromPnt( &tmp_pnt, plane->origin );
        movePnt( &tmp_pnt, 1.0, plane->normal );
        normal_pnt = pntToScreenCoord( &tmp_pnt );

        origin_pnt = pntToScreenCoord( plane->origin );

        printfFlexStringCat( str_buf,
                            "%f %f %f %f chrome_line\n",
                                    origin_pnt->x, origin_pnt->y,
                                    normal_pnt->x, normal_pnt->y
        );

        free( origin_pnt );
        free( normal_pnt );
    }

    return( str_buf->str );
}

/***********************************************************
* planeRelease
*    Release any reserved memory for the plane
***********************************************************/

void planeRelease
(
    void *object
)
{
    planeObject *plane = (planeObject *)object;
    rendererRelease releaseMethod;

    free( plane->origin );
    free( plane->axis1 );
    free( plane->axis2 );
    free( plane->normal );

    if ( plane->illumination_factors != NULL )
        free( plane->illumination_factors );

    releaseMethod = getRendererReleaseMethod( surface_renderers, plane->surface_type );
    releaseMethod( plane->surface_properties );

    free( plane->surface_type );
    free( plane->name );
    free( plane->screen_region );

    free( plane->chrome );

    free( plane );
}

/***********************************************************
* planeSurfaceLoadDefaultLoad
*    Load the property info for a plain surface.
*    Return a pointer to a renderer_info struct
***********************************************************/

void *planeSurfaceDefaultLoad
(
    list *property_list,
    list *error_list
)
{
    plane_surface_default *renderer_info;

    char key_base[] = "objects+object";

    renderer_info = _ALLOC( plane_surface_default );
    if ( renderer_info == NULL ) {
        printf( "FATAL calloc() failed in planeSurfaceDefaultLoad\n" );
        exit( -1 );
    }

    renderer_info->color = getConfigColor( property_list, error_list, key_base, "" );

    return renderer_info;
}

/***********************************************************
* planeSurfaceDefaultContact
*    Find the color of the given point on a checkboard
***********************************************************/

void planeSurfaceDefaultContact
(
    void *object,
    pnt_vr *contact,
    rgb_color *contact_color
)
{
    planeObject *plane = (planeObject *)object;

    plane_surface_default *info = (plane_surface_default *)plane->surface_properties;

    copyFromColor( info->color, contact_color );
}

/***********************************************************
* planeSurfaceDefaultRelease
*    Release the memory for a checkboard object
***********************************************************/

void planeSurfaceDefaultRelease
(
    void *renderer_info
)
{
    plane_surface_default *info = (plane_surface_default *)renderer_info;

    if ( ( info != NULL ) && ( info->color != NULL ) )
        free( info->color );

    free( info );
}

/***********************************************************
* planeSurfaceLoadCheckerboardLoad
*    Load the property info for a checkboard surface.
*    Return a pointer to a renderer_info struct
***********************************************************/

void *planeSurfaceCheckerboardLoad
(
    list *property_list,
    list *error_list
)
{
    plane_surface_checkerboard    *renderer_info;

    char key_base[] = "objects+object";

    renderer_info = _ALLOC( plane_surface_checkerboard );
    if ( renderer_info == NULL ) {
        printf( "FATAL calloc() failed in planeSurfaceCheckerboardLoad\n" );
        exit( -1 );
    }

    renderer_info->tile1_color = getConfigColor( property_list, error_list, key_base, "tile1" );
    renderer_info->tile2_color = getConfigColor( property_list, error_list, key_base, "tile2" );

    getConfigInteger( property_list, error_list, key_base, "tile1+count", &renderer_info->axis1_tiles );
    getConfigInteger( property_list, error_list, key_base, "tile2+count", &renderer_info->axis2_tiles );

    return renderer_info;
}

/***********************************************************
* planeSurfaceCheckerboardContact
*    Find the color of the given point on a checkboard
***********************************************************/

void planeSurfaceCheckerboardContact
(
    void *object,
    pnt_vr *contact,
    rgb_color *contact_color
)
{
    double axis1_ratio, axis2_ratio;
    double s1, s2;
    planeObject *plane = (planeObject *)object;
    rgb_color *tile_color;
    short tile_type;
    plane_surface_checkerboard *renderer_info;

    renderer_info = (plane_surface_checkerboard *)plane->surface_properties;

    dir_vr *plane_vr = newDir(
                    contact->i - plane->origin->i,
                    contact->j - plane->origin->j,
                    contact->k - plane->origin->k
    );

    s1 = _SIZEVR( plane->axis1 );
    s2 = _SIZEVR( plane->axis2 );
    axis1_ratio = dot( plane->axis1, plane_vr ) / ( s1 * s1 );
    axis2_ratio = dot( plane->axis2, plane_vr ) / ( s2 * s2 );
    free( plane_vr );

    tile_type = (
        ( (int)(axis1_ratio * renderer_info->axis1_tiles) ) + 
        ( (int)(axis2_ratio * renderer_info->axis2_tiles) ) 
    ) % 2;

    tile_color = tile_type ?
                renderer_info->tile1_color
                : 
                renderer_info->tile2_color 
    ; 

    copyFromColor( tile_color, contact_color );
}

/***********************************************************
* planeSurfaceCheckerboardRelease
*    Release the memory for a checkboard object
***********************************************************/

void planeSurfaceCheckerboardRelease
(
    void *renderer_info
)
{
    plane_surface_checkerboard *checker_board = (plane_surface_checkerboard *)renderer_info;

    if ( checker_board->tile1_color != NULL )
        free( checker_board->tile1_color );

    if ( checker_board->tile2_color != NULL )
        free( checker_board->tile2_color );

    free( checker_board );
}
