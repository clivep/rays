/***********************************************************
* traceRay.c
*    Follow  ray until it strikes an object
***********************************************************/
 
#include <stdio.h>
#include "traceRay.h"

int traceRay
(
    ray     *sight_ray,
    list    *object_list,
    char    check_screen_region
)
{
    double distance, closest_distance;
    list_item *closest_object;
    list_item *object_item;
    pnt_vr contact;
    region *screen_region;
    objectIntersection intersectionMethod;
    objectScreenRegion screenRegionMethod;
    objectContact contactMethod;
    int contacts = 0;
    char *object_type;

    while ( 1 ) {

        if ( sight_ray->strength <= 0.1 )
            return contacts;
    
        closest_distance = -1;
        closest_object = NULL;

        object_item = object_list->head;
        while ( object_item != NULL ) {
    
            object_type = object_item->label;

            // Skip this object if the screen point is outside its' region
            screenRegionMethod = getObjectScreenRegionMethod( object_type );
            screen_region = screenRegionMethod( object_item->data );

            if (
                ( check_screen_region == 'N' ) ||
                (
                    ( sight_ray->screen_coord->x > screen_region->lx ) &&
                    ( sight_ray->screen_coord->y > screen_region->ly ) &&
                    ( sight_ray->screen_coord->x < screen_region->ux ) &&
                    ( sight_ray->screen_coord->y < screen_region->uy )
                )
            ) {

                intersectionMethod = getObjectIntersetionMethod( object_type );
                distance = intersectionMethod( object_item->data, sight_ray );
    
                if (
                    ( distance > 0 ) &&
                    ( ( closest_distance < 0 ) || ( distance < closest_distance ) ) 
                ) {

                    /* If the contact points are REALLY close together then assume
                     * that the objects are coplanar at this point - arbitrarily
                     * select the object with the lower address
                     */
                    if (
                        ( closest_object != NULL ) &&
                        ( _MOD( closest_distance - distance ) < _VSMALL_DIST )
                    ) {

                        closest_object = _MIN( closest_object, object_item );
                    }
                    else  {

                        closest_object = object_item;
                        closest_distance = distance;
                    }
                }
            }

            object_item = object_item->next;
        }

        // Any intersections...?
        if ( closest_distance <= 0 )
            return contacts;

        contacts++;

        rayGetPoint( sight_ray, closest_distance, &contact );

        contactMethod = getObjectContactMethod( closest_object->label );
        contactMethod( closest_object->data, sight_ray, &contact, object_list );
    }
}   

