/**********************************************************************
* lists.h
*
* Definitions to support lists
*
**********************************************************************/
 
#ifndef _LISTS_H
#define _LISTS_H

typedef struct list_item {
    char    *label;
    void    *data;
    struct  list_item *next;
    struct  list_item *prev;
} list_item;

typedef struct {
    void    ( *freeItem )( void *data );
    int     entries;
    struct  list_item *head;
    struct  list_item *tail;
} list;

list *initialiseList( void (*freeFunc)(void *) );
list_item *appendToList( list *list, char *label, void *data );
list_item *findInList( list *list, char *label );
list_item *findKeyInList( list *list, char *key_base, char *key );
list_item *replaceInList( list *list, char *label, void *data );
void popFromList( list *list );
void displayList( list *list, void (*displayItem)(void *) );
void freeList( list *list );

list_item *appendToList( list *list, char *label, void *data );
void popFromList( list *list );

list_item *appendDoubleToList( list *list, double value );
short getLastDoubleFromList( list *list, double *value );

#endif
