/**********************************************************************
* rings.h
*
* Definitions to support ring objects for ray tracing
*
**********************************************************************/

#ifndef _RINGS_H
#define _RINGS_H

#include "lists.h"
#include "geometry.h"
#include "objects.h"

typedef struct {
    char                    *name;
    pnt_vr                  *origin;
    dir_vr                  *axis1;
    dir_vr                  *axis2;
    dir_vr                  *normal;
    region                  *screen_region;
    double                  inner_radius;
    double                  outer_radius;
    char                    *surface_type;
    illumination_factors    *illumination_factors;
    void                    *surface_properties;
    pnt_vr                  *plane_limits[ 4 ];
    image_chrome            *chrome;
} ringObject;

void ringSetupCallbacks( list *callback_list );
void ringReleaseSurfaceRenderers();

void ringLoadObject( list *object_info, list *error_list,
                        list *object_list,
                        illumination_factors *default_factors,
                        screen_info *screen );
double ringFindIntersection( void *object, ray *sight );
void ringProcessContact( void *object, ray *sight,
                        pnt_vr *contact, list *objecti_list );
char *ringChrome( void *object, image_chrome *chrome );
region *ringScreenregion( void *object );
region *ringScreenRegion();
void ringRelease( void *object );

void ringFindNormal( void *object, pnt_vr *contact, dir_vr *normal );

/***********************************************************
* surface renderers
***********************************************************/

void ringSetupSurfaceRenderers();

/***********/
/* default */

typedef struct {
        rgb_color   *color;
} ring_surface_default;

void *ringSurfaceDefaultLoad( list *property_list, list *error_list );
void ringSurfaceDefaultContact( void *object,
                                pnt_vr *contact, rgb_color *contact_color );
void ringSurfaceDefaultRelease( void *renderer_info );

#endif
