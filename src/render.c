/*********************************************************************
* render.c
*    Methods to support displaying ray trace images
*********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "common.h"
#include "lists.h"
#include "objects.h"

#include "renderImageFile.h"
#include "renderImageScreen.h"

#include "render.h"

//TODO release this list
static list *render_callback_list = NULL;

static char *render_type;

/***********************************************************
* setupRenderCallbacks
*    Load the renderer callbacks list
***********************************************************/

void setupRenderCallbacks
(
    image_info *image,
    char *output,
    list *error_list
)
{
    render_callback_list = initialiseList( free );
    char err_buf[ 64 ];
    char *extent;
    flexString *command;

    if ( strcmp( output, "screen" ) == 0 ) {

        render_type = "screen";
        renderImageScreenSetupCallbacks( render_callback_list );
        renderImageScreenInit();
        return;
    }

    render_type = "file";
    renderImageFileSetupCallbacks( render_callback_list );

    // Validate the output file type
    extent = output + strlen( output ) - 1;
    while (
        ( extent > output ) &&
        ( *extent != '/' ) && ( *extent != '.' )
    )
        extent--;

    if ( strcmp( extent, ".ps" ) == 0 ) {

        renderImageFileInit( image, output, "ps", "cp %s %s" );
    }
    else if ( strcmp( extent, ".pdf" ) == 0 ) {

        renderImageFileInit( image, output, "pdf", "ps2pdf %s %s" );
    }
    else if (
        ( strcmp( extent, ".gif" ) == 0 ) ||
        ( strcmp( extent, ".jpg" ) == 0 ) ||
        ( strcmp( extent, ".png" ) == 0 )
    ) {
        command = newFlexString();
        printfFlexString( command, "convert -resize %dx%d %%s %%s",  
                                    (int)(image->width * image->resolution),
                                    (int)(image->height * image->resolution)
        );

        renderImageFileInit( image, output, extent, command->str );

        releaseFlexString( command );
    }
    else {

        sprintf( err_buf, "%s is not a supported file type", extent );
        appendToList( error_list, err_buf, NULL );
    }
}

void releaseRenderCallbacks()
{
    freeList( render_callback_list );
}

/***********************************************************
* runRenderPoint
*    Execute the render point method for the given renderer
***********************************************************/

void runRenderPoint
(
    rgb_color *point_color
)
{
    list_item *callback_item;
    render_callbacks *callbacks;

    callback_item = findInList( render_callback_list, render_type );

    if ( callback_item != NULL ) {

        callbacks = callback_item->data;
        callbacks->renderPoint( point_color );
    }
    else
        printf( "no renderPoint callback\n");
}

/***********************************************************
* runRenderEndOfLine
*    Execute the end of line method for the given renderer
***********************************************************/

void runRenderEndOfLine()
{
    list_item *callback_item;
    render_callbacks *callbacks;

    callback_item = findInList( render_callback_list, render_type );

    if ( callback_item != NULL ) {

        callbacks = callback_item->data;
        callbacks->renderEndOfLine();
    }
    else
        printf( "no renderEndOfLine callback\n");
}

/***********************************************************
* runRenderEndOfImage
*    Execute the end of image method for the given renderer
***********************************************************/

void runRenderEndOfImage
(
    list *objects
)
{
    list_item *callback_item;
    render_callbacks *callbacks;

    callback_item = findInList( render_callback_list, render_type );

    if ( callback_item != NULL ) {

        callbacks = callback_item->data;
        callbacks->renderEndOfImage( objects );
        callbacks->renderRelease();
    }
    else
        printf( "no renderEndOfImage callback\n");
}
