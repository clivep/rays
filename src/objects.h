/**********************************************************************
* objects.h
*
* Definitions to support generic objects for ray tracing
*
**********************************************************************/

#ifndef _OBJECTS_H
#define _OBJECTS_H

#include "lists.h"
#include "color.h"
#include "common.h"
#include "geometry.h"
#include "illumination.h"

/***********************************************************
* surface renderers
***********************************************************/

typedef void *(* rendererLoad)( list *key_val_list, list *error_list );
typedef void (* rendererContact)( void *object,
                                pnt_vr *contact, rgb_color *contact_color );
typedef void (* rendererRelease)( void *renderer_info );

typedef struct {
    void *(* rendererLoad)( list *key_val_list, list *error_list );
    void (* rendererContact)( void *object,
                                pnt_vr *contact, rgb_color *contact_color );
    void (* rendererRelease)( void *renderer_info );
} renderer_callbacks;

renderer_callbacks *newSurfaceRenderer( rendererLoad,
                                    rendererContact, rendererRelease );

rendererLoad getRendererLoadMethod( list *renderer_callback_list, char *renderer );
rendererContact getRendererContactMethod( list *renderer_callback_list, char *renderer );
rendererRelease getRendererReleaseMethod( list *renderer_callback_list, char *renderer );

/***********************************************************
* objects
***********************************************************/

typedef struct {
    char        bounding_box;
    rgb_color   *bbox_color;
    char        construction;
    rgb_color   *construction_color;
    char        normal;
    rgb_color   *normal_color;
} image_chrome;

typedef struct {
    double          height;
    double          width;
    double          resolution;
    char            *units;
    double          height_points;
    double          width_points;
    image_chrome    chrome;
    double          chrome_multiplier[ 2 ];    // x and y
} image_info;

typedef struct {
    pnt_vr   *centre;
    pnt_vr   *offset;
    dir_vr   *normal;
    dir_vr   *x_axis;
    dir_vr   *y_axis;
    int      width;
    int      height;
    double   resolution; // samples per pixel
    double   eye_dist;
    pnt_vr   *eye;
} screen_info;

typedef void (* objectLoad)( list *object_key_vals, list *error_list,
                                            list *object_list,
                                            illumination_factors *default_factors,
                                            screen_info *screen );
typedef double (* objectIntersection)(void *object, ray *sight );
typedef void (* objectContact)( void *object, ray *sight, pnt_vr *contact, list *objects );
typedef char *(* objectChrome)( void *object, image_chrome *chrome );
typedef region *(* objectScreenRegion)( void *object );
typedef void (* objectFindNormal)( void *object, pnt_vr *contact, dir_vr *normal );
typedef void (* objectRelease)( void *object );

typedef struct {
    void     (* objectLoad)( list *object_key_vals, list *error_list,
                                            list *object_list,
                                            illumination_factors *default_factors,
                                            screen_info *screen );
    double   (* objectIntersection)(void *object, ray *sight );
    void     (* objectContact)(void *object, ray *sight, pnt_vr *contact, list *objects );
    void     (* objectFindNormal)( void *object, pnt_vr *contact, dir_vr *normal );
    char     *(* objectChrome)( void *object, image_chrome *chrome );
    region   *(* objectScreenRegion)( void *object );
    void     (* objectRelease)(void *object);
} object_callbacks;

void setupObjectCallbacks();
void releaseObjectCallbacks();

objectLoad getObjectLoadMethod( char *object_type );
objectIntersection getObjectIntersetionMethod( char *object_type );
objectContact getObjectContactMethod( char *object_type );
objectChrome getObjectChromeMethod( char *object_type );
objectScreenRegion getObjectScreenRegionMethod( char *object_type );
objectRelease getObjectReleaseMethod( char *object_type );

void loadConfigImage( list *key_val_list, list *error_list, image_info *image );
short loadConfigScreen( list *key_val_list, list *error_list, screen_info *screen );
double getUnitsMultiplier( char *unit );

void loadConfigChrome( char *key_base, list *key_val_list, list *error_list,
                                                                image_chrome *chrome );

#endif
