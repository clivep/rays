/**********************************************************************
* color.h
*
* Definitions to support colors for ray tracing
*
**********************************************************************/

#ifndef _COLOR_H
#define _COLOR_H

#include "lists.h"

#define newGreyColor(g) newColor( (g), (g), (g) )

#define setBlackColor(c) setColor( (c), 0.0, 0.0, 0.0 )
#define setGreyColor(c,g) setColor( (c), (g), (g), (g) )
#define setWhiteColor(c) setColor( (c), 1.0, 1.0, 1.0 )

typedef struct {
    double   red;
    double   green;
    double   blue;
} rgb_color;

void loadDefaultColors();
void loadConfigCustomColors( list *key_val_list, list *error_list );
rgb_color *findColor( char *name );
void freeColors();
void setColor( rgb_color *color, double r, double g, double b );
void addColor( rgb_color *color, double r, double g, double b );
rgb_color *newColor( double red, double green, double blue );
rgb_color *copyColor( rgb_color *color );
void addFromColor( rgb_color *src_color, rgb_color *dst_color );
void copyFromColor( rgb_color *src_color, rgb_color *dst_color );
void addColor( rgb_color *color, double red, double green, double blue );

void scrambleColor( rgb_color *color, double scramble_factor );
double colorRange( double component );
char *psColor( rgb_color *color );

void printColor( char *label, rgb_color *color );

#endif
