/**********************************************************************
* render.h
*
* Definitions to support displaying ray trace images
*
**********************************************************************/

#ifndef _RENDER_H
#define _RENDER_H

#include "color.h"
#include "lists.h"
#include "objects.h"

typedef void (* renderPoint)( rgb_color *point_color );
typedef void (* renderEndOfLine)();
typedef void (* renderEndOfImage)( list *objects );
typedef void (* renderRelease)();

typedef struct {
   void (* renderPoint)( rgb_color *point_color );
   void (* renderEndOfLine)();
   void (* renderEndOfImage)( list *objects );
   void (* renderRelease)();
} render_callbacks;

void setupRenderCallbacks( image_info *screen, char *output, list *err_list );
void releaseRenderCallbacks();

void runRenderPoint( rgb_color *point_color );
void runRenderEndOfLine();
void runRenderEndOfImage( list *objects );
void runRenderRelease();

#endif
