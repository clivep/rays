/**********************************************************************
* rays.h
*
* Definitions to support ray tracing
*
**********************************************************************/
 
#ifndef _RAYS_H
#define _RAYS_H

#include "lists.h"
#include "color.h"
#include "config.h"
#include "objects.h"
#include "geometry.h"
#include "illumination.h"

typedef enum {

    PIXEL_CENTRE,
    PIXEL_TOP_LEFT,
    PIXEL_TOP_RIGHT,
    PIXEL_BOTTOM_LEFT,
    PIXEL_BOTTOM_RIGHT
} PIXEL_LOC;

/***********************************************************
* the image environment
***********************************************************/
 
typedef struct {
    screen_info             screen;
    image_info              image;
    illumination_factors    *default_factors;
    list                    *objects;
    rgb_color               *background;
    short                   metrics;
    short                   list_config;
    short                   anti_aliasing;
    short                   chrome;
    char                    *output;
} setup;

static short setupEnvironment( char *config_file, char *output );
static void loadConfigDefaultProperties( list *property_key_vals );   
static short setupScreen();
static ray *setupSightRay( int x, int y, PIXEL_LOC location );
static void processImage();
static void cleanupEnvironment();
void traceScreenRay( ray *sight_ray, list *object_list );

coord *pntToScreenCoord( pnt_vr *pnt );

#endif
