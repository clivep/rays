/**********************************************************************
* <things>.h
*
* Definitions to support <thing> objects for ray tracing
*
**********************************************************************/

#ifndef _<THINGS>_H
#define _<THINGS>_H

#include "lists.h"
#include "geometry.h"
#include "objects.h"

/***********************************************************
* the <thing> object
***********************************************************/
typedef struct {
    char                    *name;
    region                  *screen_region;
    char                    *surface_type;
    illumination_factors    *illumination_factors;
    void                    *surface_properties;
    image_chrome            *chrome;
} <thing>Object;

void <thing>SetupCallbacks( list *callback_list );
void <thing>ReleaseSurfaceRenderers();

void <thing>LoadObject( list *object_info, list *error_list,
                        list *object_list, list *default_properties,
                        screen_info *screen );
double <thing>FindIntersection( void *object, ray *sight );
void <thing>ProcessContact( void *object, ray *sight,
                        pnt_vr *contact, list *object_list );
void <thing>FindNormal( void *object, pnt_vr *contact, dir_vr *normal );
char *<thing>Chrome( void *object, image_chrome *chrome );
region *<thing>ScreenRegion( void *object );
void <thing>Release( void *object );

/***********************************************************
* surface renderers
***********************************************************/

void <thing>SetupSurfaceRenderers();

/***********/
/* default */

typedef struct {
    rgb_color   *color;
} <thing>_surface_default;

void *<thing>SurfaceDefaultLoad( list *property_list, list *error_list );
void <thing>SurfaceDefaultContact( void *object,
                                pnt_vr *contact, rgb_color *contact_color );
void <thing>SurfaceDefaultRelease( void *renderer_info );

#endif
