/*********************************************************************
* renderImageScreen.c
*    Methods to support displaying results on the screen
*********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "common.h"
#include "color.h"
#include "render.h"
#include "renderImageScreen.h"

/***********************************************************
* renderImageScreenSetupCallbacks
*    Load the renderer callbacks list
***********************************************************/

void renderImageScreenSetupCallbacks
(
   list *callback_list
)
{
    render_callbacks *callbacks;

    callbacks = _ALLOC(render_callbacks);
    if ( callbacks == NULL ) {
        printf( "FATAL calloc() failed in renderImageScreenSetupCallbacks\n" );
        exit( -1 );
    }

    callbacks->renderPoint = renderImageScreenPoint;
    callbacks->renderEndOfLine = renderImageScreenEndOfLine;
    callbacks->renderEndOfImage = renderImageScreenEndOfImage;
    callbacks->renderRelease = renderImageScreenRelease;

    appendToList( callback_list, "screen" , callbacks );
}

/***********************************************************
* renderImageScreenInit
*    Prepare the image renderer
***********************************************************/

void renderImageScreenInit
(
    char *output
)
{
  //  system( "clear" );
}

/***********************************************************
* renderImageScreenPoint
*    Render a point
***********************************************************/

void renderImageScreenPoint
(
    rgb_color *point_color
)
{
    double col = ( 9.9999 * sqrt (
                    ( point_color->red * point_color->red ) +
                    ( point_color->green * point_color->green ) +
                    ( point_color->blue * point_color->blue )
                 ) / sqrt( 3.0 ) );

    printf( "%c", '0' + (int)col );
}

/***********************************************************
* renderImageScreenEndOfLine
*    Actions after the end of a scanning line
***********************************************************/

void renderImageScreenEndOfLine()
{
    printf( "\n" );
}

/***********************************************************
* renderImageScreenEndOfImage
*    Actions to finalise the image
***********************************************************/

void renderImageScreenEndOfImage
(
    list    *objects
)
{
    /* nothing to do */
}

/***********************************************************
* renderImageScreenRelease
*    Releae any renderer allocated memory
***********************************************************/

void renderImageScreenRelease()
{
    /* nothing to do */
}
