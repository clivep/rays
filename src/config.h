/**********************************************************************
* config.h
*
* Definitions to support configurations for ray tracing
*
**********************************************************************/

#ifndef _CONFIG_H
#define _CONFIG_H

#include "lists.h"
#include "color.h"
#include "objects.h"
#include "illumination.h"

typedef struct config_list_item {
    int     line_number;
    char    *value;
} config_list_item;

short loadConfig( char *config_file, list *key_val_list,
                                                list *error_list );
short validateConfig( list *key_val_list );
void loadConfigDefaults( list *key_val_list );
void loadConfigObjects( list *key_val_list, list *error_list,
                        list *object_list, illumination_factors *default_factors,
                        screen_info *screen );
void loadConfigObject( list *object_info, list *error_list,
                            list *object_list, illumination_factors *default_factors,
                            screen_info *screen );
config_list_item *newConfigItem( int line_number, char *value );
void freeConfigItemList( void *item );
void freeConfigObject( void *item );
short getConfigDouble( list *object_info, list *error_list,
                        char *key_base, char *key, double *value );
void getConfigDoubleDefault( list *object_info, list *error_list,
                        char *key_base, char *key, double *value, double default_value );
void getConfigInteger( list *object_info, list *error_list,
                        char *key_base, char *key, int *value );
char *getConfigValue( list *object_info,
                        list *error_list, char *key_base, char *key );
char *getConfigValueDefault( list *object_info, list *error_list,
                        char *key_base, char *key, char *default_val );
void configListItemDisplay( void *entry );
rgb_color *getConfigColor ( list *config_list, list *error_list, 
                                                    char *key_base, char *key);

#endif
