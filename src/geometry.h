/**********************************************************************
* geometry.h
*
* Definitions to support geometric operations
*
**********************************************************************/

#ifndef _GEOMETRY_H
#define _GEOMETRY_H

#include <math.h>
#include "color.h"

#define _PRNTVR(d, v) printf( "%s => %f %f %f\n", (d), (v)->i, (v)->j, (v)->k )
#define _SIZEVR(v) sqrt( ((v)->i*(v)->i) + ((v)->j*(v)->j) + ((v)->k*(v)->k) )

typedef struct {
   double x;
   double y;
} coord;

typedef struct {
   double lx;
   double ly;
   double ux;
   double uy;
} region;

typedef struct {
   double i;
   double j;
   double k;
} dir_vr;

typedef struct {
   double i;
   double j;
   double k;
} pnt_vr;

typedef struct {
   pnt_vr       *src;
   coord        *screen_coord;
   dir_vr       *dir;
   rgb_color    *color;
   int          contact;
   double       strength;
   list         *refractive_indices;
} ray;

void setPnt( pnt_vr *pnt, double i, double j, double k );
void setPntFromPnt( pnt_vr *dst, pnt_vr *src );
void setDir( dir_vr *dir, double i, double j, double k );
double dot( dir_vr *a, dir_vr *b );
dir_vr *cross( dir_vr *a, dir_vr *b );
void normalise( dir_vr *dir );
pnt_vr *newPnt( double i, double j, double k );
dir_vr *newDir( double i, double j, double k );
ray *newRay( pnt_vr *vr, dir_vr *dir, rgb_color *color, coord *screen_coord );
coord *newCoord( double x, double y );
void addPnt( pnt_vr *pnt_1, pnt_vr *pnt_2 );
void subPnt( pnt_vr *pnt_1, pnt_vr *pnt_2 );
void rayGetPoint( ray *sight, double distance, pnt_vr *point );
void rayRelease( ray *sight_ray );
dir_vr *dirFromPnt( pnt_vr *pnt );
pnt_vr *copyPnt( pnt_vr *pnt );
void movePnt( pnt_vr *pnt, double dist, dir_vr *dir );
dir_vr *matrixSoln( dir_vr *x, dir_vr *y, dir_vr *z, dir_vr *s );
double pointDistance( pnt_vr *pnt1, pnt_vr *pnt2 );

region *newRegion( double lx, double ly, double ux, double uy );
void updateRegionWithPnt( region *rgn, pnt_vr *pnt ); 
void setupRegionWithPnt( region *rgn, pnt_vr *pnt ); 
void extendRegionWithOffsets( region *rgn, double x,
                        double y, double x_off, double y_off ); 

#endif
