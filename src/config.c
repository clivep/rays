/*********************************************************************
* config.c
*    Methods to support confiurations for ray tracing
*********************************************************************/

#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <math.h>

#include "common.h"
#include "color.h"
#include "objects.h"

#define _PHI    ( ( 1.0 + sqrt( 5.0 ) ) / 2.0 )

static char str_buf[ 1024 ];

/***********************************************************
* getConfigInteger
*    Recover an integer value from the object list
***********************************************************/
void getConfigInteger
(
    list     *object_info,
    list     *error_list,
    char     *key_base,
    char     *key,
    int      *value
)
{
    char *config_value;
    
    *value = 0;

    config_value = getConfigValue( object_info, error_list, key_base, key );

    if ( config_value != NULL )
        *value = atoi( config_value );
}

/***********************************************************
* getConfigDouble
*    Recover a double value from the object list
*    Return 1 if value found else 0
***********************************************************/
short getConfigDouble
(
    list     *object_info,
    list     *error_list,
    char     *key_base,
    char     *key,
    double   *value
)
{
    char *config_value;
    short found = 0;

    config_value = getConfigValue( object_info, error_list, key_base, key );

    if ( config_value != NULL ) {

        if ( strncmp( config_value, "phi", 3 ) == 0 )
            *value = _PHI;
        else if ( strncmp( config_value, "-phi", 4 ) == 0 )
            *value = -1.0 * _PHI;
        else if ( strncmp( config_value, "pi", 2 ) == 0 )
            *value = M_PI;
        else if ( strncmp( config_value, "-pi", 3 ) == 0 )
            *value = -1.0 * M_PI;
        else
            *value = atof( config_value );

        found = 1;
    }

    return found;
}

/***********************************************************
* getConfigDoubleDefault
*    Recover a double value from the object list - default 
*    the value if missing
***********************************************************/
void getConfigDoubleDefault
(
    list     *object_info,
    list     *error_list,
    char     *key_base,
    char     *key,
    double   *value,
    double   default_value
)
{
    short found;

    found = getConfigDouble( object_info, error_list, key_base, key, value );

    if ( ! found )
        *value = default_value;
}

/***********************************************************
* getConfigValue
*    Recover a value from the list
***********************************************************/
char *getConfigValue
(
    list     *key_val_list,
    list     *error_list,
    char     *key_base,
    char     *key
)
{
    list_item           *item;
    config_list_item    *config_item;
    char                *value = NULL;

    if ( ( item = findKeyInList( key_val_list, key_base, key ) ) == NULL ) {

        if ( error_list != NULL ) {
            config_item = (config_list_item * )key_val_list->head->data;
            sprintf( str_buf, "ln: %d Missing %s+%s value for object",
                                     config_item->line_number, key_base, key );
        
            appendToList( error_list, str_buf, NULL );
        }
    }
    else {

        config_item = (config_list_item * )item->data;
        value = config_item->value;
    }

    return value;
}

/***********************************************************
* getConfigValueDefault
*    Recover a value from the list - default if missing
***********************************************************/
char *getConfigValueDefault
(
    list    *key_val_list,
    list    *error_list,
    char    *key_base,
    char    *key,
    char    *default_val
)
{
    char *value;

    value = getConfigValue( key_val_list, error_list, key_base, key );
    
    if ( value == NULL )
        return default_val;

    return value;
}

/***********************************************************
* newConfigItem
*    Setup and return a new list object
*    Return pointer to the list or NULL
***********************************************************/

config_list_item *newConfigItem
(
    int     line_number,
    char    *value
)
{
    config_list_item *config_item;

    config_item = _ALLOC(config_list_item);

    if ( config_item == NULL ) {
        printf( "FATAL: calloc() failed in config_list_item\n" );
        exit( -1 );
    }

    config_item->line_number = line_number;
    config_item->value = ( value == NULL ) ? NULL : strdup( value );

    return config_item;
}

/***********************************************************
* freeConfigItemList
*    Free an config list item
***********************************************************/

void freeConfigItemList
(
   void *item
)
{
   config_list_item *config_item = (config_list_item *)item;

   if ( config_item->value != NULL )
      free( config_item->value );

   free( config_item );
}

/***********************************************************
* loadConfig
*    Load the config file contents into the config list
*    Return non-0 on error else 0
***********************************************************/

short loadConfig
(
    char *config_file,
    list *key_val_list,
    list *error_list
)
{
    FILE *fp;
    char key_string[ 256 ];
    short current_indent;
    short indent;
    char indent_str[ 5 ];
    short retval;
    int line_no;
    short in_lamp;
    short in_object;
    char line[ 256 ];
    list *key_indent_list;
    int item_indent;
    list_item *item;
    char *p;
    char *val;
    char *key;

    key_indent_list = initialiseList( free );

    if ( ( fp = fopen( config_file, "r" ) ) == NULL ) { 
    
        printf( "Could not open %s", config_file );
        return -1;
    }

    *key_string = '\0';
    current_indent = 0;
    retval = 0;

    in_lamp = 0;
    in_object = 0;
    appendToList( key_indent_list, "-1", strdup( "" ) );

    for( line_no = 1; fgets( line, sizeof(line) - 1, fp ) != NULL; line_no++ ) {

        // Strip comments
        if ( ( p = strstr( line, "#" ) ) != NULL )
            *p = '\0';

        if ( *line == '\0' )
            continue;

        // Strip trailing whitespace
        p = &line[ strlen( line ) - 1 ];
        while (
            ( p >= line ) && 
            ( ( *p == ' ' ) || ( *p == '\r' ) || ( *p == '\n' ) )
        ) {
            *p = '\0';
            p--;
        }

        p = line;
        while ( ( *p != '\0' ) && ( *p == ' ' ) )
            p++;

        indent = p - line;

        key = p;

        if ( *p == '\0' )
            continue;

        if ( ( p = strstr( p, ":" ) ) == NULL ) {
            appendToList( error_list, "missing ':'", NULL );
            continue;
        }

        *p = '\0';
        val = p + 1;
        while ( *val == ' ' )
            val++;

        if ( strcmp( key, "include_cfg" ) == 0 ) {

            if ( in_lamp )
                appendToList( key_val_list, "illumination+lamp+end",
                                            newConfigItem( line_no, NULL ) );
            if ( in_object )
                appendToList( key_val_list, "objects+object+end",
                                            newConfigItem( line_no, NULL ) );

            in_lamp = 0;
            in_object = 0;

            loadConfig( val, key_val_list, error_list );

            continue;
        }

        for( item = key_indent_list->tail; item != NULL; item = item->prev ) {

            item_indent = atoi( item->label );
            if ( item_indent < indent )
                break;
        }

        sprintf( indent_str, "%d", indent );

        strcpy( key_string, item->data );
        if ( *key_string )
            strcat( key_string, "+" );
        strcat( key_string, key );

        if (
            (
                ( *val == '\0' ) && ( in_lamp ) &&
                ( strcmp( key, "lamp" ) == 0 )
            ) ||
            (
                in_lamp && 
                ( strncmp( key_string, "illumination+lamp", 14 ) != 0 )
            )
        )
        {
            appendToList( key_val_list, "illumination+lamp+end",
                                            newConfigItem( line_no, NULL ) );
            in_lamp = 0;
        }

        if (
            (
                ( *val == '\0' ) && ( in_object ) &&
                ( strcmp( key, "object" ) == 0 )
            ) ||
            (
                in_object && 
                ( strncmp( key_string, "objects+object", 14 ) != 0 )
            )
        )
        {
            appendToList( key_val_list, "objects+object+end",
                                            newConfigItem( line_no, NULL ) );
            in_object = 0;
        }

        replaceInList( key_indent_list, indent_str, strdup( key_string ) );

        if ( *val )
            appendToList( key_val_list, key_string,
                                            newConfigItem( line_no, val ) );
        else {

            if ( strcmp( key_string, "illumination+lamp" ) == 0 ) {
                appendToList( key_val_list, "illumination+lamp+start",
                                            newConfigItem( line_no, NULL ) );
                in_lamp = 1;
            }

            if ( strcmp( key_string, "objects+object" ) == 0 ) {
                appendToList( key_val_list, "objects+object+start",
                                            newConfigItem( line_no, NULL ) );
                in_object = 1;
            }
        }
    }  

    fclose( fp );

    if ( in_lamp )
        appendToList( key_val_list, "illumination+lamp+end",
                                            newConfigItem( line_no, NULL ) );

    if ( in_object )
        appendToList( key_val_list, "objects+object+end",
                                            newConfigItem( line_no, NULL ) );

    freeList( key_indent_list );

    return retval;
}

/***********************************************************
* loadConfigObjects
*    Load the objects from the config list
*    Return non-zero on error else 0
***********************************************************/

void loadConfigObjects
(
    list                    *key_val_list,
    list                    *error_list,
    list                    *object_list,
    illumination_factors    *default_factors,
    screen_info             *screen
)
{
    short in_object = 0;
    list_item *item;
    list *object_key_vals = NULL;
    config_list_item *config_item;
    config_list_item *new_config_item;

// TODO correct the object free func
    for ( item = key_val_list->head; item != NULL; item = item->next ) {

        if ( strcmp( item->label, "objects+object+end" ) == 0 ) {

            if ( object_key_vals != NULL ) {

                loadConfigObject( object_key_vals, error_list,
                                        object_list, default_factors, screen );
                freeList( object_key_vals );
                object_key_vals = NULL;
            }

            in_object = 0;
            continue;
        }

        if ( strcmp( item->label, "objects+object+start" ) == 0 ) {
    
            if ( object_key_vals != NULL )
                freeList( object_key_vals );

            object_key_vals = initialiseList( freeConfigObject );
    
            in_object = 1;
            continue;
        }

        if ( in_object ) {
            config_item = item->data;
            new_config_item = newConfigItem( config_item->line_number,
                                                         config_item->value );
            appendToList( object_key_vals, item->label, new_config_item );
        }
    }

    if ( object_key_vals != NULL ) {
        
        loadConfigObject( object_key_vals, error_list,
                                            object_list, default_factors, screen );
        freeList( object_key_vals );
    }
}

/***********************************************************
 * freeConfigObject
 *    Release a config object
 ***********************************************************/

void freeConfigObject
(
    void *item
)
{
    config_list_item *config_item = (config_list_item *)item;

    if ( config_item != NULL ) {

        if ( config_item->value != NULL )
            free( config_item->value );

        free( config_item );
    }
}

/***********************************************************
* loadConfigObject
*    Load and validate the object details from the list
***********************************************************/

void loadConfigObject
(
    list *object_info,
    list *error_list,
    list *object_list,
    illumination_factors *default_factors,
    screen_info *screen
)
{
    list_item  *item;
    objectLoad loadMethod;
    char *object_type;

    if ( ( item = findInList( object_info, "objects+object+type" ) ) != NULL ) {
        
        config_list_item *config_item = (config_list_item *)item->data;
        object_type = config_item->value;

        loadMethod = getObjectLoadMethod( object_type );
        if ( loadMethod == NULL ) {
            sprintf( str_buf, "ln: %d Unsupported object type: %s",
                                 config_item->line_number, object_type );
            appendToList( error_list, str_buf, NULL );
        }
        else
            loadMethod( object_info, error_list, object_list, default_factors, screen );
    }
    else {
        config_list_item *config_item =
                                 (config_list_item *)object_info->head->data;
        sprintf( str_buf, "ln: %d Missing object type value",
                                                   config_item->line_number );
        appendToList( error_list, str_buf, NULL );
    }
}

/***********************************************************
* configListItemDisplay
*   Print the contents of a config list item
***********************************************************/

void configListItemDisplay
(
   void  *entry
)
{
   list_item *item = (list_item *)entry;
   config_list_item *config_item = (config_list_item *)item->data;

   printf( "%d label %s, data '%s'\n",
                        config_item->line_number,
                        ( item->label != NULL ) ? item->label : "NULL",
                        config_item->value
   );
}

/***********************************************************
* getConfigColor
*   Recover a color from a config list
***********************************************************/

rgb_color *getConfigColor
(
    list *config_list,
    list *error_list,
    char *key_base,
    char *key
)
{
    double grey;
    double red, green, blue;
    short c;
    char key_name[256];
    char *color_name;
    list_item *item;
    config_list_item *config_item;
    rgb_color *color_def;
    rgb_color *color = NULL;

    if ( strlen( key ) > 0 )
        sprintf( key_name, "%s+%s", key_base, key );
    else
        strcpy( key_name, key_base );

    if ( ( item = findKeyInList( config_list, key_name, "color-name" ) ) != NULL ) {

        config_item = (config_list_item *)item->data;
        color_name = config_item->value;

        if ( ( color_def = findColor( color_name ) ) != NULL ) {

            color = copyColor( color_def );
        }
        else {

            sprintf( str_buf, "ln: %d Unknown color definition: %s",
                                    config_item->line_number, color_name );
            appendToList( error_list, str_buf, NULL );
        }
    }
    else if ( getConfigDouble( config_list, NULL, key_name, "color+grey", &grey ) ) {

        color = newGreyColor( grey );
    }
    else {

        red = green = blue = 0.0;

        c = 0;
        c += getConfigDouble( config_list, error_list, key_name, "color+red", &red );
        c += getConfigDouble( config_list, error_list, key_name, "color+green", &green );
        c += getConfigDouble( config_list, error_list, key_name, "color+blue", &blue );

        if ( c > 0 )
            color = newColor( red, green, blue );
    }

    return color;
}
