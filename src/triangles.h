/**********************************************************************
* triangles.h
*
* Definitions to support triangle objects for ray tracing
*
**********************************************************************/

#ifndef _TRIANGLES_H
#define _TRIANGLES_H

#include "lists.h"
#include "geometry.h"
#include "objects.h"

/***********************************************************
* the triangle object
***********************************************************/
typedef struct {
    char                    *name;
    pnt_vr                  *vertex1;
    pnt_vr                  *vertex2;
    pnt_vr                  *vertex3;
    dir_vr                  *axis1;
    dir_vr                  *axis2;
    dir_vr                  *normal;
    region                  *screen_region;
    char                    *surface_type;
    illumination_factors    *illumination_factors;
    void                    *surface_properties;
    image_chrome            *chrome;
} triangleObject;

void triangleSetupCallbacks( list *callback_list );
void triangleReleaseSurfaceRenderers();

void triangleLoadObject( list *object_info, list *error_list,
                        list *object_list, illumination_factors *default_factors,
                        screen_info *screen );
double triangleFindIntersection( void *object, ray *sight );
void triangleProcessContact( void *object, ray *sight,
                            pnt_vr *contact, list *object_list );
void triangleFindNormal( void *object, pnt_vr *contact, dir_vr *normal );
char *triangleChrome( void *object, image_chrome *chrome );
region *triangleScreenRegion( void *object );
void triangleRelease( void *object );

/***********************************************************
* surface renderers
***********************************************************/

void triangleSetupSurfaceRenderers();

/***********/
/* default */

typedef struct {
    rgb_color   *color;
} triangle_surface_default;

void *triangleSurfaceDefaultLoad( list *property_list, list *error_list );
void triangleSurfaceDefaultContact( void *object,
                                pnt_vr *contact, rgb_color *contact_color );
void triangleSurfaceDefaultRelease( void *renderer_info );

#endif
