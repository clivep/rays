/**********************************************************************
* illumination.h
*
* Definitions to support lighting for ray tracing
*
**********************************************************************/

#ifndef _ILLUMINATION_H
#define _ILLUMINATION_H

#include "lists.h"
#include "objects.h"
#include "geometry.h"

typedef struct {
    pnt_vr   *centre;
    int      sample_size;
    double   radius;
    double   brightness;
} lamp;

typedef struct {
    double  specular_index;     // specular component to display
    double  specular_exponent; 
    double  diffusion_index;    // diffusion component to display
    double  reflective_index;   // amount of source ray to reflect
    double  color_scramble;
} illumination_factors;

typedef struct {
    double  ambience;
    double  total_brightness;
    double  color_shift;
    list    *lamps;
    list    *objects;
} illumination;

void loadConfigIllumination( list *key_val_list,
                                        list *error_list, list *objects );
illumination_factors *findFactorScheme( char *name );
void loadConfigFactorSchemes( list *key_val_list, list *error_list );
illumination_factors *copyIlluminationFactors( illumination_factors *src );
                                        
illumination_factors *newIlluminationFactors( list *object_key_vals,
                                        illumination_factors *default_factors );
void loadConfigLamp( list *key_val_list, list *error_list );
void freeConfigIllumination();
void freeLamp( void *item );
void freeConfigLamp( void *item );
void setContactPointColor( void *object, pnt_vr *contact_pnt, rgb_color *color,
                                       ray *sight, dir_vr *normal,
                                       illumination_factors *factors );
double findShadowFactor( void *source_object, pnt_vr *contact_pnt,
                                        rgb_color *color );
void colorShift( rgb_color *color );

#endif
