/*********************************************************************
* color.c
*    Methods to support colors for ray tracing
*********************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "common.h"
#include "config.h"
#include "color.h"

#define _RED_FLAG       0x001
#define _GREEN_FLAG     0x002
#define _BLUE_FLAG      0x004

static list *_color_defs = NULL;

/***********************************************************
* loadDefaultColors
*    Load some default color definitions
***********************************************************/

void loadDefaultColors()
{
    if ( _color_defs == NULL )
        _color_defs = initialiseList( free );

    appendToList( _color_defs, "red", newColor( 1.0, 0.0, 0.0 ) );
    appendToList( _color_defs, "green", newColor( 0.0, 1.0, 0.0 ) );
    appendToList( _color_defs, "blue", newColor( 0.0, 0.0, 1.0 ) );

    appendToList( _color_defs, "black", newColor( 0.0, 0.0, 0.0 ) );
    appendToList( _color_defs, "white", newColor( 1.0, 1.0, 1.0 ) );
}

/***********************************************************
* loadConfigCustomColors
*    Load any custom colors from the config file
***********************************************************/

void loadConfigCustomColors
(
    list    *key_val_list,
    list    *error_list
)
{
    int count;
    int line;
    short found;
    char *color_name;
    char str_buf[ 64 ];
    config_list_item *config_item;
    double red, green, blue;
    list_item *item;

    if ( _color_defs == NULL )
        _color_defs = initialiseList( free );

    for ( item = key_val_list->head; item != NULL; item = item->next ) {

        if ( strcmp( item->label, "custom-color-defs+name" ) == 0 ) {
            
            config_item = (config_list_item *)item->data;
            color_name = (char *)config_item->value;
            line = config_item->line_number;

            found = 0;
            for ( count = 0; count < 3; count++ ) {

                if ( ( item = item->next ) == NULL )
                    break;

                config_item = (config_list_item *)item->data;

                if ( strcmp( &item->label[ 22 ], "+red" ) == 0 ) {
                    red = atof( config_item->value );
                    found += _RED_FLAG;
                }
                else if ( strcmp( &item->label[ 22 ], "+green" ) == 0 ) {
                    green = atof( config_item->value );
                    found += _GREEN_FLAG;
                }
                else if ( strcmp( &item->label[ 22 ], "+blue" ) == 0 ) {
                    blue = atof( config_item->value );
                    found += _BLUE_FLAG;
                }
            }

            if ( ( found & _RED_FLAG ) == 0 ) {
                sprintf( str_buf, "ln: %d missing red value", line );
                appendToList( error_list, str_buf, NULL );
            }

            if ( ( found & _GREEN_FLAG ) == 0 ) {
                sprintf( str_buf, "ln: %d missing green value", line );
                appendToList( error_list, str_buf, NULL );
            }

            if ( ( found & _BLUE_FLAG ) == 0 ) {
                sprintf( str_buf, "ln: %d missing blue value", line );
                appendToList( error_list, str_buf, NULL );
            }

            if ( found == ( _RED_FLAG + _GREEN_FLAG + _BLUE_FLAG ) )
                appendToList( _color_defs, color_name, newColor( red, green, blue ) );
        }
    }
}

/***********************************************************
* freeColors
*    Release the color list
***********************************************************/

void freeColors()
{
    if ( _color_defs != NULL )
        freeList( _color_defs );
}

/***********************************************************
* findColor
*    Locate and return the given color name in the colors
*    list. return NULL if not found
***********************************************************/

rgb_color *findColor
(
    char *name
)
{
    rgb_color *color = NULL;
    list_item *item;

    if ( name == NULL )
        return NULL;

    for ( item = _color_defs->head; item != NULL; item = item->next ) {

        if ( strcmp( item->label, name ) == 0 ) {
            color = (rgb_color *)item->data;
            break;
        }
    }

    return color;
}

/***********************************************************
* newColor
*    Create a new color object and populate the values
***********************************************************/

rgb_color *newColor
(
    double   red,
    double   green,
    double   blue
)
{
    rgb_color *color = (rgb_color *)malloc( sizeof( rgb_color ) );
    if ( color == NULL ) {
        printf( "FATAL: malloc() failed in newColor\n" );
        exit( -1 );
    }

    color->red = red;
    color->green = green;
    color->blue = blue;

    return color;
}

/***********************************************************
* addColor
*    Add the given colors
***********************************************************/

void addColor
(
    rgb_color  *color,
    double     red,
    double     green,
    double     blue
)
{
    color->red = colorRange( color->red + red );
    color->green = colorRange( color->green + green );
    color->blue = colorRange( color->blue + blue );
}

/***********************************************************
* setColor
*    Populate the rgb values of the given color
***********************************************************/

void setColor
(
    rgb_color  *color,
    double     red,
    double     green,
    double     blue
)
{
    color->red = red;
    color->green = green;
    color->blue = blue;
}

/***********************************************************
* addFromColor
*    Add one colors' values onto another 
***********************************************************/

void addFromColor
(
    rgb_color  *src_color,
    rgb_color  *dst_color
)
{
    dst_color->red += src_color->red;
    dst_color->green += src_color->green;
    dst_color->blue += src_color->blue;
}

/***********************************************************
* copyFromColor
*    Copy one colors' values into another 
***********************************************************/

void copyFromColor
(
    rgb_color  *src_color,
    rgb_color  *dst_color
)
{
    dst_color->red = src_color->red;
    dst_color->green = src_color->green;
    dst_color->blue = src_color->blue;
}

/***********************************************************
* copyColor
*    Duplicate the given color - caller to free
***********************************************************/

rgb_color *copyColor
(
    rgb_color  *color
)
{
    return newColor( 
                color->red,
                color->green,
                color->blue
    );
}

/***********************************************************
* scrambleColor
*    Scamble the color components
***********************************************************/

void scrambleColor(
    rgb_color   *color,
    double      scramble_factor
)
{
    color->red = colorRange( color->red * ( 1.0 + _RND * scramble_factor ) );
    color->green = colorRange( color->green * ( 1.0 + _RND * scramble_factor ) );
    color->blue = colorRange( color->blue * ( 1.0 + _RND * scramble_factor ) );
}

/***********************************************************
* colorRange
*    Limit the given color component to between 0 and 1
***********************************************************/

double colorRange
(
    double component
)
{
    if ( component > 1.0 )
        return 1.0;

    if ( component < 0.0 )
        return 0.0;

    return component;
}

/***********************************************************
* psColor
*    Return a Postscript color command
***********************************************************/

char *psColor
(
    rgb_color   *color
)
{
    static char buf[ 64 ];
    
    sprintf( buf, "/chrome_color { %f %f %f setrgbcolor } def\n",
                                        color->red, color->green, color->blue
    );

    return buf;
}

/***********************************************************
* printColor
*    Display the color with its label
***********************************************************/

void printColor
( 
    char        *label,
    rgb_color   *color 
)
{
    if ( ( label != NULL ) && ( *label != '\0' ) )
        printf( "%s: ", label );

    printf( "r: %0.4f", color->red );
    printf( ", g: %0.4f", color->green );
    printf( ", b: %0.4f", color->blue );
    printf( "\n" );
}
