/*********************************************************************
* <things>.c
*    Methods to support <things> for ray tracing
*
*   Surface Renderers:
*       default
*********************************************************************/

TODO: add to objects.c setupObjectCallbacks() and releaseObjectCallbacks()
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "lists.h"
#include "math.h"
#include "config.h"
#include "common.h"
#include "traceRay.h"

#include "<thing>.h"

static list *surface_renderers = NULL;
static flexString *str_buf = NULL;

extern coord *pntToScreenCoord( pnt_vr *pnt );

/***********************************************************
* <thing>SetupCallbacks
*    Load the shape callbacks list
***********************************************************/

void <thing>SetupCallbacks
(
   list *callback_list
)
{
    object_callbacks *callbacks;

    callbacks = _ALLOC(object_callbacks);
    if ( callbacks == NULL ) {
        printf( "FATAL calloc() failed in <thing>SetupCallbacks\n" );
        exit( -1 );
    }

    callbacks->objectLoad = <thing>LoadObject;
    callbacks->objectIntersection = <thing>FindIntersection;
    callbacks->objectContact = <thing>ProcessContact;
    callbacks->objectScreenRegion = <thing>ScreenRegion;
    callbacks->objectChrome = <thing>Chrome;
    callbacks->objectRelease = <thing>Release;

    appendToList( callback_list, "<thing>" , callbacks );

    <thing>SetupSurfaceRenderers();
}

void <thing>ReleaseSurfaceRenderers()
{
    if ( surface_renderers != NULL )
        freeList( surface_renderers );
}

/***********************************************************
* <thing>SetupSurfaceRenderers
*    Setup the available surface renderers
*    Return non-zero on error else 0
***********************************************************/

void <thing>SetupSurfaceRenderers()
{
    renderer_callbacks *renderer;
    surface_renderers = initialiseList( free );

    renderer = newSurfaceRenderer(
                            <thing>SurfaceDefaultLoad,
                            <thing>SurfaceDefaultContact,
                            <thing>SurfaceDefaultRelease
    );
    appendToList( surface_renderers, "default", renderer );

    renderer = newSurfaceRenderer(
                            <thing>Surface<something>Load,
                            <thing>Surface<something>Contact,
                            <thing>Surface<something>Release
    );
    appendToList( surface_renderers, "default", renderer );
}

/***********************************************************
* <thing>LoadObject
*    Load a <thing> object from the given object list
*    Return non-zero on error else 0
***********************************************************/

void <thing>LoadObject
(
    list *object_key_vals,
    list *error_list,
    list *object_list,
    list *default_properties,
    screen_info *screen
)
{
    char key_base[] = "objects+object";
    char *surface_type;
    char *<thing>_name;
    list_item *renderer_item;
    renderer_callbacks *renderer;
    config_list_item *config_item;
    <thing>Object *<thing>;
    int init_entries;
    dir_vr *normal_axis;

    if ( str_buf == NULL )
        str_buf = newFlexString();

    init_entries = ( error_list == NULL ) ? 0 : error_list->entries;

    surface_type = getConfigValue( object_key_vals, error_list, key_base, "surface" );

    if ( surface_type == NULL )
        return;

    if ( ( renderer_item = findInList( surface_renderers, surface_type ) ) == NULL ) {
        config_item = (config_list_item * )object_key_vals->head->data;
        printfFlexString( str_buf,
                "ln: %d %s is not a supported <thing> surface type",
                                config_item->line_number, surface_type );

        appendToList( error_list, str_buf->str, NULL );
    }
    renderer = (renderer_callbacks *)renderer_item;

    if ( ( <thing> = _ALLOC(<thing>Object) ) == NULL ) {
        printf( "FATAL calloc() failed in <thing>LoadObject\n" );
        exit( -1 );
    }

    <thing>->surface_type = strdup( surface_type );

    // These parameters are mandatory
    <thing>->origin = newPnt( 0, 0, 0 );
    getConfigDouble( object_key_vals, error_list, key_base, "origin+i", &<thing>->origin->i );
    getConfigDouble( object_key_vals, error_list, key_base, "origin+j", &<thing>->origin->j );
    getConfigDouble( object_key_vals, error_list, key_base, "origin+k", &<thing>->origin->k );

    ...

    // These parameters may be defaulted
    <thing>->name = strdup( <thing>_name );

    <thing>->illumination_factors = newIlluminationFactors( object_key_vals, default_properties );

    // Setup the screen region for this object

    <thing>->screen_region = newRegion( 0, 0, 0, 0 );
    
    setupRegionWithPnt( <thing>->screen_region, <thing>->origin );
    ...
    updateRegionWithPnt( <thing>->screen_region, <some-point> );

    rendererLoad loadMethod = getRendererLoadMethod( surface_renderers, surface_type );
    <thing>->surface_properties = loadMethod( object_key_vals, error_list );

    <thing>->chrome = _ALLOC( image_chrome );
    loadConfigChrome( key_base, object_key_vals, error_list, <thing>->chrome );

    appendToList( object_list, "<thing>", <thing> );
}

/***********************************************************
* <thing>FindIntersection
*    Find an interection point (if any) between the given
*    <thing> and ray
*    Return distance or -1 if no intersection
***********************************************************/

double <thing>FindIntersection
(
    void *object,
    ray *sight
)
{
    <thing>Object *<thing> = (<thing>Object *)object;
    double distance = -1;

    ...

    return distance;
}

/***********************************************************
* <thing>ProcessContact
*    Set the perceived color of the contact point
***********************************************************/

void <thing>ProcessContact
(
    void    *object,
    ray     *sight,
    pnt_vr  *contact,
    list    *object_list
)
{
    <thing>Object *<thing> = (<thing>Object *)object;
    rgb_color contact_color;
    rgb_color *color;
    rendererContact contactMethod;
    double d1;
    dir_vr *reflection_dir;
    double reflective_index;
    ray *reflected_ray;

    setColor( &contact_color, 0.0, 0.0, 0.0 );

    d1 = 2 * dot( sight->dir, <thing>->normal );

    contactMethod = getRendererContactMethod( surface_renderers, <thing>->surface_type );
    contactMethod( object, contact, &contact_color );

    setContactPointColor( object, contact, &contact_color, sight,
                                            <thing>->normal, <thing>->illumination_factors );

    reflective_index = <thing>->illumination_factors->reflective_index;

    if ( reflective_index > 0.0 ) {
   
        // Find the reflected ray
        reflection_dir = newDir(
            sight->dir->i - d1 * <thing>->normal->i,
            sight->dir->j - d1 * <thing>->normal->j,
            sight->dir->k - d1 * <thing>->normal->k
        );
    
        color = newColor( 0.0, 0.0, 0.0 );
    
        reflected_ray = newRay( copyPnt( contact ), reflection_dir, color, NULL );
        movePnt( reflected_ray->src, _SMALL_DIST, reflection_dir );
    
        reflected_ray->strength = sight->strength * reflective_index;
    
        traceRay( reflected_ray, object_list, 'N' );
    
        addColor( sight->color,
                        reflected_ray->color->red,
                        reflected_ray->color->green,
                        reflected_ray->color->blue
        );
    
        if ( reflected_ray != NULL )
            rayRelease( reflected_ray );
    }

    sight->strength = 0.0;  // if not refracting or reflecting then thats it...
}

/***********************************************************
* <thing>ScreenRegion
*    Return the screen region string for the object
***********************************************************/

region *<thing>ScreenRegion
(
    void *object
)
{
    <thing>Object *<thing> = (<thing>Object *)object;

    return <thing>->screen_region;
}

/***********************************************************
* <thing>Chrome
*    Return the chrome string for the object
***********************************************************/

char *<thing>Chrome
(
    void *object,
    image_chrome *chrome
)
{
    coord *screen_pnt[ 4 ];
    coord *axis_pnt[ 3 ];   // origin, axis1, axis2
    coord *origin_pnt;
    coord *normal_pnt;
    pnt_vr tmp_pnt;
    short i;
    <thing>Object *<thing> = (<thing>Object *)object;
    image_chrome *<thing>_chrome = <thing>->chrome;

    setFlexString( str_buf, "" );

    if (
        ( chrome->bounding_box == 'A' ) ||
        ( ( chrome->bounding_box == 'Y' ) && ( <thing>_chrome->bounding_box == 'Y' ) )
    ) {

        if ( <thing>_chrome->bbox_color != NULL )
            addFlexString( str_buf, psColor( <thing>_chrome->bbox_color ) );
        else if ( chrome->bbox_color != NULL )
            addFlexString( str_buf, psColor( chrome->bbox_color ) );

        printfFlexStringCat( str_buf, "(%s) %f %f %f %f chrome_bbox\n",
                                    <thing>->name,
                                    box_pt1, box_pt2...
        );
    }

    if (
        ( chrome->construction == 'A' ) ||
        ( ( chrome->construction == 'Y' ) && ( <thing>_chrome->construction == 'Y' ) )
    ) {

        if ( <thing>_chrome->construction_color != NULL )
            addFlexString( str_buf, psColor( <thing>_chrome->construction_color ) );
        else if ( chrome->construction_color != NULL )
            addFlexString( str_buf, psColor( chrome->construction_color ) );

        printfFlexStringCat( str_buf, 
                            "%f %f chrome_point\n",
                                    pnt_x, pnt_y
        );

        printfFlexStringCat( str_buf,  
                            "%f %f %f %f chrome_line\n",
                                    pnt1_x, pnt1_y,
                                    pnt2_x, pnt2_y
        );
    }

    if (
        ( chrome->normal == 'A' ) ||
        ( ( chrome->normal == 'Y' ) && ( <thing>_chrome->normal == 'Y' ) )
    ) {

        if ( <thing>_chrome->normal_color != NULL )
            addFlexString( str_buf, psColor( <thing>_chrome->normal_color ) );
        else if ( chrome->normal_color != NULL )
            addFlexString( str_buf, psColor( chrome->normal_color ) );

        printfFlexStringCat( str_buf,  
                            "%f %f %f %f chrome_line\n",
                                    pnt1_x, pnt1_y,
                                    pnt2_x, pnt2_y
        );
    }

    return( str_buf->str );
}

/***********************************************************
* <thing>Release
*    Release any reserved memory for the <thing>
***********************************************************/

void <thing>Release
(
    void *object
)
{
    short i;

    <thing>Object *<thing> = (<thing>Object *)object;
    rendererRelease releaseMethod;

    free( <thing>->... );

    if ( <thing>->illumination_factors != NULL )
        free( <thing>->illumination_factors );

    releaseMethod = getRendererReleaseMethod( surface_renderers, <thing>->surface_type );
    releaseMethod( <thing>->surface_properties );

    free( <thing>->surface_type );
    free( <thing>->name );
    free( <thing>->screen_region );

    free( <thing>->chrome );

    free( <thing> );
}

/***********************************************************
* <thing>SurfaceLoadDefaultLoad
*    Load the property info for a plain surface.
*    Return a pointer to a renderer_info struct
***********************************************************/

void *<thing>SurfaceDefaultLoad
(
    list *property_list,
    list *error_list
)
{
    <thing>_surface_default *renderer_info;

    char key_base[] = "objects+object";

    renderer_info = _ALLOC( <thing>_surface_default );
    if ( renderer_info == NULL ) {
        printf( "FATAL calloc() failed in <thing>SurfaceDefaultLoad\n" );
        exit( -1 );
    }

    renderer_info->color = getConfigColor( property_list, error_list, key_base, "" );

    return renderer_info;
}

/***********************************************************
* <thing>SurfaceDefaultContact
*    Find the color of the given point on a checkboard
***********************************************************/

void <thing>SurfaceDefaultContact
(
    void *object,
    pnt_vr *contact,
    rgb_color *contact_color
)
{
    <thing>Object *<thing> = (<thing>Object *)object;

    <thing>_surface_default *info = (<thing>_surface_default *)<thing>->surface_properties;

    copyFromColor( info->color, contact_color );
}

/***********************************************************
* <thing>SurfaceDefaultRelease
*    Release the memory for a checkboard object
***********************************************************/

void <thing>SurfaceDefaultRelease
(
    void *renderer_info
)
{
    <thing>_surface_default *info = (<thing>_surface_default *)renderer_info;

    if ( ( info != NULL ) && ( info->color != NULL ) )
        free( info->color );

    free( info );
}
