/*********************************************************************
* sphere.c
*    Methods to support spheres for ray tracing
*
*   Surface Renderers:
*       default
*********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "lists.h"
#include "math.h"
#include "config.h"
#include "common.h"
#include "spheres.h"
#include "traceRay.h"

static list *surface_renderers;
static flexString *str_buf = NULL;

extern coord *pntToScreenCoord( pnt_vr *pnt );

/***********************************************************
* sphereSetupCallbacks
*    Load the shape callbacks list
***********************************************************/

void sphereSetupCallbacks
(
   list *callback_list
)
{
    object_callbacks *callbacks;

    callbacks = _ALLOC(object_callbacks);
    if ( callbacks == NULL ) {
        printf( "FATAL calloc() failed in sphereSetupCallbacks\n" );
        exit( -1 );
    }

    callbacks->objectLoad = sphereLoadObject;
    callbacks->objectIntersection = sphereFindIntersection;
    callbacks->objectContact = sphereProcessContact;
    callbacks->objectFindNormal = sphereFindNormal;
    callbacks->objectScreenRegion = sphereScreenRegion;
    callbacks->objectChrome = sphereChrome;
    callbacks->objectRelease = sphereRelease;

    appendToList( callback_list, "sphere" , callbacks );

    sphereSetupSurfaceRenderers();
}

void sphereReleaseSurfaceRenderers()
{
    if ( surface_renderers != NULL )
        freeList( surface_renderers );
}

/***********************************************************
* sphereSetupSurfaceRenderers
*    Setup the available surface renderers
*    Return non-zero on error else 0
***********************************************************/

void sphereSetupSurfaceRenderers()
{
    renderer_callbacks *renderer;
    surface_renderers = initialiseList( free );

    renderer = newSurfaceRenderer(
                            sphereSurfaceDefaultLoad,
                            sphereSurfaceDefaultContact,
                            sphereSurfaceDefaultRelease
    );

    appendToList( surface_renderers, "default", renderer );

    renderer = newSurfaceRenderer(
                            sphereSurfaceDefaultLoad,
                            sphereSurfaceDefaultContact,
                            sphereSurfaceDefaultRelease
    );

    appendToList( surface_renderers, "refractive", renderer );

    renderer = newSurfaceRenderer(
                            sphereSurfacePlanetLoad,
                            sphereSurfacePlanetContact,
                            sphereSurfacePlanetRelease
    );
    appendToList( surface_renderers, "planet", renderer );
}

/***********************************************************
* sphereLoadObject
*    Load a sphere object from the given object list
*    Return non-zero on error else 0
***********************************************************/

void sphereLoadObject
(
    list *object_key_vals,
    list *error_list,
    list *object_list,
    illumination_factors *default_factors,
    screen_info *screen
)
{
    char key_base[] = "objects+object";
    char *surface_type;
    char *sphere_name;
    list_item *renderer_item;
    renderer_callbacks *renderer;
    config_list_item *config_item;
    sphereObject *sphere;
    int init_entries;
    double rad;

    if ( str_buf == NULL )
        str_buf = newFlexString();

    init_entries = ( error_list == NULL ) ? 0 : error_list->entries;

    surface_type = getConfigValue( object_key_vals, error_list, key_base, "surface" );

    if ( surface_type == NULL )
        return;

    if ( ( renderer_item = findInList( surface_renderers, surface_type ) ) == NULL ) {
        config_item = (config_list_item * )object_key_vals->head->data;
        printfFlexString( str_buf,
                "ln: %d %s is not a supported sphere surface type",
                                config_item->line_number, surface_type );

        appendToList( error_list, str_buf->str, NULL );

        return;
    }
    renderer = (renderer_callbacks *)renderer_item;

    if ( ( sphere = _ALLOC(sphereObject) ) == NULL ) {
        printf( "FATAL calloc() failed in sphereLoadObject\n" );
        exit( -1 );
    }

    sphere->surface_type = strdup( surface_type );

    // These parameters are mandatory
    sphere->centre = newPnt( 0, 0, 0 );
    getConfigDouble( object_key_vals, error_list, key_base, "centre+i", &sphere->centre->i );
    getConfigDouble( object_key_vals, error_list, key_base, "centre+j", &sphere->centre->j );
    getConfigDouble( object_key_vals, error_list, key_base, "centre+k", &sphere->centre->k );

    getConfigDouble( object_key_vals, error_list, key_base, "radius", &sphere->radius );

    // These parameters are optional
    getConfigDoubleDefault( object_key_vals, NULL, key_base, "refractive_index",
                                                                &sphere->refractive_index, 1.0 );

    sphere_name = getConfigValueDefault( object_key_vals, NULL, key_base, "name", "sphere" );
    sphere->name = strdup( sphere_name );

    sphere->illumination_factors = newIlluminationFactors( object_key_vals, default_factors );

    // Setup the screen region for this object
    sphere->screen_region = newRegion( 0, 0, 0, 0 );

    rad = sphere->radius * screen->eye_dist / pointDistance( sphere->centre, screen->eye );

    setupRegionWithPnt( sphere->screen_region, sphere->centre );
    sphere->screen_region->lx -= rad;
    sphere->screen_region->ly -= rad;
    sphere->screen_region->ux += rad;
    sphere->screen_region->uy += rad;

    rendererLoad loadMethod = getRendererLoadMethod( surface_renderers, surface_type );
    sphere->surface_properties = loadMethod( object_key_vals, error_list );

    sphere->chrome = _ALLOC( image_chrome );
    loadConfigChrome( key_base, object_key_vals, error_list, sphere->chrome );

    appendToList( object_list, "sphere", sphere );
}

/***********************************************************
* sphereFindIntersection
*    Find an interection point (if any) between the given
*    sphere and ray
*    Return distance or -1 if no intersection
***********************************************************/

double sphereFindIntersection
(
    void *object,
    ray *sight
)
{
    sphereObject *sphere = (sphereObject *)object;
    double dist1, dist2;
    double distance = -1;
    double a, b, c;

    a =
        ( sight->dir->i * sight->dir->i ) +
        ( sight->dir->j * sight->dir->j ) +
        ( sight->dir->k * sight->dir->k );

    b =
        2 * sight->dir->i * ( sight->src->i - sphere->centre->i ) +
        2 * sight->dir->j * ( sight->src->j - sphere->centre->j ) +
        2 * sight->dir->k * ( sight->src->k - sphere->centre->k );

    c =
        ( sight->src->i - sphere->centre->i ) * ( sight->src->i - sphere->centre->i ) +
        ( sight->src->j - sphere->centre->j ) * ( sight->src->j - sphere->centre->j ) +
        ( sight->src->k - sphere->centre->k ) * ( sight->src->k - sphere->centre->k ) -
        ( sphere->radius * sphere->radius );

    if ( ( a != 0 ) && ( (b * b) > (4 * a * c) ) ) {

        dist1 = ( -b + sqrt( b * b - 4 * a * c ) ) / ( 2 * a );
        dist2 = ( -b - sqrt( b * b - 4 * a * c ) ) / ( 2 * a );

        if ( dist1 > 0 )
            distance = dist1;

        if ( ( dist2 > 0 ) && ( ( dist1 < 0 ) || (dist2 < dist1 ) ) )
            distance = dist2;
    }

    return distance;
}

/***********************************************************
* sphereFindNormal
*    Return the normal for the given sphere at the given
*    contact point
***********************************************************/

void sphereFindNormal
(
    void *object,
    pnt_vr *point,
    dir_vr *normal
)
{
    sphereObject *sphere = (sphereObject *)object;

    setDir( normal,
               point->i - sphere->centre->i,
               point->j - sphere->centre->j,
               point->k - sphere->centre->k
    );

   normalise( normal );
}

/***********************************************************
* sphereProcessContact
*    Set the perceived color of the contact point
***********************************************************/

void sphereProcessContact
(
    void    *object,
    ray     *sight,
    pnt_vr  *contact,
    list    *object_list
)
{
    sphereObject *sphere = (sphereObject *)object;
    rgb_color contact_color;
    rgb_color *color;
    rendererContact contactMethod;
    dir_vr normal;
    dir_vr *reflection_dir;
    dir_vr *refraction_dir;
    double current_refractive_index;
    double reflective_index;
    double d1, d2;
    short entering;
    ray *reflected_ray;

    sphereFindNormal( sphere, contact, &normal );

    d1 = dot( sight->dir, &normal );

    setColor( &contact_color, 0.0, 0.0, 0.0 );

    contactMethod = getRendererContactMethod( surface_renderers, sphere->surface_type );
    contactMethod( object, contact, &contact_color );

    setContactPointColor( object, contact, &contact_color, sight, 
                                                &normal, sphere->illumination_factors );

    reflective_index = sphere->illumination_factors->reflective_index;

    if ( reflective_index > 0.0 ) {

        // Find the reflected ray
        reflection_dir = newDir(
            sight->dir->i - 2 * d1 * normal.i,
            sight->dir->j - 2 * d1 * normal.j,
            sight->dir->k - 2 * d1 * normal.k
        );

        color = newColor( 0.0, 0.0, 0.0 );

        reflected_ray = newRay( copyPnt( contact ), reflection_dir, color, NULL );
        movePnt( reflected_ray->src, _SMALL_DIST, reflection_dir );

        reflected_ray->strength = sight->strength * reflective_index;

        traceRay( reflected_ray, object_list, 'N' );

        addColor( sight->color,
                        reflected_ray->color->red,
                        reflected_ray->color->green,
                        reflected_ray->color->blue
        );

        if ( reflected_ray != NULL )
            rayRelease( reflected_ray );
    }

    // Adjust the sight ray if this is a refractive object
    if ( strcmp( sphere->surface_type, "refractive" ) == 0 ) {

        // Was the ray contacting from inside or outside the sphere..?
        entering = ( d1 < 0 );

        getLastDoubleFromList( sight->refractive_indices, &current_refractive_index );

        d2 = ( entering ) ? sphere->refractive_index / current_refractive_index :
                                    current_refractive_index / sphere->refractive_index;

        if ( entering ) {
            appendDoubleToList( sight->refractive_indices, sphere->refractive_index );
        }
        else {
            popFromList( sight->refractive_indices );
        }

        refraction_dir = newDir(
            sight->dir->i * d2 + normal.i * d1 * ( 1.0 - d2 ),
            sight->dir->j * d2 + normal.j * d1 * ( 1.0 - d2 ),
            sight->dir->k * d2 + normal.k * d1 * ( 1.0 - d2 )
        );
        
        setPnt( sight->src, contact->i, contact->j, contact->k );
        setDir( sight->dir, refraction_dir->i, refraction_dir->j, refraction_dir->k );
        movePnt( sight->src, _SMALL_DIST, refraction_dir );
    }
    else if ( strcmp( sphere->surface_type, "reflective" ) != 0 ) {

        sight->strength = 0.0;  // if not refracting or reflecting then thats it...
    }
}

/***********************************************************
 * sphereScreenRegion
 *    Return the screen region string for the object
 ***********************************************************/

region *sphereScreenRegion
(
    void *object
)
{
    sphereObject *sphere = (sphereObject *)object;

    return sphere->screen_region;
}

/***********************************************************
* sphereChrome
*    Return the chrome string for the object
***********************************************************/

char *sphereChrome
(
    void *object,
    image_chrome *chrome
)    
{
    coord *centre_pnt;
    sphereObject *sphere = (sphereObject *)object;
    image_chrome *sphere_chrome = sphere->chrome;

    setFlexString( str_buf, "" );

    if (
        ( chrome->bounding_box == 'A' ) ||
        ( ( chrome->bounding_box == 'Y' ) && ( sphere_chrome->bounding_box == 'Y' ) )
    ) {

        if ( sphere_chrome->bbox_color != NULL )
            addFlexString( str_buf, psColor( sphere_chrome->bbox_color ) );
        else if ( chrome->bbox_color != NULL )
            addFlexString( str_buf, psColor( chrome->bbox_color ) );

        printfFlexStringCat( str_buf, "(%s) %f %f %f %f chrome_bbox\n",
                                    sphere->name,
                                    sphere->screen_region->lx,
                                    sphere->screen_region->ly,
                                    sphere->screen_region->ux,
                                    sphere->screen_region->uy
        );
    }

    if (
        ( chrome->construction == 'A' ) ||
        ( ( chrome->construction == 'Y' ) && ( sphere_chrome->construction == 'Y' ) )
    ) {

        if ( sphere_chrome->construction_color != NULL )
            addFlexString( str_buf, psColor( sphere_chrome->construction_color ) );
        else if ( chrome->construction_color != NULL )
            addFlexString( str_buf, psColor( chrome->construction_color ) );

        centre_pnt = pntToScreenCoord( sphere->centre );

        printfFlexStringCat( str_buf,
                            "%f %f chrome_point\n",
                                    centre_pnt->x, centre_pnt->y
        );

        free( centre_pnt );
    }

    return str_buf->str;
}

/***********************************************************
* sphereRelease
*    Release any reserved memory for the sphere
***********************************************************/

void sphereRelease
(
    void *object
)
{
    sphereObject *sphere = (sphereObject *)object;
    rendererRelease releaseMethod;

    if ( sphere->centre != NULL )
        free( sphere->centre );

    if ( sphere->illumination_factors != NULL )
        free( sphere->illumination_factors );

    releaseMethod = getRendererReleaseMethod( surface_renderers, sphere->surface_type );
    if ( releaseMethod != NULL )
        releaseMethod( sphere->surface_properties );

    free( sphere->surface_type );
    free( sphere->name );
    free( sphere->screen_region );

    free( sphere->chrome );

    free( sphere );
}

/***********************************************************
* sphereSurfaceLoadDefaultLoad
*    Load the property info for a plain surface.
*    Return a pointer to a renderer_info struct
***********************************************************/

void *sphereSurfaceDefaultLoad
(
    list *property_list,
    list *error_list
)
{
    sphere_surface_default    *renderer_info;

    char key_base[] = "objects+object";

    renderer_info = _ALLOC( sphere_surface_default );
    if ( renderer_info == NULL ) {
        printf( "FATAL calloc() failed in sphereSurfaceDefaultLoad\n" );
        exit( -1 );
    }

    renderer_info->color = getConfigColor( property_list, error_list, key_base, "" );

    return renderer_info;
}

/***********************************************************
* sphereSurfaceDefaultContact
*    Find the given point color on the default surface
***********************************************************/

void sphereSurfaceDefaultContact
(
    void *object,
    pnt_vr *contact,
    rgb_color *contact_color
)
{
    sphereObject *sphere = (sphereObject *)object;

    sphere_surface_default *info = (sphere_surface_default *)sphere->surface_properties;

    copyFromColor( info->color, contact_color );
}

/***********************************************************
* sphereSurfaceDefaultRelease
*    Release the memory for a default surface object
***********************************************************/

void sphereSurfaceDefaultRelease
(
    void *renderer_info
)
{
    sphere_surface_default *info = (sphere_surface_default *)renderer_info;

    if ( ( info != NULL ) && ( info->color != NULL ) )
        free( info->color );

    free( info );
}

/***********************************************************
* sphereSurfacePlanetLoad
*    Load the property info for a checkboard surface.
*    Return a pointer to a renderer_info struct
***********************************************************/

void *sphereSurfacePlanetLoad
(
    list *property_list,
    list *error_list
)
{
    sphere_surface_planet    *renderer_info;

    char key_base[] = "objects+object";

    renderer_info = _ALLOC( sphere_surface_planet );
    if ( renderer_info == NULL ) {
        printf( "FATAL calloc() failed in sphereSurfacePlanet\n" );
        exit( -1 );
    }

    renderer_info->color = getConfigColor( property_list, error_list, key_base, "" );
    renderer_info->north_pole = newDir( 0.0, 0.0, 0.0 );

    getConfigDouble( property_list, error_list, key_base, "north_pole+i", &renderer_info->north_pole->i );
    getConfigDouble( property_list, error_list, key_base, "north_pole+j", &renderer_info->north_pole->j );
    getConfigDouble( property_list, error_list, key_base, "north_pole+k", &renderer_info->north_pole->k );

    return renderer_info;
}

/***********************************************************
* sphereSurfacePlanetContact
*    Find the color of the given point on a planet surface
***********************************************************/

void sphereSurfacePlanetContact
(
    void *object,
    pnt_vr *contact,
    rgb_color *contact_color
)
{
    sphereObject *sphere = (sphereObject *)object;
    double ang, cos_ang;
    double v1, v2;
    dir_vr *normal;
    sphere_surface_planet *renderer_info;

    renderer_info = (sphere_surface_planet *)sphere->surface_properties;

    normal = newDir( 
            contact->i - sphere->centre->i,
            contact->j - sphere->centre->j,
            contact->k - sphere->centre->k
    );
    normalise( normal );

    cos_ang = dot( renderer_info->north_pole, normal );
    ang = 2 * acos( cos_ang ) / _PI;

    v1 = 1 + (
        sin( 3 * ang ) +
        sin( 5 * ang ) +
        sin( 7 * ang )
    ) / 6;

    v2 = 1 + (
        sin( 11 * ang ) +
        sin( 13 * ang ) +
        sin( 17 * ang ) +
        sin( 19 * ang )
    ) / 8;

    copyFromColor( renderer_info->color, contact_color );

    contact_color->red *= v1;
    contact_color->green *= v1;
    contact_color->blue *= v2;
printf("%d %d %d\n", 
            (int)(contact_color->red*256),
            (int)(contact_color->green*256),
            (int)(contact_color->blue*256)
);
    colorShift( contact_color );

    free( normal );
}

/***********************************************************
 * sphereSurfacePlanetRelease
 *    Release the memory for a checkboard object
 ***********************************************************/

void sphereSurfacePlanetRelease
(
    void *renderer_info
)
{
    sphere_surface_planet *planet = (sphere_surface_planet *)renderer_info;

    if ( planet->color != NULL )
        free( planet->color );

    if ( planet->north_pole != NULL )
        free( planet->north_pole );

    free( planet );
}
