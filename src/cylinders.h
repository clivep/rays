/**********************************************************************
* cylinders.h
*
* Definitions to support cylinder objects for ray tracing
*
**********************************************************************/

#ifndef _CYLINDERS_H
#define _CYLINDERS_H

#include "lists.h"
#include "geometry.h"
#include "objects.h"

/***********************************************************
* the cylinder object
***********************************************************/
typedef struct {
    char                    *name;
    pnt_vr                  *axis_start;
    pnt_vr                  *axis_end;
    double                  axis_length;
    double                  radius;
    region                  *screen_region;
    char                    *surface_type;
    illumination_factors    *illumination_factors;
    void                    *surface_properties;
    image_chrome            *chrome;
    double                  *cache;
} cylinderObject;

void cylinderSetupCallbacks( list *callback_list );
void cylinderReleaseSurfaceRenderers();

void cylinderLoadObject( list *object_info, list *error_list,
                        list *object_list, illumination_factors *default_factors,
                        screen_info *screen );
double cylinderFindIntersection( void *object, ray *sight );
void cylinderProcessContact( void *object, ray *sight,
                        pnt_vr *contact, list *object_list );
void cylinderFindNormal( void *object, pnt_vr *contact, dir_vr *normal );
char *cylinderChrome( void *object, image_chrome *chrome );
region *cylinderScreenRegion( void *object );
void cylinderRelease( void *object );

/***********************************************************
* surface renderers
***********************************************************/

void cylinderSetupSurfaceRenderers();

/***********/
/* default */

typedef struct {
    rgb_color   *color;
} cylinder_surface_default;

void *cylinderSurfaceDefaultLoad( list *property_list, list *error_list );
void cylinderSurfaceDefaultContact( void *object,
                                pnt_vr *contact, rgb_color *contact_color );
void cylinderSurfaceDefaultRelease( void *renderer_info );

#endif
