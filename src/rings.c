/*********************************************************************
* rings.c
*    Methods to support rings for ray tracing
*
*   Surface Renderers:
*       default
*********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "lists.h"
#include "math.h"
#include "config.h"
#include "common.h"
#include "rings.h"
#include "traceRay.h"

static list *surface_renderers = NULL;
static flexString *str_buf = NULL;

extern coord *pntToScreenCoord( pnt_vr *pnt );

/***********************************************************
* ringSetupCallbacks
*    Load the shape callbacks list
***********************************************************/

void ringSetupCallbacks
(
   list *callback_list
)
{
    object_callbacks *callbacks;

    callbacks = _ALLOC(object_callbacks);
    if ( callbacks == NULL ) {
        printf( "FATAL calloc() failed in ringSetupCallbacks\n" );
        exit( -1 );
    }

    callbacks->objectLoad = ringLoadObject;
    callbacks->objectIntersection = ringFindIntersection;
    callbacks->objectContact = ringProcessContact;
    callbacks->objectScreenRegion = ringScreenRegion;
    callbacks->objectChrome = ringChrome;
    callbacks->objectRelease = ringRelease;

    appendToList( callback_list, "ring" , callbacks );

    ringSetupSurfaceRenderers();
}

void ringReleaseSurfaceRenderers()
{
    if ( surface_renderers != NULL )
        freeList( surface_renderers );
}

/***********************************************************
* ringSetupSurfaceRenderers
*    Setup the available surface renderers
*    Return non-zero on error else 0
***********************************************************/

void ringSetupSurfaceRenderers()
{
    renderer_callbacks *renderer;
    surface_renderers = initialiseList( free );

    renderer = newSurfaceRenderer(
                            ringSurfaceDefaultLoad,
                            ringSurfaceDefaultContact,
                            ringSurfaceDefaultRelease
    );
    appendToList( surface_renderers, "default", renderer );
}

/***********************************************************
* ringLoadObject
*    Load a ring object from the given object list
*    Return non-zero on error else 0
***********************************************************/

void ringLoadObject
(
    list *object_key_vals,
    list *error_list,
    list *object_list,
    illumination_factors *default_factors,
    screen_info *screen
)
{
    char key_base[] = "objects+object";
    char *surface_type;
    char *ring_name;
    list_item *renderer_item;
    renderer_callbacks *renderer;
    config_list_item *config_item;
    ringObject *ring;
    int init_entries;
    dir_vr *normal_axis;

    if ( str_buf == NULL )
        str_buf = newFlexString();

    init_entries = ( error_list == NULL ) ? 0 : error_list->entries;

    surface_type = getConfigValue( object_key_vals, error_list, key_base, "surface" );

    if ( surface_type == NULL )
        return;

    if ( ( renderer_item = findInList( surface_renderers, surface_type ) ) == NULL ) {
        config_item = (config_list_item * )object_key_vals->head->data;
        printfFlexString( str_buf,
                "ln: %d %s is not a supported ring surface type",
                                config_item->line_number, surface_type );

        appendToList( error_list, str_buf->str, NULL );
    }
    renderer = (renderer_callbacks *)renderer_item;

    if ( ( ring = _ALLOC(ringObject) ) == NULL ) {
        printf( "FATAL calloc() failed in ringLoadObject\n" );
        exit( -1 );
    }

    ring->surface_type = strdup( surface_type );

    // These parameters are mandatory
    ring->origin = newPnt( 0, 0, 0 );
    getConfigDouble( object_key_vals, error_list, key_base, "origin+i", &ring->origin->i );
    getConfigDouble( object_key_vals, error_list, key_base, "origin+j", &ring->origin->j );
    getConfigDouble( object_key_vals, error_list, key_base, "origin+k", &ring->origin->k );

    ring->axis1 = newDir( 0, 0, 0 );
    getConfigDouble( object_key_vals, error_list, key_base, "axis1+i", &ring->axis1->i );
    getConfigDouble( object_key_vals, error_list, key_base, "axis1+j", &ring->axis1->j );
    getConfigDouble( object_key_vals, error_list, key_base, "axis1+k", &ring->axis1->k );
    normalise( ring->axis1 );

    ring->axis2 = newDir( 0, 0, 0 );
    getConfigDouble( object_key_vals, error_list, key_base, "axis2+i", &ring->axis2->i );
    getConfigDouble( object_key_vals, error_list, key_base, "axis2+j", &ring->axis2->j );
    getConfigDouble( object_key_vals, error_list, key_base, "axis2+k", &ring->axis2->k );
    normalise( ring->axis2 );

    getConfigDouble( object_key_vals, error_list, key_base, "inner_radius", &ring->inner_radius );
    getConfigDouble( object_key_vals, error_list, key_base, "outer_radius", &ring->outer_radius );

    ring->normal = cross( ring->axis1, ring->axis2 );
    normalise( ring->normal );

    // These parameters may be defaulted
    ring_name = getConfigValueDefault( object_key_vals, NULL, key_base, "name", "ring" );
    ring->name = strdup( ring_name );

    ring->illumination_factors = newIlluminationFactors( object_key_vals, default_factors );

    // Setup the screen region for this object

    ring->screen_region = newRegion( 0, 0, 0, 0 );
    
    normal_axis = cross( ring->axis1, ring->normal );

    setupRegionWithPnt( ring->screen_region, ring->origin );

    ring->plane_limits[ 0 ] = copyPnt( ring->origin );
    movePnt( ring->plane_limits[ 0 ], 1.0 * ring->outer_radius, ring->axis1 );
    movePnt( ring->plane_limits[ 0 ], 1.0 * ring->outer_radius, normal_axis );
    updateRegionWithPnt( ring->screen_region, ring->plane_limits[ 0 ] );

    ring->plane_limits[ 1 ] = copyPnt( ring->origin );
    movePnt( ring->plane_limits[ 1 ], -1.0 * ring->outer_radius, ring->axis1 );
    movePnt( ring->plane_limits[ 1 ], 1.0 * ring->outer_radius, normal_axis );
    updateRegionWithPnt( ring->screen_region, ring->plane_limits[ 1 ] );

    ring->plane_limits[ 2 ] = copyPnt( ring->origin );
    movePnt( ring->plane_limits[ 2 ], -1.0 * ring->outer_radius, ring->axis1 );
    movePnt( ring->plane_limits[ 2 ], -1.0 * ring->outer_radius, normal_axis );
    updateRegionWithPnt( ring->screen_region, ring->plane_limits[ 2 ] );

    ring->plane_limits[ 3 ] = copyPnt( ring->origin );
    movePnt( ring->plane_limits[ 3 ], 1.0 * ring->outer_radius, ring->axis1 );
    movePnt( ring->plane_limits[ 3 ], -1.0 * ring->outer_radius, normal_axis );
    updateRegionWithPnt( ring->screen_region, ring->plane_limits[ 3 ] );

    free( normal_axis );

    rendererLoad loadMethod = getRendererLoadMethod( surface_renderers, surface_type );
    ring->surface_properties = loadMethod( object_key_vals, error_list );

    ring->chrome = _ALLOC( image_chrome );
    loadConfigChrome( key_base, object_key_vals, error_list, ring->chrome );

    appendToList( object_list, "ring", ring );
}

/***********************************************************
* ringFindIntersection
*    Find an interection point (if any) between the given
*    ring and ray
*    Return distance or -1 if no intersection
***********************************************************/

double ringFindIntersection
(
    void *object,
    ray *sight
)
{
    ringObject *ring = (ringObject *)object;
    double distance = -1;
    double contact_distance;
    double radius;
    pnt_vr contact_pnt;
    dir_vr values;
    dir_vr *soln;

    setDir( &values,
        sight->src->i - ring->origin->i,
        sight->src->j - ring->origin->j,
        sight->src->k - ring->origin->k
    );

    soln = matrixSoln( ring->axis1, ring->axis2, sight->dir, &values );

    if ( soln != NULL ) {

        contact_distance = -1 * soln->k;
        rayGetPoint( sight, contact_distance, &contact_pnt );

        radius = pointDistance( &contact_pnt, ring->origin );
        if ( 
            ( radius > ring->inner_radius ) &&
            ( radius < ring->outer_radius )
        )
            distance = contact_distance;

        free( soln );
    }

    return distance;
}

/***********************************************************
* ringProcessContact
*    Set the perceived color of the contact point
***********************************************************/

void ringProcessContact
(
    void    *object,
    ray     *sight,
    pnt_vr  *contact,
    list    *object_list
)
{
    ringObject *ring = (ringObject *)object;
    rgb_color contact_color;
    rgb_color *color;
    rendererContact contactMethod;
    double d1;
    dir_vr *reflection_dir;
    double reflective_index;
    ray *reflected_ray;

    setColor( &contact_color, 0.0, 0.0, 0.0 );

    d1 = 2 * dot( sight->dir, ring->normal );

    contactMethod = getRendererContactMethod( surface_renderers, ring->surface_type );
    contactMethod( object, contact, &contact_color );

    setContactPointColor( object, contact, &contact_color, sight,
                                            ring->normal, ring->illumination_factors );

    reflective_index = ring->illumination_factors->reflective_index;

    if ( reflective_index > 0.0 ) {
   
        // Find the reflected ray
        reflection_dir = newDir(
            sight->dir->i - d1 * ring->normal->i,
            sight->dir->j - d1 * ring->normal->j,
            sight->dir->k - d1 * ring->normal->k
        );
    
        color = newColor( 0.0, 0.0, 0.0 );
    
        reflected_ray = newRay( copyPnt( contact ), reflection_dir, color, NULL );
        movePnt( reflected_ray->src, _SMALL_DIST, reflection_dir );
    
        reflected_ray->strength = sight->strength * reflective_index;
    
        traceRay( reflected_ray, object_list, 'N' );
    
        addColor( sight->color,
                        reflected_ray->color->red,
                        reflected_ray->color->green,
                        reflected_ray->color->blue
        );
    
        if ( reflected_ray != NULL )
            rayRelease( reflected_ray );
    }

    sight->strength = 0.0;  // if not refracting or reflecting then thats it...
}

/***********************************************************
* ringScreenRegion
*    Return the screen region string for the object
***********************************************************/

region *ringScreenRegion
(
    void *object
)
{
    ringObject *ring = (ringObject *)object;

    return ring->screen_region;
}

/***********************************************************
* ringChrome
*    Return the chrome string for the object
***********************************************************/

char *ringChrome
(
    void *object,
    image_chrome *chrome
)
{
    coord *screen_pnt[ 4 ];
    coord *axis_pnt[ 3 ];   // origin, axis1, axis2
    coord *origin_pnt;
    coord *normal_pnt;
    pnt_vr tmp_pnt;
    short i;
    ringObject *ring = (ringObject *)object;
    image_chrome *ring_chrome = ring->chrome;

    setFlexString( str_buf, "" );

    for( i = 0; i < 4; i++ )
        screen_pnt[ i ] = pntToScreenCoord( ring->plane_limits[ i ] );

    if (
        ( chrome->bounding_box == 'A' ) ||
        ( ( chrome->bounding_box == 'Y' ) && ( ring_chrome->bounding_box == 'Y' ) )
    ) {

        if ( ring_chrome->bbox_color != NULL )
            addFlexString( str_buf, psColor( ring_chrome->bbox_color ) );
        else if ( chrome->bbox_color != NULL )
            addFlexString( str_buf, psColor( chrome->bbox_color ) );

        printfFlexStringCat( str_buf, "(%s) %f %f %f %f chrome_bbox\n",
                                    ring->name,
                                    ring->screen_region->lx,
                                    ring->screen_region->ly,
                                    ring->screen_region->ux,
                                    ring->screen_region->uy
        );
    }

    if (
        ( chrome->construction == 'A' ) ||
        ( ( chrome->construction == 'Y' ) && ( ring_chrome->construction == 'Y' ) )
    ) {

        if ( ring_chrome->construction_color != NULL )
            addFlexString( str_buf, psColor( ring_chrome->construction_color ) );
        else if ( chrome->construction_color != NULL )
            addFlexString( str_buf, psColor( chrome->construction_color ) );

        printfFlexStringCat( str_buf, 
                            "%f %f chrome_point\n"
                            "%f %f chrome_point\n"
                            "%f %f chrome_point\n"
                            "%f %f chrome_point\n",
                                    screen_pnt[ 0 ]->x, screen_pnt[ 0 ]->y,
                                    screen_pnt[ 1 ]->x, screen_pnt[ 1 ]->y,
                                    screen_pnt[ 2 ]->x, screen_pnt[ 2 ]->y,
                                    screen_pnt[ 3 ]->x, screen_pnt[ 3 ]->y
        );

        origin_pnt = pntToScreenCoord( ring->origin );

        setPntFromPnt( &tmp_pnt, ring->origin );
        movePnt( &tmp_pnt, ring->outer_radius / 2.0, ring->axis1 );
        axis_pnt[ 0 ] = pntToScreenCoord( &tmp_pnt );

        setPntFromPnt( &tmp_pnt, ring->origin );
        movePnt( &tmp_pnt, ring->outer_radius / 2.0, ring->axis2 );
        axis_pnt[ 1 ] = pntToScreenCoord( &tmp_pnt );

        printfFlexStringCat( str_buf,  
                            "%f %f %f %f chrome_line\n"
                            "%f %f %f %f chrome_line\n",
                                    origin_pnt->x, origin_pnt->y,
                                    axis_pnt[ 0 ]->x, axis_pnt[ 0 ]->y,
                                    origin_pnt->x, origin_pnt->y,
                                    axis_pnt[ 1 ]->x, axis_pnt[ 1 ]->y
        );

        free( origin_pnt );
        free( axis_pnt[ 0 ] );
        free( axis_pnt[ 1 ] );
    }

    if (
        ( chrome->normal == 'A' ) ||
        ( ( chrome->normal == 'Y' ) && ( ring_chrome->normal == 'Y' ) )
    ) {

        if ( ring_chrome->normal_color != NULL )
            addFlexString( str_buf, psColor( ring_chrome->normal_color ) );
        else if ( chrome->normal_color != NULL )
            addFlexString( str_buf, psColor( chrome->normal_color ) );

        setPntFromPnt( &tmp_pnt, ring->origin );
        movePnt( &tmp_pnt, ring->outer_radius / 2.0, ring->normal );
        normal_pnt = pntToScreenCoord( &tmp_pnt );

        origin_pnt = pntToScreenCoord( ring->origin );

        printfFlexStringCat( str_buf,  
                            "%f %f %f %f chrome_line\n",
                                    origin_pnt->x, origin_pnt->y,
                                    normal_pnt->x, normal_pnt->y
        );

        free( origin_pnt );
        free( normal_pnt );
    }

    for( i = 0; i < 4; i++ )
        free( screen_pnt[ i ] );

    return( str_buf->str );
}

/***********************************************************
* ringRelease
*    Release any reserved memory for the ring
***********************************************************/

void ringRelease
(
    void *object
)
{
    short i;

    ringObject *ring = (ringObject *)object;
    rendererRelease releaseMethod;

    free( ring->origin );
    free( ring->axis1 );
    free( ring->axis2 );
    free( ring->normal );

    if ( ring->illumination_factors != NULL )
        free( ring->illumination_factors );

    releaseMethod = getRendererReleaseMethod( surface_renderers, ring->surface_type );
    releaseMethod( ring->surface_properties );

    for( i = 0; i < 4; i++ )
        free( ring->plane_limits[ i ] );

    free( ring->surface_type );
    free( ring->name );
    free( ring->screen_region );

    free( ring->chrome );

    free( ring );
}

/***********************************************************
* ringSurfaceLoadDefaultLoad
*    Load the property info for a plain surface.
*    Return a pointer to a renderer_info struct
***********************************************************/

void *ringSurfaceDefaultLoad
(
    list *property_list,
    list *error_list
)
{
    ring_surface_default *renderer_info;

    char key_base[] = "objects+object";

    renderer_info = _ALLOC( ring_surface_default );
    if ( renderer_info == NULL ) {
        printf( "FATAL calloc() failed in ringSurfaceDefaultLoad\n" );
        exit( -1 );
    }

    renderer_info->color = getConfigColor( property_list, error_list, key_base, "" );

    return renderer_info;
}

/***********************************************************
* ringSurfaceDefaultContact
*    Find the color of the given point on a checkboard
***********************************************************/

void ringSurfaceDefaultContact
(
    void *object,
    pnt_vr *contact,
    rgb_color *contact_color
)
{
    ringObject *ring = (ringObject *)object;

    ring_surface_default *info = (ring_surface_default *)ring->surface_properties;

    copyFromColor( info->color, contact_color );
}

/***********************************************************
* ringSurfaceDefaultRelease
*    Release the memory for a checkboard object
***********************************************************/

void ringSurfaceDefaultRelease
(
    void *renderer_info
)
{
    ring_surface_default *info = (ring_surface_default *)renderer_info;

    if ( ( info != NULL ) && ( info->color != NULL ) )
        free( info->color );

    free( info );
}
