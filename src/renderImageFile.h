/**********************************************************************
* renderImageFile.h
*
* Definitions to support image generation to a file
*
**********************************************************************/

#ifndef _RENDER_FILE_H
#define _RENDER_FILE_H

#include "color.h"
#include "objects.h"
#include "lists.h"

typedef struct {
    char *      type;
    int         file_desc;
    char        *temp_path;
    char        *image_path;
    char        *conversion_command;
    image_info  *image;
} render_file_info;

void renderImageFileSetupCallbacks( list *callback_list );

void renderImageFileInit( image_info *image, char *image_name,
                                    char *type, char *conversion_command );

void renderImageFilePoint( rgb_color *point_color );
void renderImageFileEndOfLine();
void renderImageFileEndOfImage( list *objects );
void renderImageFileRelease();

#endif
