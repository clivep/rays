/*********************************************************************
* rays.c
*    Ray tracing
*********************************************************************/
 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>

#include "geometry.h"
#include "render.h"
#include "rays.h"
#include "traceRay.h"
 
static setup   environment;

/***********************************************************
* main loop
*    Setup the environment, collect the running parameters
*    and process the image
***********************************************************/
 
int main(
    int   argc,
    char  **argv
)
{
    char *config_file = "rays.cfg"; // the default
    char *output = "screen";
    int c;
    int retval = 0;

    srand( time( NULL ) );

    memset( &environment, '\0', sizeof( setup ) );
    environment.metrics = 0;

    for( c = 1; c < argc; c++ ) {

        if ( strcmp( argv[ c ], "-g" ) == 0 ) {
            environment.chrome = 1;
            continue;
        }

        if ( strcmp( argv[ c ], "-c" ) == 0 ) {
            c++;
            if ( c == argc ) {
                printf( "Missing parameter to -c\n" );
                retval = -1;
                continue;
            }
            else
                config_file = argv[ c ];
        }

        if ( strcmp( argv[ c ], "-f" ) == 0 ) {

            c++;
            if ( c == argc ) {
                printf( "Missing parameter to -f\n" );
                retval = -1;
                continue;
            }
            else
                output = argv[ c ];
        }

        if ( strcmp( argv[ c ], "-l" ) == 0 ) {
            environment.list_config = 1;
            continue;
        }

        if ( strcmp( argv[ c ], "-m" ) == 0 ) {
            environment.metrics = 1;
            continue;
        }

        if ( *argv[ c ] == '-' ) {
            printf( "Unknown parameter %s\n", argv[ c ] );
            retval = -1;
        }
    }

    if ( retval == 0 ) {

        if ( ( retval = setupEnvironment( config_file, output ) ) == 0 )
            processImage();
    
        cleanupEnvironment();
    }

    return retval;
}
 
/***********************************************************
* setupEnvironment
*    Read the setup file and populate the parameters
*    Return non-0 on error else 0
***********************************************************/
 
static short setupEnvironment
(
    char *config_file,
    char *output
)
{
    short retval = 0;
    list *key_val_list;
    list *error_list;
    list_item *error_item;
    double units_multiplier;
    char key_base[] = "defaults+background";
    image_info *image;

    environment.output = output;

    environment.screen.centre = newPnt( 0, 0, 0 );
    environment.screen.offset = newPnt( 0, 0, 0 );

    environment.objects = initialiseList( free );
    error_list = initialiseList( free );

    setupObjectCallbacks();

    key_val_list = initialiseList( freeConfigItemList );

    if ( ( retval = loadConfig( config_file, key_val_list, error_list ) ) != 0 ) {
        freeList( key_val_list );
        return retval;
    }

    if ( environment.list_config ) {
        printf( "CONFIG:\n" );
        displayList( key_val_list, configListItemDisplay );
    }

    loadConfigDefaultProperties( key_val_list );

    loadConfigScreen( key_val_list, error_list, &environment.screen );
    if ( strcmp( output, "screen" ) == 0 )
        environment.metrics = 0;
    setupScreen();

    loadConfigImage( key_val_list, error_list, &environment.image );

    loadDefaultColors();
    loadConfigCustomColors( key_val_list, error_list );

    loadConfigFactorSchemes( key_val_list, error_list );

    // Now that the image and screen are loaded - workout the chrome multipliers
    image = &environment.image;
    units_multiplier = getUnitsMultiplier( image->units );
    image->chrome_multiplier[ 0 ] =
                    image->width * units_multiplier / environment.screen.width;
    image->chrome_multiplier[ 1 ] =
                    image->height * units_multiplier / environment.screen.height;

    loadConfigObjects( key_val_list, error_list, environment.objects,
                                        environment.default_factors, &environment.screen );
    loadConfigIllumination( key_val_list, error_list, environment.objects );

    setupRenderCallbacks( &environment.image, output, error_list ); // Will run renderInit()

    environment.background = newColor( 0.0, 0.0, 0.0 );
    getConfigDouble( key_val_list, NULL, key_base, "red", &environment.background->red );
    getConfigDouble( key_val_list, NULL, key_base, "green", &environment.background->green );
    getConfigDouble( key_val_list, NULL, key_base, "blue", &environment.background->blue );

    if ( ( error_item = error_list->head ) != NULL ) {

        printf( "There were errors:\n" );
        while ( error_item != NULL ) {

            printf( "    %s\n", error_item->label );
            error_item = error_item->next;
        }

        retval = -1;
    }

    freeColors();

    freeList( error_list );
    freeList( key_val_list );

    return retval;
}
 
/***********************************************************
* loadConfigDefaultProperties
*    Load any defaults properties from the config list
*    Return non-zero on error else 0
***********************************************************/

void loadConfigDefaultProperties
(
    list *property_key_vals
)
{
    char key_base[] = "defaults+properties";
    double d;
    char *str;

    environment.anti_aliasing = 0;

    if ( ( str = getConfigValue( property_key_vals, NULL, key_base, "anti-alias" ) ) ) {
        if ( strcasecmp( str, "ON" ) == 0 )
            environment.anti_aliasing = 1;
    }

    environment.default_factors = _ALLOC(illumination_factors);
    if ( environment.default_factors == NULL ) {
        printf( "FATAL calloc() failed in loadConfigDefaultProperties\n" );
        exit( -1 );
    }

    if ( getConfigDouble( property_key_vals, NULL, key_base, "diffusion_index", &d ) )
        environment.default_factors->diffusion_index = d;

    if ( getConfigDouble( property_key_vals, NULL, key_base, "reflective_index", &d ) )
        environment.default_factors->reflective_index = d;

    if ( getConfigDouble( property_key_vals, NULL, key_base, "specular_index", &d ) )
        environment.default_factors->specular_index = d;

    if ( getConfigDouble( property_key_vals, NULL, key_base, "specular_exponent", &d ) )
        environment.default_factors->specular_exponent = d;

    if ( getConfigDouble( property_key_vals, NULL, key_base, "color_scramble", &d ) )
        environment.default_factors->color_scramble = d;
}

/***********************************************************
* processImage
*    Render the shapes on the screen
***********************************************************/
 
static void processImage()
{
    image_info *image = &environment.image;
    double x_resolution, y_resolution; 
    ray *sight_ray;
    int x, y;
    int pcent;
    int last_pcent = -1;
    rgb_color *anti_alias_buffer = NULL;
    rgb_color render_color;

    x_resolution = image->width * image->resolution;
    y_resolution = image->height * image->resolution;

    if ( environment.metrics )
        system( "clear" );

    if ( environment.anti_aliasing ) {

        anti_alias_buffer = _NALLOC( x_resolution + 1, rgb_color );

        for ( x = 0; x < x_resolution; x++ ) {

            pcent = (int)( 100.0 * x / x_resolution );
            if ( environment.metrics && ( pcent > last_pcent ) ) {
                printf( "Anti-alias prep: (%d%%)\r", pcent );
                fflush( stdout );
                last_pcent = pcent;
            }

            y = y_resolution - 1;

            if ( x == 0 ) {
                sight_ray = setupSightRay( x, y, PIXEL_TOP_LEFT );
                traceScreenRay( sight_ray, environment.objects );

                copyFromColor( sight_ray->color, &anti_alias_buffer[ x ] );
                rayRelease( sight_ray );
            }

            sight_ray = setupSightRay( x, y, PIXEL_TOP_RIGHT );
            traceScreenRay( sight_ray, environment.objects );

            copyFromColor( sight_ray->color, &anti_alias_buffer[ x ] );
            rayRelease( sight_ray );
        }

        if ( environment.metrics )
            last_pcent = -1;
    }

    for ( y = y_resolution - 1; y > -1; y-- ) {
      
        if ( environment.metrics ) {

            pcent = (int)( 100 * ( y_resolution - y ) / y_resolution );

            if ( pcent > last_pcent ) {
                printf( "Line %d of %d (%d%%)      \r",
                                    (int)( y_resolution - y ), (int)y_resolution, pcent );
                fflush( stdout );
                last_pcent = pcent;
            }
        }

        for ( x = 0; x < x_resolution; x++ ) {

            sight_ray = setupSightRay( x, y, PIXEL_CENTRE );
            traceScreenRay( sight_ray, environment.objects );
            copyFromColor( sight_ray->color, &render_color );

            /* Double the contribution from the centre spot */
            addFromColor( sight_ray->color, &render_color );

            rayRelease( sight_ray );

            if ( environment.anti_aliasing ) {

                addFromColor( &anti_alias_buffer[ x ], &render_color );
                addFromColor( &anti_alias_buffer[ x + 1 ], &render_color );

                if ( x == 0 ) {
                    sight_ray = setupSightRay( x, y, PIXEL_BOTTOM_LEFT );
                    traceScreenRay( sight_ray, environment.objects );

                    addFromColor( sight_ray->color, &render_color );
                    copyFromColor( sight_ray->color, &anti_alias_buffer[ 0 ] );
            
                    rayRelease( sight_ray );
                }
                else {

                    addFromColor( &anti_alias_buffer[ x ], &render_color );
                }

                sight_ray = setupSightRay( x, y, PIXEL_BOTTOM_RIGHT );
                traceScreenRay( sight_ray, environment.objects );

                addFromColor( sight_ray->color, &render_color );

                copyFromColor( sight_ray->color, &anti_alias_buffer[ x + 1 ] );

                rayRelease( sight_ray );

                render_color.red /= 6.0;
                render_color.green /= 6.0;
                render_color.blue /= 6.0;
            }

            runRenderPoint( &render_color );
        }

        runRenderEndOfLine();
    }

    runRenderEndOfImage( environment.chrome ? environment.objects : NULL );

    if ( environment.metrics ) {
        system( "clear" );
        printf( "Done\n" );
    }

    if ( anti_alias_buffer != NULL )
        free( anti_alias_buffer );
}

/***********************************************************
* setupSightRay
*   Return the sight ray travelling through the given
*   screen pixel location
***********************************************************/
 
static ray *setupSightRay
(
    int         x,
    int         y,
    PIXEL_LOC   location
)
{
    image_info *image = &environment.image;
    screen_info *screen = &environment.screen;
    double screen_x, screen_y;
    double resolution_x, resolution_y; 
    double offset_x, offset_y; 
    pnt_vr *screen_pnt;
    dir_vr *sight_dir;
    coord *screen_coord;
    rgb_color *color;

    resolution_x = image->width * image->resolution;
    resolution_y = image->height * image->resolution;

    screen_x = screen->width * ( ( (x + 0.5) / resolution_x ) - 0.5 );
    screen_y = screen->height * ( ( (y + 0.5) / resolution_y ) - 0.5 );

    offset_x = screen->width / ( resolution_x * 2.0 );
    offset_y = screen->height / ( resolution_y * 2.0 );

    switch ( location ) {

        case PIXEL_TOP_LEFT:
            screen_x -= offset_x;
            screen_y += offset_y;
            break;
        case PIXEL_TOP_RIGHT:
            screen_x += offset_x;
            screen_y += offset_y;
            break;
        case PIXEL_BOTTOM_LEFT:
            screen_x -= offset_x;
            screen_y -= offset_y;
            break;
        case PIXEL_BOTTOM_RIGHT:
            screen_x += offset_x;
            screen_y -= offset_y;
            break;
        case PIXEL_CENTRE:
            break;
    }

    screen_pnt = newPnt(
                screen->centre->i + screen->x_axis->i * screen_x + screen->y_axis->i * screen_y,
                screen->centre->j + screen->x_axis->j * screen_x + screen->y_axis->j * screen_y,
                screen->centre->k + screen->x_axis->k * screen_x + screen->y_axis->k * screen_y
    );

    sight_dir = newDir(
                screen_pnt->i - screen->eye->i,
                screen_pnt->j - screen->eye->j,
                screen_pnt->k - screen->eye->k
    );
    normalise( sight_dir );

    color = newColor( 0.0, 0.0, 0.0 );
    screen_coord = newCoord( screen_x, screen_y );

    return newRay( screen_pnt, sight_dir, color, screen_coord );
}

/***********************************************************
* setupScreen
*    Setup the screen location and verctors
***********************************************************/
 
static short setupScreen()
{
    screen_info *screen = &environment.screen;
    dir_vr *plane;
 
    screen->normal = dirFromPnt( screen->centre );
    normalise( screen->normal );
 
    // Find the x dir for the screen - parallel the the xy plane
    if ( ( screen->normal->i == 0 ) && ( screen->normal->j == 0 ) )
        plane = newDir( -1, 0, 0 );
    else
        plane = newDir( 0, 0, 1 );

    screen->x_axis = cross( plane, screen->normal );
    free( plane );

    normalise( screen->x_axis );
 
    screen->y_axis = cross( screen->normal, screen->x_axis );
    normalise( screen->y_axis );
 
    // can also work out the eye pnt now...
    environment.screen.eye = copyPnt( screen->centre );
    movePnt( environment.screen.eye, environment.screen.eye_dist, screen->normal );
 
    addPnt( environment.screen.eye, screen->offset );
    addPnt( screen->centre, screen->offset );

    if ( environment.list_config ) {
        _PRNTVR( "Scrn normal: ", screen->normal );
        _PRNTVR( "Scrn X axis: ", screen->x_axis );
        _PRNTVR( "Scrn Y axis: ", screen->y_axis );
        _PRNTVR( "Eye pos: ", environment.screen.eye );
    }

    return 0;
}
 
/***********************************************************
* cleanupEnvironment
*    Cleanup
***********************************************************/
static void cleanupEnvironment()
{
    char *object_type;
    list_item *object_item;
    objectRelease releaseMethod;
    screen_info *screen;
    image_chrome *chrome;
 
    // Free up the objects
    while ( ( object_item = environment.objects->head ) != NULL ) {

        object_type = object_item->label;
        releaseMethod = getObjectReleaseMethod( object_type );

        if ( releaseMethod != NULL )
            releaseMethod( object_item->data );

        free( object_item->label );

        environment.objects->head = object_item->next;

        free( object_item );
    }

    // Free up the illumination config
    freeConfigIllumination();

    // Other allocations to free up
    releaseObjectCallbacks();

    free( environment.objects );

    if ( environment.default_factors )
        free( environment.default_factors );

    chrome = &environment.image.chrome;
    if ( chrome->bbox_color != NULL )
        free( chrome->bbox_color );
    if ( chrome->construction_color != NULL )
        free( chrome->construction_color );

    screen = &environment.screen;
    free( screen->offset );
    free( screen->centre );
    free( screen->normal );
    free( screen->eye );
    free( screen->x_axis );
    free( screen->y_axis );
}

/***********************************************************
* traceScreenRay
*    Start following the ray - set the screen_region check 
*    so that objects can be initially rejected.
***********************************************************/
 
void traceScreenRay
(
    ray     *sight_ray,
    list    *object_list
)
{
    // If not contacts found - set the backgrond color
    if ( traceRay( sight_ray, object_list, 'Y' ) == 0 )
       copyFromColor( environment.background, sight_ray->color ); 
}

/***********************************************************
* pntToScreenCoord
*    Find the viewed location on the screen for the point
*    Return a pointer to a coord or NULL if no intersection
***********************************************************/
 
coord *pntToScreenCoord
(
    pnt_vr   *pnt
)
{
    coord *scn_coord;   // caller to free
    screen_info *scn = &environment.screen;
 
    scn_coord = _ALLOC( coord );

    // Solve the simultaneous eqns for
    // A.scn_x + B.scn_y + scn_cntr =  p - C.pnt-to-eye

    // shortcuts for the 3x3 matrix elements
    double a =  scn->x_axis->i;
    double b =  scn->y_axis->i;
    double c =  pnt->i - environment.screen.eye->i;

    double d =  scn->x_axis->j;
    double e =  scn->y_axis->j;
    double f =  pnt->j - environment.screen.eye->j;

    double g =  scn->x_axis->k;
    double h =  scn->y_axis->k;
    double i =  pnt->k - environment.screen.eye->k;
    
    double det = a * (e*i - f*h) - b * (d*i - f*g) + c * (d*h - e*g);

    if ( det == 0 )
        return NULL;

    scn_coord->x =
        ( e*i - f*h ) * ( pnt->i - scn->centre->i ) / det +
        ( c*h - b*i ) * ( pnt->j - scn->centre->j ) / det +
        ( b*f - c*e ) * ( pnt->k - scn->centre->k ) / det;
   
    scn_coord->y =
        ( f*g - d*i ) * ( pnt->i - scn->centre->i ) / det +
        ( a*i - c*g ) * ( pnt->j - scn->centre->j ) / det +
        ( c*d - a*f ) * ( pnt->k - scn->centre->k ) / det;
    
    // the 3rd solution is the distance along the pnt-to-eye vr

    //double z =
    //    ( d*h - e*g ) * ( pnt->i - scn->centre->i ) / det +
    //    ( b*g - a*h ) * ( pnt->j - scn->centre->j ) / det +
    //    ( a*e - b*d ) * ( pnt->k - scn->centre->k ) / det;

    return scn_coord;
}
