/*********************************************************************
* cylinders.c
*    Methods to support cylinders for ray tracing
*
*   Surface Renderers:
*       default
*********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "lists.h"
#include "math.h"
#include "config.h"
#include "common.h"
#include "traceRay.h"

#include "cylinders.h"

static list *surface_renderers = NULL;
static flexString *str_buf = NULL;

extern coord *pntToScreenCoord( pnt_vr *pnt );

/***********************************************************
* cylinderSetupCallbacks
*    Load the shape callbacks list
***********************************************************/

void cylinderSetupCallbacks
(
   list *callback_list
)
{
    object_callbacks *callbacks;

    callbacks = _ALLOC(object_callbacks);
    if ( callbacks == NULL ) {
        printf( "FATAL calloc() failed in cylinderSetupCallbacks\n" );
        exit( -1 );
    }

    callbacks->objectLoad = cylinderLoadObject;
    callbacks->objectIntersection = cylinderFindIntersection;
    callbacks->objectContact = cylinderProcessContact;
    callbacks->objectScreenRegion = cylinderScreenRegion;
    callbacks->objectChrome = cylinderChrome;
    callbacks->objectRelease = cylinderRelease;

    appendToList( callback_list, "cylinder" , callbacks );

    cylinderSetupSurfaceRenderers();
}

void cylinderReleaseSurfaceRenderers()
{
    if ( surface_renderers != NULL )
        freeList( surface_renderers );
}

/***********************************************************
* cylinderSetupSurfaceRenderers
*    Setup the available surface renderers
*    Return non-zero on error else 0
***********************************************************/

void cylinderSetupSurfaceRenderers()
{
    renderer_callbacks *renderer;
    surface_renderers = initialiseList( free );

    renderer = newSurfaceRenderer(
                            cylinderSurfaceDefaultLoad,
                            cylinderSurfaceDefaultContact,
                            cylinderSurfaceDefaultRelease
    );
    appendToList( surface_renderers, "default", renderer );
}

/***********************************************************
* cylinderLoadObject
*    Load a cylinder object from the given object list
*    Return non-zero on error else 0
***********************************************************/

void cylinderLoadObject
(
    list                    *object_key_vals,
    list                    *error_list,
    list                    *object_list,
    illumination_factors    *default_factors,
    screen_info             *screen
)
{
    char key_base[] = "objects+object";
    char *surface_type;
    char *cylinder_name;
    double rad;
    coord *screen_axis_start;
    coord *screen_axis_end;
    list_item *renderer_item;
    renderer_callbacks *renderer;
    config_list_item *config_item;
    cylinderObject *cylinder;
    int init_entries;

    if ( str_buf == NULL )
        str_buf = newFlexString();

    init_entries = ( error_list == NULL ) ? 0 : error_list->entries;

    surface_type = getConfigValue( object_key_vals, error_list, key_base, "surface" );

    if ( surface_type == NULL )
        return;

    if ( ( renderer_item = findInList( surface_renderers, surface_type ) ) == NULL ) {
        config_item = (config_list_item * )object_key_vals->head->data;
        printfFlexString( str_buf,
                "ln: %d %s is not a supported cylinder surface type",
                                config_item->line_number, surface_type );

        appendToList( error_list, str_buf->str, NULL );
    }
    renderer = (renderer_callbacks *)renderer_item;

    if ( ( cylinder = _ALLOC(cylinderObject) ) == NULL ) {
        printf( "FATAL alloc failed in cylinderLoadObject\n" );
        exit( -1 );
    }

    if ( ( cylinder->cache = _NALLOC( 8, double ) ) == NULL ) {
        printf( "FATAL cache alloc failed in cylinderLoadObject\n" );
        exit( -1 );
    }

    cylinder->surface_type = strdup( surface_type );

    // These parameters are mandatory
    cylinder->axis_start = newPnt( 0, 0, 0 );
    getConfigDouble( object_key_vals, error_list, key_base, "axis_start+i", &cylinder->axis_start->i );
    getConfigDouble( object_key_vals, error_list, key_base, "axis_start+j", &cylinder->axis_start->j );
    getConfigDouble( object_key_vals, error_list, key_base, "axis_start+k", &cylinder->axis_start->k );

    cylinder->axis_end = newPnt( 0, 0, 0 );
    getConfigDouble( object_key_vals, error_list, key_base, "axis_end+i", &cylinder->axis_end->i );
    getConfigDouble( object_key_vals, error_list, key_base, "axis_end+j", &cylinder->axis_end->j );
    getConfigDouble( object_key_vals, error_list, key_base, "axis_end+k", &cylinder->axis_end->k );

    cylinder->axis_length = pointDistance( cylinder->axis_start, cylinder->axis_end );

    getConfigDouble( object_key_vals, error_list, key_base, "radius", &cylinder->radius );

    // These parameters may be defaulted
    cylinder_name = getConfigValueDefault( object_key_vals, NULL, key_base, "name", "cylinder" );
    cylinder->name = strdup( cylinder_name );

    cylinder->illumination_factors = newIlluminationFactors( object_key_vals, default_factors );

    // Setup the screen region for this object
    cylinder->screen_region = newRegion( 0, 0, 0, 0 );

    setupRegionWithPnt( cylinder->screen_region, cylinder->axis_start );
    updateRegionWithPnt( cylinder->screen_region, cylinder->axis_end );

    screen_axis_start = pntToScreenCoord( cylinder->axis_start );
    screen_axis_end = pntToScreenCoord( cylinder->axis_end );

    rad = cylinder->radius * screen->eye_dist / pointDistance( screen->eye, cylinder->axis_start );
    extendRegionWithOffsets( cylinder->screen_region, screen_axis_start->x, screen_axis_start->y, rad, rad );

    rad = cylinder->radius * screen->eye_dist / pointDistance( screen->eye, cylinder->axis_end );
    extendRegionWithOffsets( cylinder->screen_region, screen_axis_end->x, screen_axis_end->y, rad, rad );

    rendererLoad loadMethod = getRendererLoadMethod( surface_renderers, surface_type );
    cylinder->surface_properties = loadMethod( object_key_vals, error_list );

    cylinder->chrome = _ALLOC( image_chrome );
    loadConfigChrome( key_base, object_key_vals, error_list, cylinder->chrome );

    appendToList( object_list, "cylinder", cylinder );
}

/***********************************************************
* cylinderFindIntersection
*    Find an interection point (if any) between the given
*    cylinder and ray. See calc description at the end
*    Return distance or -1 if no intersection
***********************************************************/

double cylinderFindIntersection
(
    void *object,
    ray *sight
)
{
    cylinderObject *cylinder = (cylinderObject *)object;
    double distance = -1;
    dir_vr *axis;
    double axis_distance;
    dir_vr *tmp_dir1;
    dir_vr *tmp_dir2;
    dir_vr *tmp_dir3;
    double tmp1, tmp2, tmp3;
    short inside;
    double contact, contact1, contact2;
    dir_vr *contact_dir;

    axis = newDir (  // n
        cylinder->axis_end->i - cylinder->axis_start->i,
        cylinder->axis_end->j - cylinder->axis_start->j,
        cylinder->axis_end->k - cylinder->axis_start->k
    );
    normalise( axis );

    tmp_dir1 = newDir ( // E
        sight->src->i - cylinder->axis_start->i,
        sight->src->j - cylinder->axis_start->j,
        sight->src->k - cylinder->axis_start->k
    );

    tmp_dir2 = newDir(  // P
        tmp_dir1->i - dot( tmp_dir1, axis ) * axis->i,
        tmp_dir1->j - dot( tmp_dir1, axis ) * axis->j,
        tmp_dir1->k - dot( tmp_dir1, axis ) * axis->k
    );

    tmp_dir3 = newDir(  // Q
        sight->dir->i - dot( sight->dir, axis ) * axis->i,
        sight->dir->j - dot( sight->dir, axis ) * axis->j,
        sight->dir->k - dot( sight->dir, axis ) * axis->k
    );

    // R, S and T
    tmp1 = ( tmp_dir3->i * tmp_dir3->i ) + ( tmp_dir3->j * tmp_dir3->j ) + ( tmp_dir3->k * tmp_dir3->k );
    tmp2 = ( tmp_dir2->i * tmp_dir3->i ) + ( tmp_dir2->j * tmp_dir3->j ) + ( tmp_dir2->k * tmp_dir3->k );
    tmp3 = ( tmp_dir2->i * tmp_dir2->i ) + ( tmp_dir2->j * tmp_dir2->j ) + ( tmp_dir2->k * tmp_dir2->k )
                                                                  - ( cylinder->radius * cylinder->radius );

    inside = 0; // any first contact will be external..

    if ( ( tmp1 != 0 ) && ( tmp2 * tmp2 > tmp1 * tmp3 ) ) { // any real solutions..?

        contact1 = ( ( -1 * tmp2 ) + sqrt( tmp2 * tmp2 - tmp1 * tmp3 ) ) / tmp1;
        contact2 = ( ( -1 * tmp2 ) - sqrt( tmp2 * tmp2 - tmp1 * tmp3 ) ) / tmp1;

        contact = ( contact1 > contact2 ) ? contact2 : contact1;

        contact_dir = newDir( 
            tmp_dir1->i + ( contact * sight->dir->i ),
            tmp_dir1->j + ( contact * sight->dir->j ),
            tmp_dir1->k + ( contact * sight->dir->k )
        );

        axis_distance = dot( axis, contact_dir );

        free( contact_dir );

        if ( ( contact > 0 ) && ( axis_distance > 0.0 ) && ( axis_distance < cylinder->axis_length ) )

            distance = contact;
        else {

            contact = contact1 + contact2 - contact;    // try the other contact then

            contact_dir = newDir( 
                tmp_dir1->i + ( contact * sight->dir->i ),
                tmp_dir1->j + ( contact * sight->dir->j ),
                tmp_dir1->k + ( contact * sight->dir->k )
            );

            axis_distance = dot( axis, contact_dir );

            free( contact_dir );
        
            if ( ( contact > 0 ) && ( axis_distance > 0.0 ) && ( axis_distance < cylinder->axis_length ) ) {
                inside = 1;
                distance = contact;
            }
        }

        // Store the insideness and axis distance into the cache
        if ( distance > 0 ) {
            cylinder->cache[ 0 ] = axis_distance;
            cylinder->cache[ 1 ] = (double)inside;
        }
    }
    else {
        cylinder->cache[ 0 ] = 0.0;
        cylinder->cache[ 1 ] = 0.0;
    }

    free( axis );
    free( tmp_dir1 );
    free( tmp_dir2 );
    free( tmp_dir3 );

    return distance;
}

/***********************************************************
* cylinderFindNormal
*    Return the normal for the given cylinder at the given
*    contact point. Assumes that the insideness and axis 
*    distance has been stored in the cache
***********************************************************/

void cylinderFindNormal
(
    void    *object,
    pnt_vr  *point,
    dir_vr  *normal
)
{
    pnt_vr  *axis_pnt;
    dir_vr  *axis_dir;
    double  inside;
    cylinderObject *cylinder = (cylinderObject *)object;
    double  axis_distance = cylinder->cache[ 0 ];
    
    axis_dir = newDir( 
        cylinder->axis_end->i - cylinder->axis_start->i,
        cylinder->axis_end->j - cylinder->axis_start->j,
        cylinder->axis_end->k - cylinder->axis_start->k
    );
    normalise( axis_dir );

    axis_pnt = copyPnt( cylinder->axis_start );
    movePnt( axis_pnt, axis_distance, axis_dir );

    // if the contact was internal set this to -1 - else 1
    inside = ( cylinder->cache[ 1 ] * -2.0 ) + 1.0;

    setDir( normal,
        inside * ( point->i - axis_pnt->i ),
        inside * ( point->j - axis_pnt->j ),
        inside * ( point->k - axis_pnt->k )
    );

    normalise( normal );

    free( axis_dir );
    free( axis_pnt );
}

/***********************************************************
* cylinderProcessContact
*    Set the perceived color of the contact point
***********************************************************/

void cylinderProcessContact
(
    void    *object,
    ray     *sight,
    pnt_vr  *contact,
    list    *object_list
)
{
    cylinderObject *cylinder = (cylinderObject *)object;
    rgb_color contact_color;
    rgb_color *color;
    rendererContact contactMethod;
    double d1;
    dir_vr normal;
    dir_vr *reflection_dir;
    double reflective_index;
    ray *reflected_ray;

    cylinderFindNormal( cylinder, contact, &normal );

    d1 = 2 * dot( sight->dir, &normal );

    setColor( &contact_color, 0.0, 0.0, 0.0 );

    contactMethod = getRendererContactMethod( surface_renderers, cylinder->surface_type );
    contactMethod( object, contact, &contact_color );

    //printf("C ");
    setContactPointColor( object, contact, &contact_color, sight,
                                            &normal, cylinder->illumination_factors );

    reflective_index = cylinder->illumination_factors->reflective_index;

    if ( reflective_index > 0.0 ) {
   
        // Find the reflected ray
        reflection_dir = newDir(
            sight->dir->i - d1 * normal.i,
            sight->dir->j - d1 * normal.j,
            sight->dir->k - d1 * normal.k
        );
    
        color = newColor( 0.0, 0.0, 0.0 );
    
        reflected_ray = newRay( copyPnt( contact ), reflection_dir, color, NULL );
        movePnt( reflected_ray->src, _SMALL_DIST, reflection_dir );
    
        reflected_ray->strength = sight->strength * reflective_index;
    
        traceRay( reflected_ray, object_list, 'N' );
    
        addColor( sight->color,
                        reflected_ray->color->red,
                        reflected_ray->color->green,
                        reflected_ray->color->blue
        );

        if ( reflected_ray != NULL )
            rayRelease( reflected_ray );
    }

    sight->strength = 0.0;  // if not refracting or reflecting then thats it...
}

/***********************************************************
* cylinderScreenRegion
*    Return the screen region string for the object
***********************************************************/

region *cylinderScreenRegion
(
    void *object
)
{
    cylinderObject *cylinder = (cylinderObject *)object;

    return cylinder->screen_region;
}

/***********************************************************
* cylinderChrome
*    Return the chrome string for the object
***********************************************************/

char *cylinderChrome
(
    void *object,
    image_chrome *chrome
)
{
    coord *axis_start;
    coord *axis_end;
    cylinderObject *cylinder = (cylinderObject *)object;
    image_chrome *cylinder_chrome = cylinder->chrome;

    setFlexString( str_buf, "" );

    if (
        ( chrome->bounding_box == 'A' ) ||
        ( ( chrome->bounding_box == 'Y' ) && ( cylinder_chrome->bounding_box == 'Y' ) )
    ) {

        if ( cylinder_chrome->bbox_color != NULL )
            addFlexString( str_buf, psColor( cylinder_chrome->bbox_color ) );
        else if ( chrome->bbox_color != NULL )
            addFlexString( str_buf, psColor( chrome->bbox_color ) );

        printfFlexStringCat( str_buf, "(%s) %f %f %f %f chrome_bbox\n",
                                    cylinder->name,
                                    cylinder->screen_region->lx,
                                    cylinder->screen_region->ly,
                                    cylinder->screen_region->ux,
                                    cylinder->screen_region->uy
        );
    }

    if (
        ( chrome->construction == 'A' ) ||
        ( ( chrome->construction == 'Y' ) && ( cylinder_chrome->construction == 'Y' ) )
    ) {

        if ( cylinder_chrome->construction_color != NULL )
            addFlexString( str_buf, psColor( cylinder_chrome->construction_color ) );
        else if ( chrome->construction_color != NULL )
            addFlexString( str_buf, psColor( chrome->construction_color ) );

        axis_start = pntToScreenCoord( cylinder->axis_start );
        axis_end = pntToScreenCoord( cylinder->axis_end );
        printfFlexStringCat( str_buf,  
                            "%f %f %f %f chrome_line\n",
                                    axis_start->x, axis_start->y,
                                    axis_end->x, axis_end->y
        );

        free( axis_start );
        free( axis_end );
    }

    return( str_buf->str );
}

/***********************************************************
* cylinderRelease
*    Release any reserved memory for the cylinder
***********************************************************/

void cylinderRelease
(
    void *object
)
{
    cylinderObject *cylinder = (cylinderObject *)object;
    rendererRelease releaseMethod;

    free( cylinder->axis_start );
    free( cylinder->axis_end );

    if ( cylinder->cache != NULL )
        free( cylinder->cache );

    if ( cylinder->illumination_factors != NULL )
        free( cylinder->illumination_factors );

    releaseMethod = getRendererReleaseMethod( surface_renderers, cylinder->surface_type );
    releaseMethod( cylinder->surface_properties );

    free( cylinder->surface_type );
    free( cylinder->name );
    free( cylinder->screen_region );

    free( cylinder->chrome );

    free( cylinder );
}

/***********************************************************
* cylinderSurfaceLoadDefaultLoad
*    Load the property info for a plain surface.
*    Return a pointer to a renderer_info struct
***********************************************************/

void *cylinderSurfaceDefaultLoad
(
    list *property_list,
    list *error_list
)
{
    cylinder_surface_default *renderer_info;

    char key_base[] = "objects+object";

    renderer_info = _ALLOC( cylinder_surface_default );
    if ( renderer_info == NULL ) {
        printf( "FATAL calloc() failed in cylinderSurfaceDefaultLoad\n" );
        exit( -1 );
    }

    renderer_info->color = getConfigColor( property_list, error_list, key_base, "" );

    return renderer_info;
}

/***********************************************************
* cylinderSurfaceDefaultContact
*    Find the color of the given point on a checkboard
***********************************************************/

void cylinderSurfaceDefaultContact
(
    void *object,
    pnt_vr *contact,
    rgb_color *contact_color
)
{
    cylinderObject *cylinder = (cylinderObject *)object;

    cylinder_surface_default *info = (cylinder_surface_default *)cylinder->surface_properties;

    copyFromColor( info->color, contact_color );
}

/***********************************************************
* cylinderSurfaceDefaultRelease
*    Release the memory for a checkboard object
***********************************************************/

void cylinderSurfaceDefaultRelease
(
    void *renderer_info
)
{
    cylinder_surface_default *info = (cylinder_surface_default *)renderer_info;

    if ( ( info != NULL ) && ( info->color != NULL ) )
        free( info->color );

    free( info );
}

/***********************************************************
* Intersection calculation
*
*
*   p - ray point
*   a - cylinder axis start
*   n - cylinder axis
*
*   solve: |(p-a) - [(p-a).n]n| = r
*
*   where p = e+xf   e is ray start and f is ray dir
*
*   so
*       |(e+xf-a) - [(e+xf-a).n]n| = r  #1
*
*   let E = e-a  - so #1 is now
*
*       |(E-xf) - [(E-xf).n]n | = r     #2
*
*   let A = E.n and B = x(f.n) - so #2 is now
*
*       |(E-xf) - (A+xB)n| = r
*       |(E-An) - x(f+Bn)| = r          #3
*
*   let P = E - An and Q = f + Bn - so #3 is now 
*
*       |P-xQ| = r
*
*       (Pi-xQi)^2 + (Pj-xQj)^2 + (Pk-xQk)^2 = r^2
*       x^2(Qi^2 + Qj^2 + Qk^2) - 2x(PiQi + PjQj + PkQk) + (Pi^2 + Pj^2 + Pk^2) = r^2
*
*   let R = Qi^2 + Qj^2 + Qk^2
*       S = PiQi + PjQj + PkQk
*       T = Pi^2 + Pj^2 + Pk^2 - r^2
*
*   then x = ( 2S +/- SQRT( 4S^2 - 4RT ) ) / 2R
*          = ( S +/- SQRT( S^2 - RT ) ) / R
*
*   so x is the distance(s) along the ray dir (f) that the ray
*   point has perpendicular distance r from the cylinder axis
*
***********************************************************/

