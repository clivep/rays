/**********************************************************************
* renderImageScreen.h
*
* Definitions to support image display on a screen
*
**********************************************************************/

#ifndef _RENDER_SCREEN_H
#define _RENDER_SCREEN_H

#include "color.h"
#include "lists.h"

void renderImageScreenSetupCallbacks( list *callback_list );

void renderImageScreenInit();
void renderImageScreenPoint( rgb_color *point_color );
void renderImageScreenEndOfLine();
void renderImageScreenEndOfImage( list *objects );
void renderImageScreenRelease();

#endif
