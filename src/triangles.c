/*********************************************************************
* triangle.c
*    Methods to support triangles for ray tracing
*
*   Surface Renderers:
*       default
*********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "lists.h"
#include "math.h"
#include "config.h"
#include "common.h"
#include "triangles.h"
#include "traceRay.h"

static list *surface_renderers;
static flexString *str_buf = NULL;

extern coord *pntToScreenCoord( pnt_vr *pnt );

/***********************************************************
* triangleSetupCallbacks
*    Load the shape callbacks list - setup the surface
*    renderer list
***********************************************************/

void triangleSetupCallbacks
(
   list *callback_list
)
{
    object_callbacks *callbacks;

    callbacks = _ALLOC(object_callbacks);
    if ( callbacks == NULL ) {
        printf( "FATAL calloc() failed in triangleSetupCallbacks\n" );
        exit( -1 );
    }

    callbacks->objectLoad = triangleLoadObject;
    callbacks->objectIntersection = triangleFindIntersection;
    callbacks->objectContact = triangleProcessContact;
    callbacks->objectScreenRegion = triangleScreenRegion;
    callbacks->objectChrome = triangleChrome;
    callbacks->objectRelease = triangleRelease;

    appendToList( callback_list, "triangle" , callbacks );

    triangleSetupSurfaceRenderers();
}

void triangleReleaseSurfaceRenderers()
{
    if ( surface_renderers != NULL )
        freeList( surface_renderers );
}

/***********************************************************
* triangleSetupSurfaceRenderers
*    Setup the available surface renderers
*    Return non-zero on error else 0
***********************************************************/

void triangleSetupSurfaceRenderers()
{
    renderer_callbacks *renderer;
    surface_renderers = initialiseList( free );

    renderer = newSurfaceRenderer(
                            triangleSurfaceDefaultLoad,
                            triangleSurfaceDefaultContact,
                            triangleSurfaceDefaultRelease
                            );
    appendToList( surface_renderers, "default", renderer );
}

/***********************************************************
* triangleLoadObject
*    Load a triangle object from the given object list
*    Return non-zero on error else 0
***********************************************************/

void triangleLoadObject
(
    list *object_key_vals,
    list *error_list,
    list *object_list,
    illumination_factors *default_factors,
    screen_info *screen
)
{
    char key_base[] = "objects+object";
    char *surface_type;
    char *triangle_name;
    list_item *renderer_item;
    renderer_callbacks *renderer;
    config_list_item *config_item;
    triangleObject *triangle;
    int init_entries;

    if ( str_buf == NULL )
        str_buf = newFlexString();

    init_entries = ( error_list == NULL ) ? 0 : error_list->entries;

    surface_type = getConfigValue( object_key_vals, error_list, key_base, "surface" );
        
    if ( surface_type == NULL )
        return;

    if ( ( renderer_item = findInList( surface_renderers, surface_type ) ) == NULL ) {
        config_item = (config_list_item * )object_key_vals->head->data;
        printfFlexString( str_buf,
                "ln: %d %s is not a supported triangle surface type",
                                config_item->line_number, surface_type );

        appendToList( error_list, str_buf->str, NULL );

        return;
    }
    renderer = (renderer_callbacks *)renderer_item;

    if ( ( triangle = _ALLOC(triangleObject) ) == NULL ) {
        printf( "FATAL calloc() failed in triangleLoadObject\n" );
        exit( -1 );
    }

    triangle->surface_type = strdup( surface_type );

    // These parameters are mandatory
    triangle->vertex1 = newPnt( 0, 0, 0 );
    getConfigDouble( object_key_vals, error_list, key_base, "vertex1+i", &triangle->vertex1->i );
    getConfigDouble( object_key_vals, error_list, key_base, "vertex1+j", &triangle->vertex1->j );
    getConfigDouble( object_key_vals, error_list, key_base, "vertex1+k", &triangle->vertex1->k );

    triangle->vertex2 = newPnt( 0, 0, 0 );
    getConfigDouble( object_key_vals, error_list, key_base, "vertex2+i", &triangle->vertex2->i );
    getConfigDouble( object_key_vals, error_list, key_base, "vertex2+j", &triangle->vertex2->j );
    getConfigDouble( object_key_vals, error_list, key_base, "vertex2+k", &triangle->vertex2->k );

    triangle->vertex3 = newPnt( 0, 0, 0 );
    getConfigDouble( object_key_vals, error_list, key_base, "vertex3+i", &triangle->vertex3->i );
    getConfigDouble( object_key_vals, error_list, key_base, "vertex3+j", &triangle->vertex3->j );
    getConfigDouble( object_key_vals, error_list, key_base, "vertex3+k", &triangle->vertex3->k );

    triangle->axis1 = newDir( 
                triangle->vertex2->i - triangle->vertex1->i,
                triangle->vertex2->j - triangle->vertex1->j,
                triangle->vertex2->k - triangle->vertex1->k
    );

    triangle->axis2 = newDir( 
                triangle->vertex3->i - triangle->vertex1->i,
                triangle->vertex3->j - triangle->vertex1->j,
                triangle->vertex3->k - triangle->vertex1->k
    );

    // These parameters may be defaulted
    triangle_name = getConfigValueDefault( object_key_vals, NULL, key_base, "name", "triangle" );
    triangle->name = strdup( triangle_name );
    
    triangle->illumination_factors = newIlluminationFactors( object_key_vals, default_factors );
    
    // Find the screen intersections - only populate is we are error free
    triangle->screen_region = newRegion( 0, 0, 0, 0 );
    
    setupRegionWithPnt( triangle->screen_region, triangle->vertex1 );
    updateRegionWithPnt( triangle->screen_region, triangle->vertex2 );
    updateRegionWithPnt( triangle->screen_region, triangle->vertex3 );

    triangle->normal = cross( triangle->axis1, triangle->axis2 );
    normalise( triangle->normal );
    
    rendererLoad loadMethod = getRendererLoadMethod( surface_renderers, surface_type );
    triangle->surface_properties = loadMethod( object_key_vals, error_list );
    
    triangle->chrome = _ALLOC( image_chrome );
    loadConfigChrome( key_base, object_key_vals, error_list, triangle->chrome );

    appendToList( object_list, "triangle", triangle );
}

/***********************************************************
* triangleFindIntersection
*    Find an interection point (if any) between the given
*    triangle and ray
*    Return distance or -1 if no intersection
***********************************************************/

double triangleFindIntersection
(
    void *object,
    ray *sight
)
{
    triangleObject *triangle = (triangleObject *)object;
    double distance = -1;
    double axis1_dist, axis2_dist;
    dir_vr values;
    dir_vr *soln;

    setDir( &values,
        sight->src->i - triangle->vertex1->i,
        sight->src->j - triangle->vertex1->j,
        sight->src->k - triangle->vertex1->k
    );

    soln = matrixSoln( triangle->axis1, triangle->axis2, sight->dir, &values );

    if ( soln != NULL ) {

        axis1_dist = soln->i;
        axis2_dist = soln->j;

        if ( ! (
            ( axis1_dist < 0 ) || ( axis2_dist < 0 ) ||
            ( axis1_dist + axis2_dist > 1 )
        ) )
            distance = -1 * soln->k;

        free( soln );
    }

    return distance;
}

/***********************************************************
* triangleProcessContact
*    Set the perceived color of the contact point
***********************************************************/

void triangleProcessContact
(
    void    *object,
    ray     *sight,
    pnt_vr  *contact,
    list    *object_list
)
{
    triangleObject *triangle = (triangleObject *)object;
    rgb_color contact_color;
    rgb_color *color;
    rendererContact contactMethod;
    double d1;
    dir_vr *reflection_dir;
    double reflective_index;
    ray *reflected_ray;

    setColor( &contact_color, 0.0, 0.0, 0.0 );

    d1 = 2 * dot( sight->dir, triangle->normal );

    contactMethod = getRendererContactMethod( surface_renderers, triangle->surface_type );
    contactMethod( object, contact, &contact_color );

    setContactPointColor( object, contact, &contact_color, sight,
                                            triangle->normal, triangle->illumination_factors );

    reflective_index = triangle->illumination_factors->reflective_index;

    if ( reflective_index > 0.0 ) {

        // Find the reflected ray
        reflection_dir = newDir(
            sight->dir->i - d1 * triangle->normal->i,
            sight->dir->j - d1 * triangle->normal->j,
            sight->dir->k - d1 * triangle->normal->k
        );

        color = newColor( 0.0, 0.0, 0.0 );

        reflected_ray = newRay( copyPnt( contact ), reflection_dir, color, NULL );
        movePnt( reflected_ray->src, _SMALL_DIST, reflection_dir );

        reflected_ray->strength = sight->strength * reflective_index;

        traceRay( reflected_ray, object_list, 'N' );

        addColor( sight->color,
                        reflected_ray->color->red,
                        reflected_ray->color->green,
                        reflected_ray->color->blue
        );

        if ( reflected_ray != NULL )
            rayRelease( reflected_ray );
    }

    sight->strength = 0.0;  // if not refracting or reflecting then thats it...
}

/***********************************************************
 * triangleScreenRegion
 *    Return the screen region string for the object
 ***********************************************************/

region *triangleScreenRegion
(
    void *object
)
{
    triangleObject *triangle = (triangleObject *)object;

    return triangle->screen_region;
}

/***********************************************************
* triangleChrome
*    Return the chrome string for the object
***********************************************************/

char *triangleChrome
(
    void *object,
    image_chrome *chrome
)    
{
    short i;
    coord *screen_pnt[ 3 ];
    triangleObject *triangle = (triangleObject *)object;
    image_chrome *triangle_chrome = triangle->chrome;

    setFlexString( str_buf, "" );

    if (
        ( chrome->construction == 'A' ) ||
        ( ( chrome->construction == 'Y' ) && ( triangle_chrome->construction == 'Y' ) )
    ) {

        if ( triangle_chrome->bbox_color != NULL )
            addFlexString( str_buf, psColor( triangle_chrome->bbox_color ) );
        else if ( chrome->bbox_color != NULL )
            addFlexString( str_buf, psColor( chrome->bbox_color ) );

        printfFlexStringCat( str_buf, "(%s) %f %f %f %f chrome_bbox\n",
                                    triangle->name,
                                    triangle->screen_region->lx,
                                    triangle->screen_region->ly,
                                    triangle->screen_region->ux,
                                    triangle->screen_region->uy
        );
    }

    if (
        ( chrome->construction == 'A' ) ||
        ( ( chrome->construction == 'Y' ) && ( triangle_chrome->construction == 'Y' ) )
    ) {

        if ( triangle_chrome->construction_color != NULL )
            addFlexString( str_buf, psColor( triangle_chrome->construction_color ) );
        else if ( chrome->construction_color != NULL )
            addFlexString( str_buf, psColor( chrome->construction_color ) );

        screen_pnt[ 0 ] = pntToScreenCoord( triangle->vertex1 );
        screen_pnt[ 1 ] = pntToScreenCoord( triangle->vertex2 );
        screen_pnt[ 2 ] = pntToScreenCoord( triangle->vertex3 );

        printfFlexStringCat( str_buf,
                            "%f %f chrome_point\n"
                            "%f %f chrome_point\n"
                            "%f %f chrome_point\n",
                                    screen_pnt[ 0 ]->x, screen_pnt[ 0 ]->y,
                                    screen_pnt[ 1 ]->x, screen_pnt[ 1 ]->y,
                                    screen_pnt[ 2 ]->x, screen_pnt[ 2 ]->y
        );

        for( i = 0; i < 3; i++ )
            free( screen_pnt[ i ] );
    }

    return( str_buf->str );
}

/***********************************************************
* triangleRelease
*    Release any reserved memory for the triangle
***********************************************************/

void triangleRelease
(
    void *object
)
{
    triangleObject *triangle = (triangleObject *)object;
    rendererRelease releaseMethod;

    free( triangle->vertex1 );
    free( triangle->vertex2 );
    free( triangle->vertex3 );
    free( triangle->normal );

    if ( triangle->illumination_factors != NULL )
        free( triangle->illumination_factors );

    releaseMethod = getRendererReleaseMethod( surface_renderers, triangle->surface_type );
    releaseMethod( triangle->surface_properties );

    free( triangle->surface_type );
    free( triangle->name );
    free( triangle->screen_region );

    free( triangle->chrome );

    free( triangle );
}

/***********************************************************
* triangleSurfaceLoadDefaultLoad
*    Load the property info for a plain surface.
*    Return a pointer to a renderer_info struct
***********************************************************/

void *triangleSurfaceDefaultLoad
(
    list *property_list,
    list *error_list
)
{
    triangle_surface_default *renderer_info;

    char key_base[] = "objects+object";

    renderer_info = _ALLOC( triangle_surface_default );
    if ( renderer_info == NULL ) {
        printf( "FATAL calloc() failed in triangleSurfaceDefaultLoad\n" );
        exit( -1 );
    }

    renderer_info->color = getConfigColor( property_list, error_list, key_base, "" );

    return renderer_info;
}

/***********************************************************
* triangleSurfaceDefaultContact
*    Find the color of the given point on a checkboard
***********************************************************/

void triangleSurfaceDefaultContact
(
    void *object,
    pnt_vr *contact,
    rgb_color *contact_color
)
{
    triangleObject *triangle = (triangleObject *)object;

    triangle_surface_default *info = (triangle_surface_default *)triangle->surface_properties;

    copyFromColor( info->color, contact_color );
}

/***********************************************************
* triangleSurfaceDefaultRelease
*    Release the memory for a checkboard object
***********************************************************/

void triangleSurfaceDefaultRelease
(
    void *renderer_info
)
{
    triangle_surface_default *info = (triangle_surface_default *)renderer_info;

    if ( ( info != NULL ) && ( info->color != NULL ) )
        free( info->color );

    free( info );
}

