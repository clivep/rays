/**********************************************************************
* common.h
*
* Common efinitions to support ray tracing
*
**********************************************************************/

#ifndef _COMMON_H
#define _COMMON_H

#include <stdlib.h>
#include <stdarg.h>

typedef struct {
    char    *str;
    int     len;
} flexString;

#define _PI     3.14159

#define _UNITS  "INCHES CM MM PIXELS inches cm mm pixels"

#define _MAX(val1,val2) ( (val1) > (val2) ? (val1) : (val2) )
#define _MIN(val1,val2) ( (val1) < (val2) ? (val1) : (val2) )

#define _MOD(val)    ( (val) < 0 ? (-1 * (val) ) : (val) )

#define _RND    ( ( 2.0 * (double)rand() / RAND_MAX ) - 1.0 )

#define _ALLOC(typ)     (typ *)calloc( 1, sizeof( typ ) )
#define _NALLOC(n,typ)  (typ *)calloc( (n), sizeof( typ ) )

/* Used to start a reflected ray just off a surface */
#define _SMALL_DIST     0.0000001

/* Used to test for simultaneous contact points */
#define _VSMALL_DIST    0.00000005

flexString  *newFlexString();
flexString  *initFlexString( char *init_str );
void        setFlexString( flexString *flex_str, char *init_str );
void        addFlexString( flexString *flex_str, char *str );
void        printfFlexString( flexString *flex_str, char *format, ... );
void        printfFlexStringCat( flexString *flex_str, char *format, ... );
void        releaseFlexString( flexString *flex_str );

double *newDouble( double val );

#endif
