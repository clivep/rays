/**********************************************************************
* planes.h
*
* Definitions to support plane objects for ray tracing
*
**********************************************************************/

#ifndef _PLANES_H
#define _PLANES_H

#include "lists.h"
#include "geometry.h"
#include "objects.h"

/***********************************************************
* the plane object
***********************************************************/
typedef struct {
    char                    *name;
    pnt_vr                  *origin;
    dir_vr                  *axis1;
    dir_vr                  *axis2;
    dir_vr                  *normal;
    region                  *screen_region;
    char                    *surface_type;
    illumination_factors    *illumination_factors;
    void                    *surface_properties;
    image_chrome            *chrome;
} planeObject;

void planeSetupCallbacks( list *callback_list );
void planeReleaseSurfaceRenderers();

void planeLoadObject( list *object_info, list *error_list,
                        list *object_list, illumination_factors *default_factors,
                        screen_info *screen );
double planeFindIntersection( void *object, ray *sight );
void planeProcessContact( void *object, ray *sight,
                        pnt_vr *contact, list *object_list );
void planeFindNormal( void *object, pnt_vr *contact, dir_vr *normal );
char *planeChrome( void *object, image_chrome *chrome );
region *planeScreenRegion( void *object );
void planeRelease( void *object );

/***********************************************************
* surface renderers
***********************************************************/

void planeSetupSurfaceRenderers();

/****************/
/* checkerboard */

typedef struct {
    int         axis1_tiles;
    int         axis2_tiles;
    rgb_color   *tile1_color;
    rgb_color   *tile2_color;
} plane_surface_checkerboard;

void *planeSurfaceCheckerboardLoad( list *property_list, list *error_list );
void planeSurfaceCheckerboardContact( void *object,
                                pnt_vr *contact, rgb_color *contact_color );
void planeSurfaceCheckerboardRelease( void *renderer_info );

/***********/
/* default */

typedef struct {
    rgb_color   *color;
} plane_surface_default;

void *planeSurfaceDefaultLoad( list *property_list, list *error_list );
void planeSurfaceDefaultContact( void *object,
                                pnt_vr *contact, rgb_color *contact_color );
void planeSurfaceDefaultRelease( void *renderer_info );

#endif
