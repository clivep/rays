/*********************************************************************
* lists.c
*    Methods to support lists
**********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "common.h"
#include "lists.h"

/***********************************************************
* initialiseList
*    Setup and return a new list object 
*    Return pointer to the list or NULL
***********************************************************/

list *initialiseList
(
    void (*freeFunc)(void *)
)
{
    list *new_list = _ALLOC(list);

    if ( new_list == NULL ) {
        printf( "FATAL: calloc() failed in initialiseList\n" );
        exit( -1 );
    }
    
    new_list->freeItem = freeFunc;

    return new_list;
}

/***********************************************************
* freeList
*    Free all itmes in the list and then the list itself
***********************************************************/

void freeList
(
    list *list
)
{
    list_item *item;

    while( ( item = list->head ) != NULL ) {

        if ( item->label != NULL )
            free( item->label );

        if ( ( list->freeItem != NULL ) && ( item->data != NULL ) )
            list->freeItem( item->data );

        list->head = item->next;

        free( item );
    }

    free( list );
}

/***********************************************************
* appendToList
*   Add the label and item to the given list
*   Return the list_item appended or NULL on error
***********************************************************/

list_item *appendToList
(
    list    *list,
    char    *label,
    void    *data
)
{
    list_item *item;

    if ( list == NULL )
        return NULL;
        
    item = _ALLOC(list_item);
    if ( item == NULL ) {
        printf( "FATAL: calloc() failed in config_list_item\n" );
        exit( -1 );
    }
        
    if ( label != NULL )
        item->label = strdup( label );

    if ( list->head == NULL ) {
        list->head = item;
        list->tail = item;
    }
    else {
        
        item->prev = list->tail;
        list->tail->next = item;
        list->tail = item;
    }

    item->data = data;
    list->entries++;

    return item;
}

/***********************************************************
* findKeyInList
*   Find the first list item matching the given keys
*   Return the item or NULL if not found
***********************************************************/

list_item *findKeyInList
(
    list    *list,
    char    *key_base,
    char    *key
)
{
    char key_value[ 256 ];

    sprintf( key_value, "%s+%s", key_base, key );

    return findInList( list, key_value );
}

/***********************************************************
* findInList
*   Find the first list item matching the given label
*   Return the item or NULL if not found
***********************************************************/

list_item *findInList
(
    list    *list,
    char    *label
)
{
    list_item *item;

    if ( ( list == NULL ) || ( ( item = list->head ) == NULL ) )
        return NULL;
        
    while ( item != NULL ) {
        
        if ( item->label == NULL )
            continue;

        if ( strcmp( label, item->label ) == 0 )
            return item;

        item = item->next;
    }

    return NULL;
}

/***********************************************************
* replaceInList
*   Replace the entry in the list matching the label. Free
*   any existing data for that label.
*   Return the item replaced or NULL on error
***********************************************************/

list_item *replaceInList
(
    list    *list,
    char    *label,
    void    *data
)
{
    list_item *item = findInList( list, label );

    if ( item != NULL ) {
        
        if ( item->data != NULL )
            list->freeItem( item->data );

        item->data = data;

        return item;
    }

    return appendToList( list, label, data );
}

/***********************************************************
* displayList
*   Print the contents of the list
***********************************************************/

void displayList
(
    list    *list,
    void    (*item_display)( void * )
)
{
    list_item *item;

    if ( ( list == NULL ) || ( ( item = list->head ) == NULL ) ) {
        printf( "empty list\n" );
        return;
    }
        
    printf( "list:\n" );
    while ( item != NULL ) {
        
        item_display( item );
        item = item->next;
    }
    printf( "----------------\n" );
}

/***********************************************************
  * appendDoubleToList
  *   Add the given double to the given list
  *   Return the list_item appended or NULL on error
  ***********************************************************/

list_item *appendDoubleToList
(
    list    *list,
    double  value
)
{
    double *pnt = (double *)malloc( sizeof(double) );
    *pnt = value;

    return appendToList( list, NULL, pnt );
}

/***********************************************************
  * popFromList
  *   Remove the last entry from the list
  ***********************************************************/

void popFromList
(
    list    *list
)
{
    list_item *prev_item;

    if ( ( list == NULL ) || ( list->tail == NULL ) )
        return;

    if ( list->tail->label != NULL )
        free( list->tail->label );

    if ( list->tail->data != NULL )
        list->freeItem( list->tail->data );

    prev_item = list->tail->prev;
    free( list->tail );

    prev_item->next = NULL;
    list->tail = prev_item;
}

/***********************************************************
  * getLastDoubleFromList
  *   Recover the last double values from the given list
  *   Return 0 if empty list else 1
  ***********************************************************/

short getLastDoubleFromList
(
    list    *list,
    double  *value
)
{
    double *pnt;

    if ( ( list == NULL ) || ( list->tail == NULL ) )
        return 0;

    pnt = (double *)list->tail->data;
    *value = *pnt;

    return 1;
}

