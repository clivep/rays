#!/bin/sh

for f in `find . -mindepth 2 -name 'build-all.sh'`
do
    date
    cd `dirname $f`
    ./build-all.sh
    cd - > /dev/null 2>&1
done
