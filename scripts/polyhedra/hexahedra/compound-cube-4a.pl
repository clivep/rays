#!/usr/bin/perl

use warnings;
use strict;

use Math::Trig;

################################################################################
#
#   compound4-a.pl
#
#       parameters detailed in usage()
#
################################################################################

do '../utils/utilities.pl';

##########################################
# Setup the working environment

my $context = setupContext();
$context->{ 'OFFSETS' } = [ 0, 0, -2.2 ];

processParameters( $context );

##########################################
# Define the vertices

my $size = 1.8;

my $v0 = [ "v0",   $size,  $size, -$size ];
my $v1 = [ "v1",   $size,  $size,  $size ];
my $v2 = [ "v2",  -$size,  $size,  $size ];
my $v3 = [ "v3",  -$size,  $size, -$size ];
my $v4 = [ "v4",   $size, -$size, -$size ];
my $v5 = [ "v5",   $size, -$size,  $size ];
my $v6 = [ "v6",  -$size, -$size,  $size ];
my $v7 = [ "v7",  -$size, -$size, -$size ];

##########################################
# Define the faces

my @faces;
my $polyhedra = [];
my $p = 0;

push @faces, [ "f1",  $v3, $v2, $v1, $v0, "red" ];
push @faces, [ "f2",  $v0, $v1, $v5, $v4, "red" ];
push @faces, [ "f3",  $v4, $v7, $v3, $v0, "red" ];
push @faces, [ "f4",  $v1, $v2, $v6, $v5, "red" ];
push @faces, [ "f5",  $v2, $v3, $v7, $v6, "red" ];
push @faces, [ "f6",  $v4, $v5, $v6, $v7, "red" ];
push @{ $polyhedra }, [ "p3",
                    $faces[ $p++ ], $faces[ $p++ ], $faces[ $p++ ],
                    $faces[ $p++ ], $faces[ $p++ ], $faces[ $p++ ],
];

push @faces, xRotateFace( [ "f1",  $v3, $v2, $v1, $v0, "green" ], pi / 4.0, 4 );
push @faces, xRotateFace( [ "f2",  $v0, $v1, $v5, $v4, "green" ], pi / 4.0, 4 );
push @faces, xRotateFace( [ "f3",  $v4, $v7, $v3, $v0, "green" ], pi / 4.0, 4 );
push @faces, xRotateFace( [ "f4",  $v1, $v2, $v6, $v5, "green" ], pi / 4.0, 4 );
push @faces, xRotateFace( [ "f5",  $v2, $v3, $v7, $v6, "green" ], pi / 4.0, 4 );
push @faces, xRotateFace( [ "f6",  $v4, $v5, $v6, $v7, "green" ], pi / 4.0, 4 );
push @{ $polyhedra }, [ "p3",
                    $faces[ $p++ ], $faces[ $p++ ], $faces[ $p++ ],
                    $faces[ $p++ ], $faces[ $p++ ], $faces[ $p++ ],
];

push @faces, yRotateFace( [ "f1",  $v3, $v2, $v1, $v0, "blue" ], pi / 4.0, 4 );
push @faces, yRotateFace( [ "f2",  $v0, $v1, $v5, $v4, "blue" ], pi / 4.0, 4 );
push @faces, yRotateFace( [ "f3",  $v4, $v7, $v3, $v0, "blue" ], pi / 4.0, 4 );
push @faces, yRotateFace( [ "f4",  $v1, $v2, $v6, $v5, "blue" ], pi / 4.0, 4 );
push @faces, yRotateFace( [ "f5",  $v2, $v3, $v7, $v6, "blue" ], pi / 4.0, 4 );
push @faces, yRotateFace( [ "f6",  $v4, $v5, $v6, $v7, "blue" ], pi / 4.0, 4 );
push @{ $polyhedra }, [ "p3",
                    $faces[ $p++ ], $faces[ $p++ ], $faces[ $p++ ],
                    $faces[ $p++ ], $faces[ $p++ ], $faces[ $p++ ],
];

push @faces, zRotateFace( [ "f1",  $v3, $v2, $v1, $v0, "yellow" ], pi / 4.0, 4 );
push @faces, zRotateFace( [ "f2",  $v0, $v1, $v5, $v4, "yellow" ], pi / 4.0, 4 );
push @faces, zRotateFace( [ "f3",  $v4, $v7, $v3, $v0, "yellow" ], pi / 4.0, 4 );
push @faces, zRotateFace( [ "f4",  $v1, $v2, $v6, $v5, "yellow" ], pi / 4.0, 4 );
push @faces, zRotateFace( [ "f5",  $v2, $v3, $v7, $v6, "yellow" ], pi / 4.0, 4 );
push @faces, zRotateFace( [ "f6",  $v4, $v5, $v6, $v7, "yellow" ], pi / 4.0, 4 );
push @{ $polyhedra }, [ "p3",
                    $faces[ $p++ ], $faces[ $p++ ], $faces[ $p++ ],
                    $faces[ $p++ ], $faces[ $p++ ], $faces[ $p++ ],
];

fixFaceOrientations( $polyhedra );

##########################################
# Write out the config file

writeConfigHeader( $context );

$context->{ 'FLOOR_Z' } = -1.0 * $size * sqrt( 2.0 );
writeConfigFloor( $context );

writePlaneFaces( $context, $polyhedra );

##########################################
# Display the config if requested

showConfig( $context );

##########################################
# Run the ray-tracer to produce the image

processImage( $context );

