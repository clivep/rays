#!/bin/sh

if [ -z "$1" ]; then
    echo "Usage: $0 <draw script>"
    exit 0
fi

if [ ! -f "$1" ]; then
    echo "Could not find $1"
    exit 0
fi

target=$1
image=`echo $target | sed -e 's/.*\///' -e 's/\.[^\.]*$/.gif/'`

dest=animations/$image

work_dir='tmp-images'

steps=2

mkdir $work_dir > /dev/null 2>&1

rot() {
    ang="000$1"
    ang=`echo $ang | sed -e's/.*\(...\)$/\1/'`

    echo "`date` ang: $ang"

    $target -o $work_dir/tmp_$ang.jpg -r 200 -rx $ang -ry $ang -rz $ang
}

ang=0
while [ $ang -lt 360 ];
do
    rot $ang
    ang=`expr $ang \+ $steps`
done

convert -delay 20 -loop 0 $work_dir/tmp*.jpg $dest
rm tmp-images/tmp*.jpg

echo Done
