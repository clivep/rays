#!/bin/sh -x

mkdir images > /dev/null 2>&1

compound-cube-3a.pl -r 200 -rz 15 -o images/compound-cube-3a.pdf -l 'Compound of 3 Cubes'
compound-cube-4a.pl -r 200 -o images/compound-cube-4a.pdf -l 'Compound of 4 Cubes'
compound-cube-4b.pl -r 200 -o images/compound-cube-4b.pdf -l 'Compound of 4 Cubes'
compound-cube-5a.pl -r 200 -o images/compound-cube-5a.pdf -l 'Compound of 5 Cubes'

snub-cube.pl -r 200 -o images/snub-cube.pdf -rz 30 -l 'Snub Cube'

compound-snub-cube.pl -r 200 -o images/compound-snub-cube.pdf -rz 30 -l 'Compound of 2 Snub Cubes'
