#!/usr/bin/perl

use warnings;
use strict;

use Math::Trig;

################################################################################
#
#   compound5-a.pl
#
#       parameters detailed in usage()
#
################################################################################

do '../utils/utilities.pl';

##########################################
# Setup the working environment

my $context = setupContext();
$context->{ 'OFFSETS' } = [ 0, 0, -2.2 ];

processParameters( $context );

##########################################
# Define the vertices

my $size = 2;

my $_phi = ( 1.0 + sqrt( 5.0 ) ) / 2.0;

my $phi = $size * $_phi;
my $ph2 = $size / $_phi;

my $vlst = [];

$vlst->[ 0 ] = [ (v0),   $size,  $size, -$size ];
$vlst->[ 1 ] = [ (v1),   $size,  $size,  $size ];
$vlst->[ 2 ] = [ (v2),  -$size,  $size,  $size ];
$vlst->[ 3 ] = [ (v3),  -$size,  $size, -$size ];

$vlst->[ 4 ] = [ (v4),   $size, -$size, -$size ];
$vlst->[ 5 ] = [ (v5),   $size, -$size,  $size ];
$vlst->[ 6 ] = [ (v6),  -$size, -$size,  $size ];
$vlst->[ 7 ] = [ (v7),  -$size, -$size, -$size ];

$vlst->[ 8 ]  = [ (v8),      0,   $ph2,  $phi  ];
$vlst->[ 9 ]  = [ (v9),      0,  -$ph2,  $phi  ];
$vlst->[ 10 ] = [ (v10),     0,   $ph2, -$phi  ];
$vlst->[ 11 ] = [ (v11),     0,  -$ph2, -$phi  ];

$vlst->[ 12 ] = [ (v12),  $ph2,   $phi,      0 ];
$vlst->[ 13 ] = [ (v13), -$ph2,   $phi,      0 ];
$vlst->[ 14 ] = [ (v14),  $ph2,  -$phi,      0 ];
$vlst->[ 15 ] = [ (v15), -$ph2,  -$phi,      0 ];

$vlst->[ 16 ] = [ (v16),  $phi,      0,   $ph2 ];
$vlst->[ 17 ] = [ (v17), -$phi,      0,   $ph2 ];
$vlst->[ 18 ] = [ (v18),  $phi,      0,  -$ph2 ];
$vlst->[ 19 ] = [ (v19), -$phi,      0,  -$ph2 ];

##########################################
# Define the faces

my @faces;
my $polyhedra = [];
my $p = 0;

push @faces, [ "f1",   $vlst-> [ 3 ], $vlst-> [ 2 ],  $vlst-> [ 1 ],  $vlst-> [ 0 ],  "blue" ];
push @faces, [ "f2",   $vlst-> [ 0 ], $vlst-> [ 1 ],  $vlst-> [ 5 ],  $vlst-> [ 4 ],  "blue" ];
push @faces, [ "f3",   $vlst-> [ 4 ], $vlst-> [ 7 ],  $vlst-> [ 3 ],  $vlst-> [ 0 ],  "blue" ];
push @faces, [ "f4",   $vlst-> [ 1 ], $vlst-> [ 2 ],  $vlst-> [ 6 ],  $vlst-> [ 5 ],  "blue" ];
push @faces, [ "f5",   $vlst-> [ 2 ], $vlst-> [ 3 ],  $vlst-> [ 7 ],  $vlst-> [ 6 ],  "blue" ];
push @faces, [ "f6",   $vlst-> [ 4 ], $vlst-> [ 5 ],  $vlst-> [ 6 ],  $vlst-> [ 7 ],  "blue" ];
push @{ $polyhedra }, [ "p1",
                    $faces[ $p++ ], $faces[ $p++ ], $faces[ $p++ ],
                    $faces[ $p++ ], $faces[ $p++ ], $faces[ $p++ ],
];

push @faces, [ "f7",   $vlst-> [ 16 ], $vlst-> [ 14 ], $vlst-> [ 11 ], $vlst-> [ 16 ], "green" ];
push @faces, [ "f8",   $vlst-> [ 0 ], $vlst-> [ 11 ], $vlst-> [ 19 ], $vlst-> [ 13 ], "green" ];
push @faces, [ "f9",   $vlst-> [ 0 ], $vlst-> [ 13 ], $vlst-> [ 8 ],  $vlst-> [ 16 ], "green" ];
push @faces, [ "f10",  $vlst-> [ 6 ], $vlst-> [ 8 ],  $vlst-> [ 13 ], $vlst-> [ 19 ], "green" ];
push @faces, [ "f11",  $vlst-> [ 14 ], $vlst-> [ 16 ],  $vlst-> [ 8 ], $vlst-> [ 6 ], "green" ];
push @faces, [ "f12",  $vlst-> [ 19 ], $vlst-> [ 11 ], $vlst-> [ 14 ], $vlst-> [ 6 ], "green" ];
push @{ $polyhedra }, [ "p2",
                    $faces[ $p++ ], $faces[ $p++ ], $faces[ $p++ ],
                    $faces[ $p++ ], $faces[ $p++ ], $faces[ $p++ ],
];

push @faces, [ "f13",  $vlst-> [ 17 ], $vlst-> [ 13 ], $vlst-> [ 10 ], $vlst-> [ 7 ], "red" ];
push @faces, [ "f14",  $vlst-> [ 7 ], $vlst-> [ 10 ], $vlst-> [ 18 ], $vlst-> [ 14 ], "red" ];
push @faces, [ "f15",  $vlst-> [ 7 ], $vlst-> [ 14 ], $vlst-> [ 9 ],  $vlst-> [ 17 ], "red" ];
push @faces, [ "f16",  $vlst-> [ 1 ], $vlst-> [ 9 ],  $vlst-> [ 14 ], $vlst-> [ 18 ], "red" ];
push @faces, [ "f17",  $vlst-> [ 13 ], $vlst-> [ 17 ],  $vlst-> [ 9 ], $vlst-> [ 1 ], "red" ];
push @faces, [ "f18",  $vlst-> [ 18 ], $vlst-> [ 10 ], $vlst-> [ 13 ], $vlst-> [ 1 ], "red" ];
push @{ $polyhedra }, [ "p3",
                    $faces[ $p++ ], $faces[ $p++ ], $faces[ $p++ ],
                    $faces[ $p++ ], $faces[ $p++ ], $faces[ $p++ ],
];

push @faces, [ "f19",  $vlst-> [ 19 ], $vlst-> [ 15 ],  $vlst-> [ 9 ], $vlst-> [ 2 ], "yellow" ];
push @faces, [ "f20",  $vlst-> [ 2 ], $vlst-> [ 9 ],  $vlst-> [ 16 ], $vlst-> [ 12 ], "yellow" ];
push @faces, [ "f21",  $vlst-> [ 2 ], $vlst-> [ 12 ], $vlst-> [ 10 ], $vlst-> [ 19 ], "yellow" ];
push @faces, [ "f22",  $vlst-> [ 4 ], $vlst-> [ 10 ], $vlst-> [ 12 ], $vlst-> [ 16 ], "yellow" ];
push @faces, [ "f23",  $vlst-> [ 15 ], $vlst-> [ 19 ], $vlst-> [ 10 ], $vlst-> [ 4 ], "yellow" ];
push @faces, [ "f24",  $vlst-> [ 16 ], $vlst-> [ 9 ], $vlst-> [ 15 ],  $vlst-> [ 4 ], "yellow" ];
push @{ $polyhedra }, [ "p4",
                    $faces[ $p++ ], $faces[ $p++ ], $faces[ $p++ ],
                    $faces[ $p++ ], $faces[ $p++ ], $faces[ $p++ ],
];

push @faces, [ "f25",  $vlst-> [ 3 ], $vlst-> [ 11 ], $vlst-> [ 15 ], $vlst-> [ 17 ], "magenta" ];
push @faces, [ "f26",  $vlst-> [ 12 ], $vlst-> [ 18 ], $vlst-> [ 11 ], $vlst-> [ 3 ], "magenta" ];
push @faces, [ "f27",  $vlst-> [ 17 ], $vlst-> [ 8 ], $vlst-> [ 12 ],  $vlst-> [ 3 ], "magenta" ];
push @faces, [ "f28",  $vlst-> [ 18 ], $vlst-> [ 12 ],  $vlst-> [ 8 ], $vlst-> [ 5 ], "magenta" ];
push @faces, [ "f29",  $vlst-> [ 15 ], $vlst-> [ 17 ],  $vlst-> [ 8 ], $vlst-> [ 5 ], "magenta" ];
push @faces, [ "f30",  $vlst-> [ 5 ], $vlst-> [ 15 ], $vlst-> [ 11 ], $vlst-> [ 18 ], "magenta" ];
push @{ $polyhedra }, [ "p5",
                    $faces[ $p++ ], $faces[ $p++ ], $faces[ $p++ ],
                    $faces[ $p++ ], $faces[ $p++ ], $faces[ $p++ ],
];

fixFaceOrientations( $polyhedra );

##########################################
# Write out the config file

writeConfigHeader( $context );

$context->{ 'FLOOR_Z' } = -$phi;
writeConfigFloor( $context );

writePlaneFaces( $context, $polyhedra );

##########################################
# Display the config if requested

showConfig( $context );

##########################################
# Run the ray-tracer to produce the image

processImage( $context );

