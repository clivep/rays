#!/usr/bin/perl

use warnings;
use strict;

use File::Temp qw/tempfile/;
use Math::Trig;

################################################################################
#
#   cube.pl -o <out file> [-rx <a>] [-ry <a>] [-rz <a>]
#
#       where mandatory parameters are:
#           -o  the mandatory output file
#
#       and optional paramters are:
#           -c  display the config details only
#           -rx rotation about the x axis (default 0)
#           -ry rotation about the y axis (default 0)
#           -rz rotation about the z axis (default 0)
#
################################################################################

use constant RAY_TRACER => '../../../bin/rays';

##########################################
# Process the parameters

my $outfile;
my $show_config;
my $show_progress;
my $rotations = [ 0, 0, 0 ];

for ( my $a = 0; $a <= $#ARGV; $a += 2 ) {

    usage() if ( $ARGV[ $a ] eq "-h" );

    if ( $ARGV[ $a ] eq "-m" ) {
        $show_progress = 1;
        $a--;
        next;
    }

    if ( $ARGV[ $a ] eq "-s" ) {
        $show_config = 1;
        $a--;
        next;
    }

    unless ( $ARGV[ $a + 1 ] ) {
        printf "Missing value for param %s\n", $ARGV[ $a ];
        exit -1;
    }

    if ( $ARGV[ $a ] eq "-o" ) {
        $outfile = $ARGV[ $a + 1 ];
    }
    elsif ( $ARGV[ $a ] eq "-rx" ) {
        $rotations->[ 0 ] = $ARGV[ $a + 1 ];
    }
    elsif ( $ARGV[ $a ] eq "-ry" ) {
        $rotations->[ 1 ] = $ARGV[ $a + 1 ];
    }
    elsif ( $ARGV[ $a ] eq "-rz" ) {
        $rotations->[ 2 ] = $ARGV[ $a + 1 ];
    }
    else {
        printf "Unknown parameter: %s\n", $ARGV[ $a ];
        exit -1;
    }
}

unless ( $outfile ) {
    printf "Missing outfile parameter\n";
    exit -1;
}

my ( $fh, $config ) = tempfile();

##########################################
# Define the vertices

my $side = 1.5;

my $v0 = [ "v0",  $side,  $side,  $side ];
my $v1 = [ "v1",  $side, -$side,  $side ];
my $v2 = [ "v2", -$side, -$side,  $side ];
my $v3 = [ "v3", -$side,  $side,  $side ];
my $v4 = [ "v4",  $side,  $side, -$side ];
my $v5 = [ "v5",  $side, -$side, -$side ];
my $v6 = [ "v6", -$side, -$side, -$side ];
my $v7 = [ "v7", -$side,  $side, -$side ];

##########################################
# Define the faces

my @faces;

push @faces, [ "f1",  $v3, $v2, $v1, $v0 ];
push @faces, [ "f2",  $v0, $v1, $v5, $v4 ];
push @faces, [ "f3",  $v1, $v2, $v6, $v5 ];
push @faces, [ "f4",  $v4, $v7, $v3, $v0 ];
push @faces, [ "f5",  $v7, $v6, $v2, $v3 ];
push @faces, [ "f6",  $v4, $v5, $v6, $v7 ];

push @faces, xRotateFace( [ "f7",   $v3, $v2, $v1, $v0 ], pi / 4.0 );
push @faces, xRotateFace( [ "f8",   $v0, $v1, $v5, $v4 ], pi / 4.0 );
push @faces, xRotateFace( [ "f9",   $v1, $v2, $v6, $v5 ], pi / 4.0 );
push @faces, xRotateFace( [ "f10",  $v4, $v7, $v3, $v0 ], pi / 4.0 );
push @faces, xRotateFace( [ "f11",  $v7, $v6, $v2, $v3 ], pi / 4.0 );
push @faces, xRotateFace( [ "f12",  $v4, $v5, $v6, $v7 ], pi / 4.0 );

push @faces, yRotateFace( [ "f13",  $v3, $v2, $v1, $v0 ], pi / 4.0 );
push @faces, yRotateFace( [ "f14",  $v0, $v1, $v5, $v4 ], pi / 4.0 );
push @faces, yRotateFace( [ "f15",  $v1, $v2, $v6, $v5 ], pi / 4.0 );
push @faces, yRotateFace( [ "f16",  $v4, $v7, $v3, $v0 ], pi / 4.0 );
push @faces, yRotateFace( [ "f17",  $v7, $v6, $v2, $v3 ], pi / 4.0 );
push @faces, yRotateFace( [ "f18",  $v4, $v5, $v6, $v7 ], pi / 4.0 );

push @faces, zRotateFace( [ "f19",  $v3, $v2, $v1, $v0 ], pi / 4.0 );
push @faces, zRotateFace( [ "f20",  $v0, $v1, $v5, $v4 ], pi / 4.0 );
push @faces, zRotateFace( [ "f21",  $v1, $v2, $v6, $v5 ], pi / 4.0 );
push @faces, zRotateFace( [ "f22",  $v4, $v7, $v3, $v0 ], pi / 4.0 );
push @faces, zRotateFace( [ "f23",  $v7, $v6, $v2, $v3 ], pi / 4.0 );
push @faces, zRotateFace( [ "f24",  $v4, $v5, $v6, $v7 ], pi / 4.0 );

##########################################
# Write out the config header

printf $fh "
defaults:
    background:
        red: 0.0
        green: 0.0
        blue: 0.0
    properties:
        anti-alias: on
        diffusion_index: 1.0
        specular_index: 0.3
        specular_exponent: 7.0
        color_scramble: 0.05

screen:
    centre:
        i: 40
        j: 40
        k: 45
    offset:
        i: 0
        j: 0
        k: -2
    eye_dist: 500
    height: 10
    width: 10

factor-scheme-defs:
    name: face
        diffusion_index: 0.9
        specular_index: 0.4
        specular_exponent: 5.0
        reflective_index: 0.3
    name: plane
        diffusion_index: 0.9
        specular_index: 0.4
        specular_exponent: 5.0
        reflective_index: 0.2

custom-color-defs:
    name: face
        red: 0.8
        green: 0.8
        blue: 0.8
    name: plane
        red: 0.5
        green: 0.5
        blue: 0.5

image:
    height: 5
    width: 5
    units: IN
    resolution: 200
    anti_aliasing: on

illumination:
    ambience: 0.3
    lamp:
        brightness: 100
        sample_size: 20
        centre:
            i: 0
            j: 0
            k: 10
        radius: 5
    lamp:
        brightness: 80
        sample_size: 20
        centre:
            i: 10
            j: 0
            k: 10
        radius: 5

objects:
    object:
        type: plane
        surface: default
        color-name: plane
        scheme-name: plane
        origin:
            i: -5
            j: -5
            k: -2
        axis1:
            i: 10
            j: 0
            k: 0
        axis2:
            i: 0
            j: 10
            k: 0

";

##########################################
# Write out the faces

map {

    my $face = rotateFace ( $_ );

    printf $fh "    object:\n";
    printf $fh "        type: plane\n";
    printf $fh "        surface: default\n";
    printf $fh "        color-name: face\n";
    printf $fh "        scheme-name: face\n";

    printf $fh "        origin:\n";
    printf $fh "            i: %s\n", $face->[ 2 ]->[ 1 ];
    printf $fh "            j: %s\n", $face->[ 2 ]->[ 2 ];
    printf $fh "            k: %s\n", $face->[ 2 ]->[ 3 ];

    printf $fh "        axis1:\n";
    printf $fh "            i: %s\n", $face->[ 3 ]->[ 1 ] - $face->[ 2 ]->[ 1 ];
    printf $fh "            j: %s\n", $face->[ 3 ]->[ 2 ] - $face->[ 2 ]->[ 2 ];
    printf $fh "            k: %s\n", $face->[ 3 ]->[ 3 ] - $face->[ 2 ]->[ 3 ];

    printf $fh "        axis2:\n";
    printf $fh "            i: %s\n", $face->[ 1 ]->[ 1 ] - $face->[ 2 ]->[ 1 ];
    printf $fh "            j: %s\n", $face->[ 1 ]->[ 2 ] - $face->[ 2 ]->[ 2 ];
    printf $fh "            k: %s\n", $face->[ 1 ]->[ 3 ] - $face->[ 2 ]->[ 3 ];

} @faces;

##########################################
# Display the config if requested

if ( $show_config ) {

    system( "cat $config" );
    unlink $config;

    exit 0;
}

##########################################
# Run the ray-tracer to produce the image

my $cmd = RAY_TRACER . " -c $config -f $outfile";

$cmd .= " -m" if $show_progress;

system( $cmd );

unlink $config;

exit 0;

##########################################
# Rotate the face about the X axis

sub xRotateFace {

    my $face = shift;
    my $xrot = shift || $rotations->[ 0 ];

    my $new_face = [ $face->[ 0 ] ];

    for ( my $v = 1; $v < 4; $v++ ) {

        push @{ $new_face }, [
            $face->[ $v ]->[ 0 ],
            $face->[ $v ]->[ 1 ],
            ( $face->[ $v ]->[ 2 ] * cos( $xrot ) ) + ( $face->[ $v ]->[ 3 ] * sin( $xrot ) ),
            ( $face->[ $v ]->[ 3 ] * cos( $xrot ) ) - ( $face->[ $v ]->[ 2 ] * sin( $xrot ) ),
        ];
    };

    return $new_face;
}

##########################################
# Rotate the face about the Y axis

sub yRotateFace {

    my $face = shift;
    my $yrot = shift || $rotations->[ 1 ];

    my $new_face = [ $face->[ 0 ] ];

    for ( my $v = 1; $v < 4; $v++ ) {

        push @{ $new_face }, [
            $face->[ $v ]->[ 0 ],
            ( $face->[ $v ]->[ 1 ] * cos( $yrot ) ) - ( $face->[ $v ]->[ 3 ] * sin( $yrot ) ),
            $face->[ $v ]->[ 2 ],
            ( $face->[ $v ]->[ 1 ] * sin( $yrot ) ) + ( $face->[ $v ]->[ 3 ] * cos( $yrot ) ),
        ];
    }

    return $new_face;
}

##########################################
# Rotate the face about the Z axis

sub zRotateFace {

    my $face = shift;
    my $zrot = shift || $rotations->[ 2 ];

    my $new_face = [ $face->[ 0 ] ];

    for ( my $v = 1; $v < 4; $v++ ) {

        push @{ $new_face }, [
            $face->[ $v ]->[ 0 ],
            ( $face->[ $v ]->[ 1 ] * cos( $zrot ) ) + ( $face->[ $v ]->[ 2 ] * sin( $zrot ) ),
            ( $face->[ $v ]->[ 2 ] * cos( $zrot ) ) - ( $face->[ $v ]->[ 1 ] * sin( $zrot ) ),
            $face->[ $v ]->[ 3 ],
        ];
    }

    return $new_face;
}

##########################################
# Rotate the face about all axes

sub rotateFace {

    my $face = shift;

    my $new_face;
    
    $new_face = xRotateFace( $face );
    $new_face = yRotateFace( $new_face );
    $new_face = zRotateFace( $new_face );

    return $new_face;
}

##########################################
# Print out the help text

sub usage {

    printf "
       icosahedron.pl -o <out file> [-rx <a>] [-ry <a>] [-rz <a>]

           where madatory parameters are:
               -o  the mandatory output file

           and optional paramters are:
               -c   display config details only
               -rx  rotation about the x axis (default 0)
               -ry  rotation about the y axis (default 0)
               -rz  rotation about the z axis (default 0)
    \n";

    exit 0;
}
