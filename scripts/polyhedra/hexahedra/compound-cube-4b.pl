#!/usr/bin/perl

use warnings;
use strict;

use Math::Trig;

################################################################################
#
#   compound4-a.pl
#
#       parameters detailed in usage()
#
################################################################################

do '../utils/utilities.pl';

##########################################
# Setup the working environment

my $context = setupContext();
$context->{ 'OFFSETS' } = [ 0, 0, -2 ];

processParameters( $context );

##########################################
# Define the vertices

my $side = 2.0;

my $v0 = [ "v0",  $side,  $side,  $side ];
my $v1 = [ "v1",  $side, -$side,  $side ];
my $v2 = [ "v2", -$side, -$side,  $side ];
my $v3 = [ "v3", -$side,  $side,  $side ];
my $v4 = [ "v4",  $side,  $side, -$side ];
my $v5 = [ "v5",  $side, -$side, -$side ];
my $v6 = [ "v6", -$side, -$side, -$side ];
my $v7 = [ "v7", -$side,  $side, -$side ];

##########################################
# Define the faces

my @faces;
my $polyhedra = [];
my $p = 0;

push @faces, [ "f1",  $v3, $v2, $v1, $v0, "red" ];
push @faces, [ "f2",  $v0, $v1, $v5, $v4, "red" ];
push @faces, [ "f3",  $v1, $v2, $v6, $v5, "red" ];
push @faces, [ "f4",  $v4, $v7, $v3, $v0, "red" ];
push @faces, [ "f5",  $v7, $v6, $v2, $v3, "red" ];
push @faces, [ "f6",  $v4, $v5, $v6, $v7, "red" ];
push @{ $polyhedra }, [ "p1",
                    $faces[ $p++ ], $faces[ $p++ ], $faces[ $p++ ],
                    $faces[ $p++ ], $faces[ $p++ ], $faces[ $p++ ],
];

push @faces, xzRotateFace( [ "f7",   $v3, $v2, $v1, $v0, "green" ], pi / 6.0, pi / 6.0 );
push @faces, xzRotateFace( [ "f8",   $v0, $v1, $v5, $v4, "green" ], pi / 6.0, pi / 6.0 );
push @faces, xzRotateFace( [ "f9",   $v1, $v2, $v6, $v5, "green" ], pi / 6.0, pi / 6.0 );
push @faces, xzRotateFace( [ "f10",  $v4, $v7, $v3, $v0, "green" ], pi / 6.0, pi / 6.0 );
push @faces, xzRotateFace( [ "f11",  $v7, $v6, $v2, $v3, "green" ], pi / 6.0, pi / 6.0 );
push @faces, xzRotateFace( [ "f12",  $v4, $v5, $v6, $v7, "green" ], pi / 6.0, pi / 6.0 );
push @{ $polyhedra }, [ "p2",
                    $faces[ $p++ ], $faces[ $p++ ], $faces[ $p++ ],
                    $faces[ $p++ ], $faces[ $p++ ], $faces[ $p++ ],
];

push @faces, yRotateFace( [ "f13",  $v3, $v2, $v1, $v0, "blue" ], pi / 4.0 );
push @faces, yRotateFace( [ "f14",  $v0, $v1, $v5, $v4, "blue" ], pi / 4.0 );
push @faces, yRotateFace( [ "f15",  $v1, $v2, $v6, $v5, "blue" ], pi / 4.0 );
push @faces, yRotateFace( [ "f16",  $v4, $v7, $v3, $v0, "blue" ], pi / 4.0 );
push @faces, yRotateFace( [ "f17",  $v7, $v6, $v2, $v3, "blue" ], pi / 4.0 );
push @faces, yRotateFace( [ "f18",  $v4, $v5, $v6, $v7, "blue" ], pi / 4.0 );
push @{ $polyhedra }, [ "p3",
                    $faces[ $p++ ], $faces[ $p++ ], $faces[ $p++ ],
                    $faces[ $p++ ], $faces[ $p++ ], $faces[ $p++ ],
];

push @faces, zRotateFace( [ "f19",  $v3, $v2, $v1, $v0, "yellow" ], pi / 4.0 );
push @faces, zRotateFace( [ "f20",  $v0, $v1, $v5, $v4, "yellow" ], pi / 4.0 );
push @faces, zRotateFace( [ "f21",  $v1, $v2, $v6, $v5, "yellow" ], pi / 4.0 );
push @faces, zRotateFace( [ "f22",  $v4, $v7, $v3, $v0, "yellow" ], pi / 4.0 );
push @faces, zRotateFace( [ "f23",  $v7, $v6, $v2, $v3, "yellow" ], pi / 4.0 );
push @faces, zRotateFace( [ "f24",  $v4, $v5, $v6, $v7, "yellow" ], pi / 4.0 );
push @{ $polyhedra }, [ "p4",
                    $faces[ $p++ ], $faces[ $p++ ], $faces[ $p++ ],
                    $faces[ $p++ ], $faces[ $p++ ], $faces[ $p++ ],
];

fixFaceOrientations( $polyhedra );

##########################################
# Write out the config file

writeConfigHeader( $context );

$context->{ 'FLOOR_Z' } = -1.0 * $side * sqrt( 2.0 );
writeConfigFloor( $context );

writePlaneFaces( $context, $polyhedra );

##########################################
# Display the config if requested

showConfig( $context );

##########################################
# Run the ray-tracer to produce the image

processImage( $context );

