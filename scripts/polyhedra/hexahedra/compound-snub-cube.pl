#!/usr/bin/perl

use warnings;
use strict;

use Math::Trig;

################################################################################
#
#  compound-snub-cube.pl
#
#       parameters detailed in usage()
#
################################################################################

do '../utils/utilities.pl';

use constant    TRIB    => (
                            1.0 +
                            (19.0 - 3 * sqrt( 33.0 ) ) ** ( 1.0 / 3.0 ) +
                            (19.0 + 3 * sqrt( 33.0 ) ) ** ( 1.0 / 3.0 )
                       ) / 3.0;

##########################################
# Setup the working environment

my $context = setupContext();
$context->{ 'OFFSETS' } = [ 0, 0, -1.5 ];

processParameters( $context );

##########################################
# Setup the figure

my $side = 2.8;

my $v1 = ( 26.0 + 6.0 * sqrt( 33.0 ) ) ** ( 1.0 / 3.0 );
my $v2 = sqrt( 
            ( 4.0 / 3.0 ) - 
            ( 16.0 / ( 3.0 * $v1 ) ) + 
            ( 2.0 * $v1 / 3.0 )
);

my $vlist = [];

my $size = $side / $v2;

push @{ $vlist },  @{ vertexPerms( 
    [ $size, $size / TRIB, $size * TRIB ],
    [ 'ABC', 'BCA', 'CAB' ],
) };

push @{ $vlist },  @{ vertexPerms( 
    [ $size, $size / TRIB, $size * TRIB ],
    [ 'ACB', 'BAC', 'CBA' ],
) };

setupVertexConnections( $vlist, $side );

my $triangles = findTriangles( $vlist, $side, "blue" );
my $squares = findSquares( $vlist, $side, "red" );

# Triangles adjacent to squares to be yellow
map {
    my $square = $_;
    map {
        $_->[ 4 ] = 'yellow' if hasCommonEdge( $_, $square );
        } @{ $triangles };
} @{ $squares };

my $polyhedra = [
        [ "p1",
            @{ $triangles },
            @{ squares2triangles( $squares ) },
        ]
];

fixFaceOrientations( $polyhedra );

##########################################
# Write out the config file

writeConfigHeader( $context );

$context->{ 'FLOOR_Z' } = -1.0 * $side; # * sqrt( 2.0 );
writeConfigFloor( $context );

writeTriangleFaces( $context, $polyhedra );

##########################################
# Display the config if requested

showConfig( $context );

##########################################
# Run the ray-tracer to produce the image

processImage( $context );

