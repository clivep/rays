#!/usr/bin/perl

use warnings;
use strict;

for( my $v = 5; $v < 9; $v++ ) {

    printf "\nVertices: $v\n-----------\n";
    printf "valid starts 1->%d\n", ( $v - 1 ) / 2;

    my $max_every = int( ( $v - 1 ) / 2.0 );
    printf "max skip %d\n", $max_every;

    for( my $s = 1; $s < $v / 2; $s++ ) {

        my $dst1 = $v - $s;

        if (
            ( $dst1 - $s > 1 ) &&
            ( ( $dst1 - $s ) != ( $v / 2 ) )
        ) {
            my $every = $dst1 - $s;
            $every = $v - $every if ( $every > ( $v / 2 ) );
            printf "  %d/%d   %d -> %d rot: N %s\n", $v, $every, $s, $dst1, ( $s > ( $v / 4 ) ) ? 'crossed' : '';
        }

        my $dst2 = 1 + $v - $s;

        if (
            ( $dst2 < $v ) &&
            ( ( $dst2 - $s ) != ( $v / 2 ) )
        ) {
            my $every = $dst2 - $s;
            $every = $v - $every if ( $every > ( $v / 2 ) );
            printf "  %d/%d   %d -> %d rot: Y %s\n", $v, $every, $s, $dst2, ( $s > ( 0.5 + ( $v / 4 ) ) ) ? 'crossed' : '';
        }
    }
}
