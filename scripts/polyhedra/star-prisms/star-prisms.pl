#!/usr/bin/perl

use warnings;
use strict;

use File::Temp qw/tempfile/;
use Math::Trig;

################################################################################
#
#   star-prisms.pl <points> <every>
#
#       parameters detailed in usage()
#
################################################################################

do '../utils/utilities.pl';

use constant    STAR_COLOR  =>  'red';
use constant    BAND1_COLOR =>  'blue';
use constant    BAND2_COLOR =>  'green';

##########################################
# Setup the working environment

my $context = setupContext();
$context->{ 'OFFSETS' } = [ 0, 0, .5 ];

processParameters( $context );

unless ( $context->{ 'STAR_EVERY' } < ( $context->{ 'STAR_POINTS' } / 2.0 ) ) {

    printf "$context->{ 'STAR_POINTS' } / $context->{ 'STAR_EVERY' } is not a valid combination\n";
    exit -1;
}

if ( $context->{ 'STAR_EVERY' } < 2 ) {

    printf "Not a valid every value\n";
    exit -1;
}

getPrismOffsets();

##########################################
# Setup the 2 vertex lists and star paths

my $upper_vlist = [];
my $lower_vlist = [];

for( my $p = 0; $p < $context->{ 'STAR_POINTS' }; $p++ ) {

    my $offset = 0;

    push @{ $upper_vlist }, [
        "uv$p",
        $context->{ 'STAR_RADIUS' } * cos( $offset + $p * 2.0 * pi / $context->{ 'STAR_POINTS' } ),
        $context->{ 'STAR_RADIUS' } * sin( $offset + $p * 2.0 * pi / $context->{ 'STAR_POINTS' } ),
        $context->{ 'PRISM_HEIGHT' },
    ];

    $offset = -1.0 * pi / $context->{ 'STAR_POINTS' }
                            if ( $context->{ 'OFFSET_INFO' }->{ 'ROTATE' } );

    push @{ $lower_vlist }, [
        "lv$p",
        $context->{ 'STAR_RADIUS' } * cos( $offset + $p * 2.0 * pi / $context->{ 'STAR_POINTS' } ),
        $context->{ 'STAR_RADIUS' } * sin( $offset + $p * 2.0 * pi / $context->{ 'STAR_POINTS' } ),
        0
    ];
}

my $start = 0;
my $paths = [ [] ];

for( my $v = 0; $v < $context->{ 'STAR_POINTS' }; $v++ ) {

    if ( $v && ( ( $v * $context->{ 'STAR_EVERY' } ) % $context->{ 'STAR_POINTS' } == 0 ) ) {
        $start++;
        push @{ $paths }, [];
    }
    push @{ $paths->[ -1 ] }, ( $v * $context->{ 'STAR_EVERY' } ) % $context->{ 'STAR_POINTS' } + $start;
}

##########################################
# Setup the figure

my $upper_triangles = getStarTriangles( $upper_vlist, STAR_COLOR );
my $lower_triangles = getStarTriangles( $lower_vlist, STAR_COLOR, scalar @{ $upper_triangles } );

my $band_triangles = getBandTriangles( $upper_vlist, $lower_vlist, BAND1_COLOR, BAND2_COLOR );

my $polyhedra = [
        [ "p1",
#          @{ $upper_triangles },
#          @{ $lower_triangles },
          @{ $band_triangles },
        ]
];

fixFaceOrientations( $polyhedra, [ 'C', 0, 0, $context->{ 'PRISM_HEIGHT' } / 2.0 ] );

##########################################
# Write out the config file

writeConfigHeader( $context );

$context->{ 'FLOOR_Z' } = -0.5;
writeConfigFloor( $context );

writeTriangleFaces( $context, $polyhedra );

##########################################
# Display the config if requested

showConfig( $context );

##########################################
# Run the ray-tracer to produce the image

processImage( $context );

exit;

##########################################
# Return the triangles for the star

sub getBandTriangles {

    my $upper_vlist = shift;
    my $lower_vlist = shift;
    my $upper_color = shift || "yellow";
    my $lower_color = shift || $upper_color;

    my $triangles = [];

    my $start_vtx = $context->{ 'OFFSET_INFO' }->{ 'OFFSET1' };
    my $end_vtx = $context->{ 'OFFSET_INFO' }->{ 'OFFSET2' };

    my $offset = $context->{ 'OFFSET_INFO' }->{ 'ROTATE' } ? 1 : 0;

#    for( my $v = 0; $v < 1; $v++ ) {
    for( my $v = 0; $v < $context->{ 'STAR_POINTS' }; $v++ ) {

        push @{ $triangles }, [
            sprintf( "fb%d", scalar @{ $triangles } ),
            $upper_vlist->[ $v ],
            $lower_vlist->[ ( $v + $start_vtx ) % $context->{ 'STAR_POINTS' } ],
            $lower_vlist->[ ( $v + $end_vtx ) % $context->{ 'STAR_POINTS' } ],
            $upper_color
        ];

       push @{ $triangles }, [
            sprintf( "fb%d", scalar @{ $triangles } ),
            $lower_vlist->[ $v ],
            $upper_vlist->[ ( $v + $start_vtx - $offset ) % $context->{ 'STAR_POINTS' } ],
            $upper_vlist->[ ( $v + $end_vtx - $offset ) % $context->{ 'STAR_POINTS' } ],
            $lower_color
        ];
    }

    return $triangles;
}

##########################################
# Return the triangles for the star

sub getStarTriangles {

    my $vlist = shift;
    my $color = shift;
    my $offset = shift;

    my $triangles = [];

    # From each vertex there will be 2 triangles - from the vertex
    # to the first intersection points along the 2 legs and from
    # those points to the centre

    for( my $v = 0; $v < $context->{ 'STAR_POINTS' }; $v++ ) {

        my $v0x = $vlist->[ $v ]->[ 1 ];  # the vertex
        my $v0y = $vlist->[ $v ]->[ 2 ];

        # end of the first leg
        my $v1x = $vlist->[ ( $v + $context->{ 'STAR_EVERY' } ) % $context->{ 'STAR_POINTS' } ]->[ 1 ]; 
        my $v1y = $vlist->[ ( $v + $context->{ 'STAR_EVERY' } ) % $context->{ 'STAR_POINTS' } ]->[ 2 ];

        # end of the other leg
        my $v2x = $vlist->[ $v - $context->{ 'STAR_EVERY' } ]->[ 1 ];
        my $v2y = $vlist->[ $v - $context->{ 'STAR_EVERY' } ]->[ 2 ];

        # The first crossing leg v3->v4 will originate from the next vertex

        my $v3x = $vlist->[ ( $v + 1 ) % $context->{ 'STAR_POINTS' } ]->[ 1 ];
        my $v3y = $vlist->[ ( $v + 1 ) % $context->{ 'STAR_POINTS' } ]->[ 2 ];

        my $v4x = $vlist->[ $v + 1 - $context->{ 'STAR_EVERY' } ]->[ 1 ];
        my $v4y = $vlist->[ $v + 1 - $context->{ 'STAR_EVERY' } ]->[ 2 ];

        my $a1x = ( $v1x - $v0x );
        my $a1y = ( $v1y - $v0y );

        my $b1x = ( $v4x - $v3x );
        my $b1y = ( $v4y - $v3y );

        my $v1 = ( $a1x * ( $v0y - $v3y ) + $a1y * ( $v3x - $v0x ) ) / ( $b1y * $a1x - $b1x * $a1y );

        # This is the first intersection point

        my $i0x = $v3x + $v1 * $b1x;
        my $i0y = $v3y + $v1 * $b1y;
    
        # The other crossing leg v5->v6 will originate from the previous vertex

        my $v5x = $vlist->[ $v - 1 ]->[ 1 ];
        my $v5y = $vlist->[ $v - 1 ]->[ 2 ];

        my $v6x = $vlist->[ ( $v - 1 + $context->{ 'STAR_EVERY' } ) % $context->{ 'STAR_POINTS' } ]->[ 1 ];
        my $v6y = $vlist->[ ( $v - 1 + $context->{ 'STAR_EVERY' } ) % $context->{ 'STAR_POINTS' } ]->[ 2 ];

        my $a2x = ( $v2x - $v0x );
        my $a2y = ( $v2y - $v0y );

        my $b2x = ( $v6x - $v5x );
        my $b2y = ( $v6y - $v5y );

        my $v2 = ( $a2x * ( $v0y - $v5y ) + $a2y * ( $v5x - $v0x ) ) / ( $b2y * $a2x - $b2x * $a2y );

        # This is the second intersection point
        
        my $i1x = $v5x + $v2 * $b2x;
        my $i1y = $v5y + $v2 * $b2y;

        push @{ $triangles }, [
            sprintf( "f%d", scalar @{ $triangles } ),
            $vlist->[ $v ],
            [ "vi0-$v", $i0x, $i0y, $vlist->[ $v ]->[ 3 ] ],
            [ "vi1-$v", $i1x, $i1y, $vlist->[ $v ]->[ 3 ] ],
            $color
        ];

        push @{ $triangles }, [
            sprintf( "f%d", scalar @{ $triangles } ),
            [ "c", 0.0, 0.0, $vlist->[ $v ]->[ 3 ] ],
            [ "vi0-$v", $i0x, $i0y, $vlist->[ $v ]->[ 3 ] ],
            [ "vi1-$v", $i1x, $i1y, $vlist->[ $v ]->[ 3 ] ],
            $color
        ];
    }

    return $triangles;
}

##########################################
# Caclulate the offsets for the prism type

sub getPrismOffsets {

    my $max_every = int( ( $context->{ 'STAR_POINTS' } - 1 ) / 2.0 );

    my $offset_info = {};

    for( my $s = 1; $s < $context->{ 'STAR_POINTS' } / 2; $s++ ) {

        my $dst1 = $context->{ 'STAR_POINTS' } - $s;

        if ( $dst1 - $s > 1 ) {

            my $every = $dst1 - $s;
            $every = $context->{ 'STAR_POINTS' } - $every
                        if ( $every > ( $context->{ 'STAR_POINTS' } / 2 ) );

            my $crossed = ( $s > ( $context->{ 'STAR_POINTS' } / 4 ) ) ? 1 : 0;

            if (
                ( $crossed == $context->{ 'STAR_CROSSED' } ) &&
                ( $every == $context->{ 'STAR_EVERY' } ) 
            ) {
                $offset_info->{ 'OFFSET1' } = $s;
                $offset_info->{ 'OFFSET2' } = $dst1;
                $offset_info->{ 'ROTATE' } = 0;
            }
        }

        my $dst2 = 1 + $context->{ 'STAR_POINTS' } - $s;

        if ( $dst2 < $context->{ 'STAR_POINTS' } ) {

            my $every = $dst2 - $s;
            $every = $context->{ 'STAR_POINTS' } - $every
                        if ( $every > ( $context->{ 'STAR_POINTS' } / 2 ) );

            my $crossed = ( $s > ( 0.5 + $context->{ 'STAR_POINTS' } / 4 ) ) ? 1 : 0;

            if (
                ( $crossed == $context->{ 'STAR_CROSSED' } ) &&
                ( $every == $context->{ 'STAR_EVERY' } ) 
            ) {
                $offset_info->{ 'OFFSET1' } = $s;
                $offset_info->{ 'OFFSET2' } = $dst2;
                $offset_info->{ 'ROTATE' } = 1;
            }
        }
    }

    $context->{ 'OFFSET_INFO' } = $offset_info;
}
