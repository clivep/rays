#!/bin/sh 

mkdir images > /dev/null 2>&1

vertices=5
while [ $vertices -le 25 ]
do
    max=`expr \( $vertices - 1 \) / 2`
    every=2
    while [ $every -le $max ]
    do
    
        label="Star $vertices/$every anti-prism"
        image="images/star-${vertices}-${every}-antiprism.pdf"

        echo $label
        ./star-prisms.pl -r 200 -o $image -sp $vertices -se $every -rz 25 -sz 25 -oz 1.7 -sr 4.6 -l "$label"

        label="Star $vertices/$every crossed anti-prism"
        image="images/star-${vertices}-${every}-crossed-antiprism.pdf"

        echo $label
        ./star-prisms.pl -r 200 -o $image -sp $vertices -se $every -rz 25 -sz 25 -oz 1.7 -sr 4.6 -sc -l "$label"

        every=`expr $every + 1`
    done
    vertices=`expr $vertices + 1`
done
