#!/usr/bin/perl

use warnings;
use strict;

use Math::Trig;

################################################################################
#
#   star-prism.pl
#
#       parameters detailed in usage()
#
################################################################################

do '../utils/utilities.pl';

##########################################
# Setup the working environment

my $context = setupContext();
$context->{ 'OFFSETS' } = [ 0, 0, 0.5 ];

processParameters( $context );

##########################################
# Setup the figure

my $radius = 3.8;
my $height = 3.8;

my $star_count = 5;
my $skip_count = 1;

my $star1_triangles = [];
my $star2_triangles = [];

my $t = 0;

my $base = $radius * tan( deg2rad( 180.0 / ( $star_count * 2 ) ) );

for( my $s = 0; $s < $star_count; $s++ ) {

    my $ang = 360.0 * $s / $star_count;

    my $vtx1 = [
        "v$s-1",
        $radius * cos( deg2rad( $ang ) ),
        $radius * sin( deg2rad( $ang ) ),
        0
    ];

    my $vtx2 = [
        "v$s-2",
        $base * cos( deg2rad( $ang + 90 ) ),
        $base * sin( deg2rad( $ang + 90 ) ),
        $vtx1->[ 3 ],
        [],
    ];

    my $vtx3 = [
        "v$s-3",
        $base * cos( deg2rad( $ang - 90 ) ),
        $base * sin( deg2rad( $ang - 90 ) ),
        $vtx1->[ 3 ],
        [],
    ];

    push @{ $star1_triangles }, [
        sprintf( "f%d", $s ),
        $vtx1, $vtx2, $vtx3,
        "red",
    ];
}

for( my $s = 0; $s < $star_count; $s++ ) {

    my $ang = 360.0 * ( $s - 0.5 ) / $star_count;

    my $vtx1 = [
        "v$s-1",
        $radius * cos( deg2rad( $ang ) ),
        $radius * sin( deg2rad( $ang ) ),
        $height
    ];

    my $vtx2 = [
        "v$s-2",
        $base * cos( deg2rad( $ang + 90 ) ),
        $base * sin( deg2rad( $ang + 90 ) ),
        $vtx1->[ 3 ],
        [],
    ];

    my $vtx3 = [
        "v$s-3",
        $base * cos( deg2rad( $ang - 90 ) ),
        $base * sin( deg2rad( $ang - 90 ) ),
        $vtx1->[ 3 ],
        [],
    ];

    push @{ $star2_triangles }, [
        sprintf( "f%d", $s ),
        $vtx1, $vtx2, $vtx3,
        "red",
    ];
}

my $triangles = [];

for( my $s = 0; $s < $star_count; $s++ ) {

    push @{ $triangles } , [
        "",
        $star1_triangles->[ $s ]->[ 1 ],
        $star2_triangles->[ $s - 2 ]->[ 1 ],
        $star1_triangles->[ $s - 3 ]->[ 1 ],
        "yellow"
    ];

    push @{ $triangles } , [
        "",
        $star2_triangles->[ $s ]->[ 1 ],
        $star1_triangles->[ $s - 1 ]->[ 1 ],
        $star2_triangles->[ $s - 3 ]->[ 1 ],
        "yellow"
    ];
}

my $polyhedra = [
        [ "p1",
            @{ $star1_triangles },
            @{ $star2_triangles },
            @{ $triangles },
        ]
];

fixFaceOrientations( $polyhedra );

##########################################
# Write out the config file

writeConfigHeader( $context );

$context->{ 'FLOOR_Z' } = -1.0;
writeConfigFloor( $context );

writeTriangleFaces( $context, $polyhedra );

##########################################
# Display the config if requested

showConfig( $context );

##########################################
# Run the ray-tracer to produce the image

processImage( $context );

