#!/usr/bin/perl

use warnings;
use strict;

use File::Temp qw/tempfile/;
use Math::Trig;

################################################################################
#
#   star-prisms.pl <points> <every>
#
#       parameters detailed in usage()
#
################################################################################

use constant    RADIUS  =>  250;

if ( $#ARGV != 1 ) {

    printf "Usage %s <points> <every>\n", $0;
    exit -1;
}

my $points = $ARGV[ 0 ];
my $every = $ARGV[ 1 ];
my $radius = RADIUS;

unless ( $every < ( $points / 2.0 ) ) {

    printf "$points / $every is not a valid combination\n";
    exit -1;
}

my $ang_offset = 90;

# From vertex v0 there will be a legs with endpoints v1 and v2

my $v0x = $radius * cos( deg2rad( $ang_offset + 0.0 ) );
my $v0y = $radius * sin( deg2rad( $ang_offset + 0.0 ) );

my $v1x = $radius * cos( deg2rad( $ang_offset + $every * 360.0 / $points ) );
my $v1y = $radius * sin( deg2rad( $ang_offset + $every * 360.0 / $points ) );

my $v2x = $radius * cos( deg2rad( $ang_offset - $every * 360.0 / $points ) );
my $v2y = $radius * sin( deg2rad( $ang_offset - $every * 360.0 / $points ) );

# The first crossing leg v3->v4 will originate from the next vertex

my $v3x = $radius * cos( deg2rad( $ang_offset + 360.0 / $points ) );
my $v3y = $radius * sin( deg2rad( $ang_offset + 360.0 / $points ) );

my $v4x = $radius * cos( deg2rad( $ang_offset + ( 1 - $every ) * 360.0 / $points ) );
my $v4y = $radius * sin( deg2rad( $ang_offset + ( 1 - $every ) * 360.0 / $points ) );

my $a1x = ( $v1x - $v0x );
my $a1y = ( $v1y - $v0y );

my $b1x = ( $v4x - $v3x );
my $b1y = ( $v4y - $v3y );

my $v1 = ( $a1x * ( $v0y - $v3y ) + $a1y * ( $v3x - $v0x ) ) / ( $b1y * $a1x - $b1x * $a1y );

# This is the first intersection point

my $i0x = $v3x + $v1 * $b1x;
my $i0y = $v3y + $v1 * $b1y;

# The other crossing leg v5->v6 will originate from the previous vertex

my $v5x = $radius * cos( deg2rad( $ang_offset - 360.0 / $points ) );
my $v5y = $radius * sin( deg2rad( $ang_offset - 360.0 / $points ) );

my $v6x = $radius * cos( deg2rad( $ang_offset + ( $every - 1 ) * 360.0 / $points ) );
my $v6y = $radius * sin( deg2rad( $ang_offset + ( $every - 1 ) * 360.0 / $points ) );

my $a2x = ( $v2x - $v0x );
my $a2y = ( $v2y - $v0y );

my $b2x = ( $v6x - $v5x );
my $b2y = ( $v6y - $v5y );

my $v2 = ( $a2x * ( $v0y - $v5y ) + $a2y * ( $v5x - $v0x ) ) / ( $b2y * $a2x - $b2x * $a2y );

# This is the second intersection point

my $i1x = $v5x + $v2 * $b2x;
my $i1y = $v5y + $v2 * $b2y;

my $dots = "
            1 0 1 setrgbcolor $v0x $v0y dot
            0 0 1 setrgbcolor $v1x $v1y dot
            %0 1 1 setrgbcolor $v2x $v2y dot
            1 1 0 setrgbcolor $v3x $v3y dot
            1 1 0 setrgbcolor $v4x $v4y dot
            0 1 0 setrgbcolor $i0x $i0y dot
            0 1 0 setrgbcolor $i1x $i1y dot
    ";

drawImage();

exit;

########################################
# Draw the image
#
sub drawImage {

    my ( $fh, $tmp_file ) = tempfile();

    print $fh "%!PS";
    print $fh "%%BoundingBox: 0 0 800 800\n";
    print $fh "%%EndComments\n";

    print $fh "%====================================================\n";
    print $fh "% Setup the image\n";
    print $fh "%====================================================
            /pg_wid 800 def
            /pg_dep 800 def

            << /PageSize [pg_wid pg_dep] >> setpagedevice

            /points $points def
            /every $every def
            /rad $radius def

            /ang_offset $ang_offset def
    ";

    print $fh "%====================================================\n";
    print $fh "% Draw the background\n";
    print $fh "%====================================================
            gsave
            0 0 moveto
            1000 0 lineto
            1000 1000 lineto
            0 1000 lineto
            0 setgray 
            fill
            grestore
    ";

    print $fh "%====================================================\n";
    print $fh "% Draw the path\n";
    print $fh "%====================================================

            /dot {  % x y dot -

                3 dict begin
                /_y exch def
                /_x exch def

                0 10 360 {
                    /_a exch def
                    _x _a cos 3 mul add pg_wid 2 div add
                    _y _a sin 3 mul add pg_dep 2 div add
                    _a 0 eq { moveto } { lineto } ifelse
                } for
                stroke

                end
            } def
    ";

    print $fh "%====================================================\n";
    print $fh "% Draw the path\n";
    print $fh "%====================================================

            /drawPath {

                2 dict begin

                /_offset exch def
                /_every exch def

                0 1 points {

                    /_p exch def
                    /_a 360 _p _every mul _offset add points div mul ang_offset add def

                    pg_wid 2 div _a cos rad mul add 
                    pg_dep 2 div _a sin rad mul add 
    
                    _p 0 eq { moveto } { lineto } ifelse

                } for

                end
            } def
    ";

    print $fh "%====================================================\n";
    print $fh "% Draw the base polygon\n";
    print $fh "%====================================================
            1 0 drawPath .18 setgray fill
            1 0 drawPath 1 1 0 setrgbcolor stroke
    ";

    print $fh "%====================================================\n";
    print $fh "% Internal stars\n";
    print $fh "%====================================================

            2 1 points 2 div cvi {
        
                /_every exch def

                /_circuits 1 def

                points _every mod 0 eq {
                    /_circuits _every def
                } if

                1 1 _circuits {
    
                    /_offset exch 1 sub def

                    _every 0 gt points _every div 2 ne and {

                        _every _offset drawPath

                        _every every eq {
                            1 0 1 setrgbcolor 
                        } { 
                            1 1 0 setrgbcolor 
                        } ifelse 

                        stroke
                    } if
                } for
            } for

            $dots
    ";

    close $fh;

    system( "ps2pdf $tmp_file stars.pdf" );

    unlink $tmp_file;
}
