#!/bin/sh -x

mkdir images > /dev/null 2>&1

./tetrahedron.pl -r 200 -rx 45 -rz 340 -o images/tetrahedron.pdf -l 'Tetrahedron'

./compound2-a.pl -r 200 -rz 70 -rx 35 -ry 45 -o images/compound2-a.pdf -l Compound of 2 Tetrahedrons'
./compound3-a.pl -r 200 -rx 15 -rz 15 -o images/compound3-a.pdf -l Compound of 3 Tetrahedrons'
./compound4-a.pl -r 200 -rx 15 -rz 15 -o images/compound4-a.pdf -l Compound of 4 Tetrahedrons'
./compound5-a.pl -r 200 -o images/compound5-a.pdf -l Compound of 5 Tetrahedrons'

./truncated-tetrahedron.pl -r 200 -rz 45 -rx 55 -oz -1.5 -o images/truncated-tetrahedron.pdf -l 'Truncated Tetrahedron'
