#!/usr/bin/perl

use warnings;
use strict;

use Math::Trig;

################################################################################
#
#   compound5-a.pl
#
#       parameters detailed in usage()
#
################################################################################

do '../utils/utilities.pl';

##########################################
# Setup the working environment

my $context = setupContext();
$context->{ 'OFFSETS' } = [ 0, 0, -1.8 ];

processParameters( $context );

##########################################
# Define the vertices

my $size = 2;

my $phi = $size * phi();
my $ph2 = $size / phi();

my $vlst = [
    [ "v0",   $size,  $size, -$size ],
    [ "v1",   $size,  $size,  $size ],
    [ "v2",  -$size,  $size,  $size ],
    [ "v3",  -$size,  $size, -$size ],
    
    [ "v4",   $size, -$size, -$size ],
    [ "v5",   $size, -$size,  $size ],
    [ "v6",  -$size, -$size,  $size ],
    [ "v7",  -$size, -$size, -$size ],
    
    [ "v8",       0,   $ph2,   $phi ],
    [ "v9",       0,  -$ph2,   $phi ],
    [ "v10",      0,   $ph2,  -$phi ],
    [ "v11",      0,  -$ph2,  -$phi ],
    
    [ "v12" ,  $ph2,   $phi,      0 ],
    [ "v13",  -$ph2,   $phi,      0 ],
    [ "v14",   $ph2,  -$phi,      0 ],
    [ "v15",  -$ph2,  -$phi,      0 ],
    
    [ "v16",   $phi,      0,   $ph2 ],
    [ "v17",  -$phi,      0,   $ph2 ],
    [ "v18",   $phi,      0,  -$ph2 ],
    [ "v19",  -$phi,      0,  -$ph2 ],
];

##########################################
# Define the faces

my @faces;
my $polyhedra = [];

push @faces, [ "f1", $vlst->[ 0 ], $vlst->[ 2 ], $vlst->[ 5 ], "red" ];
push @faces, [ "f2", $vlst->[ 0 ], $vlst->[ 5 ], $vlst->[ 7 ], "red" ];
push @faces, [ "f3", $vlst->[ 7 ], $vlst->[ 2 ], $vlst->[ 0 ], "red" ];
push @faces, [ "f4", $vlst->[ 7 ], $vlst->[ 5 ], $vlst->[ 2 ], "red" ];
push @{ $polyhedra }, [ "p1", $faces[ 0 ], $faces[ 1 ], $faces[ 2 ], $faces[ 3 ], ];

push @faces, [ "f5", $vlst->[ 14 ], $vlst->[ 10 ], $vlst->[ 1 ], "green" ];
push @faces, [ "f6", $vlst->[ 1 ], $vlst->[ 10 ], $vlst->[ 17 ], "green" ];
push @faces, [ "f7", $vlst->[ 17 ], $vlst->[ 14 ], $vlst->[ 1 ], "green" ];
push @faces, [ "f8", $vlst->[ 10 ], $vlst->[ 14 ], $vlst->[ 17 ], "green" ];
push @{ $polyhedra }, [ "p1", $faces[ 4 ], $faces[ 5 ], $faces[ 6 ], $faces[ 7 ], ];

push @faces, [ "f9", $vlst->[ 15 ], $vlst->[ 8 ], $vlst->[ 3 ], "blue" ];
push @faces, [ "f10", $vlst->[ 3 ], $vlst->[ 8 ], $vlst->[ 18 ], "blue" ];
push @faces, [ "f11", $vlst->[ 18 ], $vlst->[ 15 ], $vlst->[ 3 ], "blue" ];
push @faces, [ "f12", $vlst->[ 8 ], $vlst->[ 15 ], $vlst->[ 18 ], "blue" ];
push @{ $polyhedra }, [ "p1", $faces[ 8 ], $faces[ 9 ], $faces[ 10 ], $faces[ 11 ], ];

push @faces, [ "f13", $vlst->[ 12 ], $vlst->[ 9 ], $vlst->[ 4 ], "yellow" ];
push @faces, [ "f14", $vlst->[ 4 ], $vlst->[ 9 ], $vlst->[ 19 ], "yellow" ];
push @faces, [ "f15", $vlst->[ 19 ], $vlst->[ 12 ], $vlst->[ 4 ], "yellow" ];
push @faces, [ "f16", $vlst->[ 9 ], $vlst->[ 12 ], $vlst->[ 19 ], "yellow" ];
push @{ $polyhedra }, [ "p1", $faces[ 12 ], $faces[ 13 ], $faces[ 14 ], $faces[ 15 ], ];

push @faces, [ "f17", $vlst->[ 13 ], $vlst->[ 11 ], $vlst->[ 6 ], "cyan" ];
push @faces, [ "f18", $vlst->[ 6 ], $vlst->[ 11 ], $vlst->[ 16 ], "cyan" ];
push @faces, [ "f19", $vlst->[ 16 ], $vlst->[ 13 ], $vlst->[ 6 ], "cyan" ];
push @faces, [ "f20", $vlst->[ 11 ], $vlst->[ 13 ], $vlst->[ 16 ], "cyan" ];
push @{ $polyhedra }, [ "p1", $faces[ 16 ], $faces[ 17 ], $faces[ 18 ], $faces[ 19 ], ];

fixFaceOrientations( $polyhedra );

##########################################
# Write out the config file

writeConfigHeader( $context );

$context->{ 'FLOOR_Z' } = -$size * sqrt( 2.0 );
writeConfigFloor( $context );

writeTriangleFaces( $context, $polyhedra );

##########################################
# Display the config if requested

showConfig( $context );

##########################################
# Run the ray-tracer to produce the image

processImage( $context );

