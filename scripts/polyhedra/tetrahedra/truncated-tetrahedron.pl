#!/usr/bin/perl

use warnings;
use strict;

use Math::Trig;

################################################################################
#
#   truncated-tetrahedron.pl
#
#       parameters detailed in usage()
#
################################################################################

do '../utils/utilities.pl';

##########################################
# Setup the working environment

my $context = setupContext();
$context->{ 'OFFSETS' } = [ 0, 0, -1.5 ];

processParameters( $context );

##########################################
# Define the vertices

my $side = 3;

my $vlist = [];

my $size = $side / sqrt( 8.0 );

# of the odd permutataions take those with an even number of -ve coordinates
map {
    my $count = 0;
    $count++ if ( $_->[ 1 ] < 0 );
    $count++ if ( $_->[ 2 ] < 0 );
    $count++ if ( $_->[ 3 ] < 0 );

    push @{ $vlist }, $_ if ( ( $count == 0 ) || ( $count == 2 ) );

} @{ vertexPerms(
    [ $size, $size, $size * 3.0 ],
    [ 'ABC', 'BCA', 'CAB' ],
) };

setupVertexConnections( $vlist, $side );

my $triangles = findTriangles( $vlist, $side, "yellow" );
my $hexagons = findHexagons( $vlist, $side, "red" );

my $polyhedra = [
        [ "p1",
            @{ $triangles },
            @{ hexagons2triangles( $hexagons, scalar @{ $triangles } ) },
        ]
];

fixFaceOrientations( $polyhedra );

##########################################
# Write out the config file

writeConfigHeader( $context );

$context->{ 'FLOOR_Z' } = -1.0 * $side;
writeConfigFloor( $context );

writeTriangleFaces( $context, $polyhedra );

##########################################
# Display the config if requested

showConfig( $context );

##########################################
# Run the ray-tracer to produce the image

processImage( $context );

