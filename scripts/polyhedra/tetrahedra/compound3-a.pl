#!/usr/bin/perl

use warnings;
use strict;

use Math::Trig;

################################################################################
#
#   compound3-a.pl
#
#       parameters detailed in usage()
#
################################################################################

do '../utils/utilities.pl';

##########################################
# Setup the working environment

my $context = setupContext();
$context->{ 'OFFSETS' } = [ 0, 0, -2 ];

processParameters( $context );

##########################################
# Define the vertices

my $size = 2;

my $vlst = [];

$vlst->[ 0 ] = [ (v0),  -$size, -$size, -$size ];
$vlst->[ 1 ] = [ (v1),  -$size,  $size,  $size ];
$vlst->[ 2 ] = [ (v2),   $size, -$size,  $size ];
$vlst->[ 3 ] = [ (v3),   $size,  $size, -$size ];

##########################################
# Define the faces

my @faces;
my $polyhedra = [];

push @faces, [ "f1", $vlst->[ 0 ], $vlst->[ 1 ], $vlst->[ 2 ], "red" ];
push @faces, [ "f2", $vlst->[ 0 ], $vlst->[ 1 ], $vlst->[ 3 ], "red" ];
push @faces, [ "f3", $vlst->[ 0 ], $vlst->[ 2 ], $vlst->[ 3 ], "red" ];
push @faces, [ "f4", $vlst->[ 1 ], $vlst->[ 2 ], $vlst->[ 3 ], "red" ];
push @{ $polyhedra }, [ "p1", $faces[ 0 ], $faces[ 1 ], $faces[ 2 ], $faces[ 3 ], ];

push @faces, zRotateFace( [ "f5", $vlst->[ 0 ], $vlst->[ 1 ], $vlst->[ 2 ], "green" ], pi / 1.5 );
push @faces, zRotateFace( [ "f6", $vlst->[ 0 ], $vlst->[ 1 ], $vlst->[ 3 ], "green" ], pi / 1.5 );
push @faces, zRotateFace( [ "f7", $vlst->[ 0 ], $vlst->[ 2 ], $vlst->[ 3 ], "green" ], pi / 1.5 );
push @faces, zRotateFace( [ "f8", $vlst->[ 1 ], $vlst->[ 2 ], $vlst->[ 3 ], "green" ], pi / 1.5 );
push @{ $polyhedra }, [ "p1", $faces[ 4 ], $faces[ 5 ], $faces[ 6 ], $faces[ 7 ], ];

push @faces, zRotateFace( [ "f9",  $vlst->[ 0 ], $vlst->[ 1 ], $vlst->[ 2 ], "blue" ], pi / 3.0 );
push @faces, zRotateFace( [ "f10", $vlst->[ 0 ], $vlst->[ 1 ], $vlst->[ 3 ], "blue" ], pi / 3.0 );
push @faces, zRotateFace( [ "f11", $vlst->[ 0 ], $vlst->[ 2 ], $vlst->[ 3 ], "blue" ], pi / 3.0 );
push @faces, zRotateFace( [ "f12", $vlst->[ 1 ], $vlst->[ 2 ], $vlst->[ 3 ], "blue" ], pi / 3.0 );
push @{ $polyhedra }, [ "p1", $faces[ 8 ], $faces[ 9 ], $faces[ 10 ], $faces[ 11 ], ];

fixFaceOrientations( $polyhedra );

##########################################
# Write out the config file

writeConfigHeader( $context );

$context->{ 'FLOOR_Z' } = -$size * sqrt( 2.0 );
writeConfigFloor( $context );

writeTriangleFaces( $context, $polyhedra );

##########################################
# Display the config if requested

showConfig( $context );

##########################################
# Run the ray-tracer to produce the image

processImage( $context );

