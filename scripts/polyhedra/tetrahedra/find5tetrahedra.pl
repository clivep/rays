#!/usr/bin/perl

use warnings;
use strict;

my $phi = ( 1 + sqrt( 5.0 ) ) / 2.0;
my $ph2 = 1.0 / $phi;

my $root8 = 2.0 * sqrt( 2.0 );
my $small = 0.0000001;

my %keys;

my $vlist = [
    [ "v0",  1,  1, -1, ],
    [ "v1",  1,  1,  1, ],
    [ "v2", -1,  1,  1, ],
    [ "v3", -1,  1, -1, ],

    [ "v4",  1, -1, -1 ],
    [ "v5",  1, -1,  1 ],
    [ "v6", -1, -1,  1 ],
    [ "v7", -1, -1, -1 ],

    [ "v8",  0,         $ph2,      $phi      ],
    [ "v9",  0,         -1 * $ph2, $phi      ],
    [ "v10", 0,         $ph2,      -1 * $phi ],
    [ "v11", 0,         -1 * $ph2, -1 * $phi ],

    [ "v12", $ph2,      $phi,      0         ],
    [ "v13", -1 * $ph2, $phi,      0         ],
    [ "v14", $ph2,      -1 * $phi, 0         ],
    [ "v15", -1 * $ph2, -1 * $phi, 0         ],

    [ "v16", $phi,       0,        $ph2      ],
    [ "v17", -1 * $phi,  0,        $ph2      ],
    [ "v18", $phi,       0,       -1 * $ph2  ],
    [ "v19", -1 * $phi,  0,       -1 * $ph2  ],
];
 
my $count = 1;
my $faces = [];

for ( my $v1 = 0; $v1 < 20; $v1++ ) {

    for ( my $v2 = 0; $v2 < 20; $v2++ ) {

        next if ( $v2 == $v1 );

        my $d1 =  dist( $vlist->[ $v1 ], $vlist->[ $v2 ] );
        next if ( mod( $d1 - $root8 ) > $small );

        for ( my $v3 = 0; $v3 < 20; $v3++ ) {
        
            next if ( $v3 == $v1 );
            next if ( $v3 == $v2 );
        
            my $d2 = dist( $vlist->[ $v3 ], $vlist->[ $v2 ] );
            next if ( mod( $d2 - $root8 ) > $small );

            next if ( mod( dist( $vlist->[ $v3 ], $vlist->[ $v1 ] ) - $root8 ) > $small );

            my @arr = sort ( $v1, $v2, $v3 );
            my $key = join '-', @arr;

            next if $keys{ $key };
            $keys{ $key } = 1;

            my $a1 = [
                "a1",
                $vlist->[ $v3 ]->[ 1 ] - $vlist->[ $v2 ]->[ 1 ],
                $vlist->[ $v3 ]->[ 2 ] - $vlist->[ $v2 ]->[ 2 ],
                $vlist->[ $v3 ]->[ 3 ] - $vlist->[ $v2 ]->[ 3 ],
            ];

            my $a2 = [
                "a2",
                $vlist->[ $v1 ]->[ 1 ] - $vlist->[ $v2 ]->[ 1 ],
                $vlist->[ $v1 ]->[ 2 ] - $vlist->[ $v2 ]->[ 2 ],
                $vlist->[ $v1 ]->[ 3 ] - $vlist->[ $v2 ]->[ 3 ],
            ];

            my $nrm = [
                "nrm",
                ( $a1->[ 2 ] * $a2->[ 3 ] ) - ( $a1->[ 3 ] * $a2->[ 2 ] ),
                ( $a1->[ 3 ] * $a2->[ 1 ] ) - ( $a1->[ 1 ] * $a2->[ 3 ] ),
                ( $a1->[ 1 ] * $a2->[ 2 ] ) - ( $a1->[ 2 ] * $a2->[ 1 ] ),
            ];

            my $cen = [
                "cen",
                ( $vlist->[ $v1 ]->[ 1 ] + $vlist->[ $v2 ]->[ 1 ] + $vlist->[ $v3 ]->[ 1 ] ) / 3.0,
                ( $vlist->[ $v1 ]->[ 2 ] + $vlist->[ $v2 ]->[ 2 ] + $vlist->[ $v3 ]->[ 2 ] ) / 3.0,
                ( $vlist->[ $v1 ]->[ 3 ] + $vlist->[ $v2 ]->[ 3 ] + $vlist->[ $v3 ]->[ 3 ] ) / 3.0,
            ];

            my $dot =
                    $cen->[ 1 ] * $nrm->[ 1 ] +
                    $cen->[ 2 ] * $nrm->[ 2 ] +
                    $cen->[ 3 ] * $nrm->[ 3 ]
            ;

            if ( $dot > 0 ) {
                printf "%d: %d-%d-%d\n", $count++, $v1, $v2, $v3;
                push @{ $faces }, sprintf "%d-%d-%d", $v1, $v2, $v3;
            }
            else {
                printf "%d: %d-%d-%d (*)\n", $count++, $v3, $v2, $v1;
                push @{ $faces }, sprintf "%d-%d-%d", $v3, $v2, $v1;
            }
        }
    }
}

# group the faces into polyhedra

my $polyhedra = [];

for my $face ( @{ $faces } ) {

    # find an existing polyhedron with 2 common vertices
    my $found = 0;
    my @vertices = split /-/, $face;

    for my $polyhedron ( @{ $polyhedra } ) {

        my $count = 0;
        $count++ if ( defined $polyhedron->{ 'vertices' }->{ $vertices[ 0 ] } );
        $count++ if ( defined $polyhedron->{ 'vertices' }->{ $vertices[ 1 ] } );
        $count++ if ( defined $polyhedron->{ 'vertices' }->{ $vertices[ 2 ] } );

        if ( $count > 1 ) {
    
            $polyhedron->{ 'vertices' }->{ $vertices[ 0 ] } = 1;
            $polyhedron->{ 'vertices' }->{ $vertices[ 1 ] } = 1;
            $polyhedron->{ 'vertices' }->{ $vertices[ 2 ] } = 1;
    
            push @{ $polyhedron->{ 'faces' } }, $face;

            $found = 1;
            last;
        }
    }

    if ( ! $found ) {   # must belong to a new polyhedron then
        
        push @{ $polyhedra }, {

            'vertices'      => { 
                                  $vertices[ 0 ] =>   1,
                                  $vertices[ 1 ] =>   1,
                                  $vertices[ 2 ] =>   1,
                               },
            'faces'         => [ $face ]
        };
    }
}

for my $polyhedron ( @{ $polyhedra } ) {

    my $sep = 'Polyhedron: ';
    map {
        printf "$sep$_";
        $sep = '-';

    } ( keys %{ $polyhedron->{ 'vertices' } } );

    $sep = ' - faces: ';
    map {
        printf "$sep$_";
        $sep = ' / ';

    } ( @{ $polyhedron->{ 'faces' } } );

    printf "\n";
}

# group the polyhedra into 2 groups that do not share any vertices

exit;

sub dist {

    my $_v1 = shift;
    my $_v2 = shift;

    return sqrt (
        ( $_v1->[1] - $_v2->[1] ) * ( $_v1->[1] - $_v2->[1] ) +
        ( $_v1->[2] - $_v2->[2] ) * ( $_v1->[2] - $_v2->[2] ) +
        ( $_v1->[3] - $_v2->[3] ) * ( $_v1->[3] - $_v2->[3] )
    );
}

sub mod {

    my $_v = shift;

    return ( -1.0 * $_v ) if ( $_v < 0 );

    return $_v;
}
