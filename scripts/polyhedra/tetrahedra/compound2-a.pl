#!/usr/bin/perl

use warnings;
use strict;

use Math::Trig;

################################################################################
#
#   compound2-a.pl
#
#       parameters detailed in usage()
#
################################################################################

do '../utils/utilities.pl';

##########################################
# Setup the working environment

my $context = setupContext();
$context->{ 'OFFSETS' } = [ 0, 0, -2 ];

processParameters( $context );

##########################################
# Define the vertices

my $size = 2;

my $vlst = [];

$vlst->[ 0 ] = [ (v0),   $size,  $size,  $size ];
$vlst->[ 1 ] = [ (v1),   $size, -$size, -$size ];
$vlst->[ 2 ] = [ (v2),  -$size,  $size, -$size ];
$vlst->[ 3 ] = [ (v3),  -$size, -$size,  $size ];

$vlst->[ 4 ] = [ (v4),  -$size, -$size, -$size ];
$vlst->[ 5 ] = [ (v5),  -$size,  $size,  $size ];
$vlst->[ 6 ] = [ (v6),   $size, -$size,  $size ];
$vlst->[ 7 ] = [ (v7),   $size,  $size, -$size ];

##########################################
# Define the faces

my @faces;
my $polyhedra = [];

push @faces, [ "f1", $vlst->[ 0 ], $vlst->[ 1 ], $vlst->[ 2 ], "red" ];
push @faces, [ "f2", $vlst->[ 0 ], $vlst->[ 1 ], $vlst->[ 3 ], "red" ];
push @faces, [ "f3", $vlst->[ 0 ], $vlst->[ 2 ], $vlst->[ 3 ], "red" ];
push @faces, [ "f4", $vlst->[ 1 ], $vlst->[ 2 ], $vlst->[ 3 ], "red" ];
push @{ $polyhedra }, [ "p1", $faces[ 0 ], $faces[ 1 ], $faces[ 2 ], $faces[ 3 ], ];

push @faces, [ "f5", $vlst->[ 4 ], $vlst->[ 5 ], $vlst->[ 6 ], "green" ];
push @faces, [ "f6", $vlst->[ 4 ], $vlst->[ 5 ], $vlst->[ 7 ], "green" ];
push @faces, [ "f7", $vlst->[ 4 ], $vlst->[ 6 ], $vlst->[ 7 ], "green" ];
push @faces, [ "f8", $vlst->[ 5 ], $vlst->[ 6 ], $vlst->[ 7 ], "green" ];
push @{ $polyhedra }, [ "p2", $faces[ 4 ], $faces[ 5 ], $faces[ 6 ], $faces[ 7 ], ];

fixFaceOrientations( $polyhedra );

##########################################
# Write out the config file

writeConfigHeader( $context );

$context->{ 'FLOOR_Z' } = -$size * sqrt( 2.0 );
writeConfigFloor( $context );

writeTriangleFaces( $context, $polyhedra );

##########################################
# Display the config if requested

showConfig( $context );

##########################################
# Run the ray-tracer to produce the image

processImage( $context );

