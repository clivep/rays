#!/usr/bin/perl

use warnings;
use strict;

use Math::Trig;

################################################################################
#
#   compond-truncated-tetrahedron.pl
#
#       parameters detailed in usage()
#
################################################################################

do '../utils/utilities.pl';

##########################################
# Setup the working environment

my $context = setupContext();
$context->{ 'OFFSETS' } = [ 0, 0, -1.5 ];

processParameters( $context );

##########################################
# Define the vertices

my $side = 3;

my $vlist1 = [];
my $vlist2 = [];

my $size = $side / sqrt( 8.0 );

# of the odd permutataions take those with an even number of -ve coordinates
map {
    my $count = 0;
    $count++ if ( $_->[ 1 ] < 0 );
    $count++ if ( $_->[ 2 ] < 0 );
    $count++ if ( $_->[ 3 ] < 0 );

    if ( ( $count == 0 ) || ( $count == 2 ) ) {
        push @{ $vlist1 }, $_;
    }
    else {
        push @{ $vlist2 }, $_;
    }
} @{ vertexPerms(
    [ $size, $size, $size * 3.0 ],
    [ 'ABC', 'BCA', 'CAB' ],
) };

setupVertexConnections( $vlist1, $side );
my $triangles1 = findTriangles( $vlist1, $side, "red" );
my $hexagons1 = findHexagons( $vlist1, $side, "red" );

setupVertexConnections( $vlist2, $side );
my $triangles2 = findTriangles( $vlist2, $side, "yellow" );
my $hexagons2 = findHexagons( $vlist2, $side, "yellow" );

my $polyhedra = [
        [ "p1",
            @{ $triangles1 },
            @{ hexagons2triangles( $hexagons1 ) },
            @{ $triangles2 },
            @{ hexagons2triangles( $hexagons2 ) },
        ]
];

fixFaceOrientations( $polyhedra );

##########################################
# Write out the config file

writeConfigHeader( $context );

$context->{ 'FLOOR_Z' } = -1.0 * $side;
writeConfigFloor( $context );

writeTriangleFaces( $context, $polyhedra );

##########################################
# Display the config if requested

showConfig( $context );

##########################################
# Run the ray-tracer to produce the image

processImage( $context );

