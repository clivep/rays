#!/usr/bin/perl

use warnings;
use strict;

use Math::Trig;

################################################################################
#
#   tetrahedron.pl
#
#       parameters detailed in usage()
#
################################################################################

do '../utils/utilities.pl';

##########################################
# Setup the working environment

my $context = setupContext();
$context->{ 'OFFSETS' } = [ 0, 0, -2 ];

processParameters( $context );

##########################################
# Define the vertices

my $size = 2.5;

my $vlist = [];

$vlist->[ 0 ] = [ (v0),   $size,  $size,  $size ];
$vlist->[ 1 ] = [ (v1),   $size, -$size, -$size ];
$vlist->[ 2 ] = [ (v2),  -$size,  $size, -$size ];
$vlist->[ 3 ] = [ (v3),  -$size, -$size,  $size ];

$vlist->[ 0 ] = [ (v0),  -$size, -$size, -$size ];
$vlist->[ 1 ] = [ (v1),  -$size,  $size,  $size ];
$vlist->[ 2 ] = [ (v2),   $size, -$size,  $size ];
$vlist->[ 3 ] = [ (v3),   $size,  $size, -$size ];

##########################################
# Define the faces

my @faces;
my $polyhedra = [];

push @faces, [ "f1", $vlist->[ 0 ], $vlist->[ 1 ], $vlist->[ 2 ], "red" ];
push @faces, [ "f2", $vlist->[ 0 ], $vlist->[ 1 ], $vlist->[ 3 ], "blue" ];
push @faces, [ "f3", $vlist->[ 0 ], $vlist->[ 2 ], $vlist->[ 3 ], "green" ];
push @faces, [ "f4", $vlist->[ 1 ], $vlist->[ 2 ], $vlist->[ 3 ], "yellow" ];
push @{ $polyhedra }, [ "p1", $faces[ 0 ], $faces[ 1 ], $faces[ 2 ], $faces[ 3 ], ];

fixFaceOrientations( $polyhedra );

##########################################
# Write out the config file

writeConfigHeader( $context );

$context->{ 'FLOOR_Z' } = -$size * sqrt( 2.0 );
writeConfigFloor( $context );

writeTriangleFaces( $context, $polyhedra );

##########################################
# Display the config if requested

showConfig( $context );

##########################################
# Run the ray-tracer to produce the image

processImage( $context );

