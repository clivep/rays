#!/usr/bin/perl

use warnings;
use strict;

use Math::Trig;

################################################################################
#
#   rhombicosidodecahedron.pl
#
#       parameters detailed in usage()
#
################################################################################

do '../utils/utilities.pl';

##########################################
# Setup the working environment

my $context = setupContext();
$context->{ 'OFFSETS' } = [ 0, 0, -1.5 ];

processParameters( $context );

##########################################
# Define the vertices

my $size = 0.8;

# Vertices are the even permutations of the base vectors
my $vlist = [];

my $phi2 = phi() * phi();
my $phi3 = $phi2 * phi();

push @{ $vlist }, @{ vertexPerms( 
    [ $size, $size, $size * $phi3 ],
    [ 'ABC', 'CAB', 'BCA' ],
) };

push @{ $vlist }, @{ vertexPerms( 
    [ $size * $phi2, $size * phi(), 2.0 * $size * phi() ],
    [ 'ABC', 'CAB', 'BCA' ],
    scalar @{ $vlist }
) };

push @{ $vlist }, @{ vertexPerms( 
    [ $size * ( 2.0 + phi() ), 0, $size * $phi2 ],
    [ 'AbC', 'CAb', 'bCA' ],
    scalar @{ $vlist }
) };

setupVertexConnections( $vlist, 2.0 * $size );

##########################################
# Define the faces

my $triangles = findTriangles( $vlist, 2.0 * $size, "yellow" );
my $squares = findSquares( $vlist, 2.0 * $size, "blue" );
my $pentagons = findPentagons( $vlist, 2.0 * $size, "red" );

my $polyhedra = [
    [ "p1",
        @{ $triangles },
        @{ squares2triangles( $squares ) },
        @{ pentagons2triangles( $pentagons ) },
    ]
];

fixFaceOrientations( $polyhedra );

##########################################
# Write out the config file

writeConfigHeader( $context );

$context->{ 'FLOOR_Z' } = -2.0 * $size * phi();
writeConfigFloor( $context );

writeTriangleFaces( $context, $polyhedra );

##########################################
# Display the config if requested

showConfig( $context );

##########################################
# Run the ray-tracer to produce the image

processImage( $context );
