#!/usr/bin/perl

use warnings;
use strict;

use Math::Trig;

################################################################################
#
#  octahedron.pl
#
#       parameters detailed in usage()
#
################################################################################

do '../utils/utilities.pl';

##########################################
# Setup the working environment

my $context = setupContext();
$context->{ 'OFFSETS' } = [ 0, 0, -2 ];

processParameters( $context );

##########################################
# Setup the figure

my $side = 5.0;

my $vlist = [];

my $size = $side / sqrt( 2.0 );

push @{ $vlist },  @{ vertexPerms( 
    [ $size, 0.0, 0.0 ],
    [ 'Abc', 'bAc', 'cbA' ],
) };

setupVertexConnections( $vlist, $side );

my $triangles = findTriangles( $vlist, $side, "red" );

my $polyhedra = [
        [ "p1",
            @{ $triangles },
        ]
];

fixFaceOrientations( $polyhedra );

##########################################
# Write out the config file

writeConfigHeader( $context );

$context->{ 'FLOOR_Z' } = -1.0 * $size;
writeConfigFloor( $context );

writeTriangleFaces( $context, $polyhedra );

##########################################
# Display the config if requested

showConfig( $context );

##########################################
# Run the ray-tracer to produce the image

processImage( $context );

