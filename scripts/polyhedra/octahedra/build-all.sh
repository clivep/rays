#!/bin/sh -x
#
#   Usage: build-all.sh [-no-labels]

mkdir images > /dev/null 2>&1

./octahedron.pl -r 200 -rz 30 -rx 45 -ry 55 -o images/octahedron.pdf -l 'Octahedron'
