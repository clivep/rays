#!/usr/bin/perl

################################################################################
#
#   utilities.pl
#
#       Functions to support ray tracing polyhedra
#
#   Structures   
#       vertex:     [ label, i, j, k, [ connections] ]
#       vector:     [ label, i, j, k ]
#       face:       [ label, [vtx1], [vtx2], ... , color ]
#       polygon:    [ label, [face1], [face2], ... ]
#
################################################################################

use warnings;
use strict;

use File::Temp qw/tempfile/;
use Math::Trig;

use constant RAY_TRACER =>  '../../../bin/rays';
use constant PHI        =>  ( 1.0 + sqrt( 5.0 ) ) / 2.0;

use constant SMALL      =>  0.0000001;

$| = 1;

##########################################
# Setup the environment

sub setupContext {
    
    my $context = {};

    $context->{ 'OUTFILE' } = '';

    $context->{ 'SHOW_CONFIG' } = 0;
    $context->{ 'SHOW_PROGRESS' } = 0;

    $context->{ 'IMAGE_SIZE' } = 5;
    $context->{ 'ROTATIONS' } = [ 0, 0, 0 ];
    $context->{ 'RESOLUTION' } = 50;

    $context->{ 'SCREEN' } = [ 40, 40, 45 ];

    $context->{ 'EYE_DIST' } = 500;

    $context->{ 'FLOOR_Z' } = 0;
    $context->{ 'FLOOR_SIZE' } = 10;

    $context->{ 'ANTI_ALIAS' } = 'on';

    $context->{ 'OFFSETS' } = [ 0, 0, 0 ];

    $context->{ 'PRISM_HEIGHT' } = 4;

    $context->{ 'STAR_CROSSED' } = 0;
    $context->{ 'STAR_RADIUS' } = 4;
    $context->{ 'STAR_POINTS' } = 7;
    $context->{ 'STAR_EVERY' } = 3;

    return $context;
}

##########################################
# Process the parameters

sub processParameters {

    my $context = shift;

    for ( my $a = 0; $a <= $#ARGV; $a += 2 ) {

        usage() if ( $ARGV[ $a ] eq "-h" );

        if ( $ARGV[ $a ] eq "-m" ) {
            $context->{ 'SHOW_PROGRESS' } = 1;
            $a--;
            next;
        }

        if ( $ARGV[ $a ] eq "-sc" ) {
            $context->{ 'STAR_CROSSED' } = 1;
            $a--;
            next;
        }

        if ( $ARGV[ $a ] eq "-no-anti-alias" ) {
            $context->{ 'ANTI_ALIAS' } = 'off';
            $a--;
            next;
        }

        if ( $ARGV[ $a ] eq "-mono" ) {
            $context->{ 'MONO' } = 1;
            $a--;
            next;
        }

        if ( $ARGV[ $a ] eq "-?" ) {
            $context->{ 'SHOW_CONFIG' } = 1;
            $a--;
            next;
        }

        unless ( $ARGV[ $a + 1 ] ) {
            printf "Missing value for param %s\n", $ARGV[ $a ];
            exit -1;
        }

        if ( $ARGV[ $a ] eq "-o" ) {
            $context->{ 'OUTFILE' } = $ARGV[ $a + 1 ];
        }
        elsif ( $ARGV[ $a ] eq "-e" ) {
            $context->{ 'EYE_DIST' } = $ARGV[ $a + 1 ];
        }
        elsif ( $ARGV[ $a ] eq "-f" ) {
            $context->{ 'FLOOR_Z' } = $ARGV[ $a + 1 ];
        }
        elsif ( $ARGV[ $a ] eq "-l" ) {
            $context->{ 'LABEL' } = $ARGV[ $a + 1 ];
        }
        elsif ( $ARGV[ $a ] eq "-i" ) {
            $context->{ 'IMAGE_SIZE' }->[ 2 ] = $ARGV[ $a + 1 ];
        }
        elsif ( $ARGV[ $a ] eq "-r" ) {
            $context->{ 'RESOLUTION' } = $ARGV[ $a + 1 ];
        }
        elsif ( $ARGV[ $a ] eq "-s" ) {
            $context->{ 'FLOOR_SIZE' } = $ARGV[ $a + 1 ];
        }
        elsif ( $ARGV[ $a ] eq "-ox" ) {
            $context->{ 'OFFSETS' }->[ 0 ] = $ARGV[ $a + 1 ];
        }
        elsif ( $ARGV[ $a ] eq "-oy" ) {
            $context->{ 'OFFSETS' }->[ 1 ] = $ARGV[ $a + 1 ];
        }
        elsif ( $ARGV[ $a ] eq "-oz" ) {
            $context->{ 'OFFSETS' }->[ 2 ] = $ARGV[ $a + 1 ];
        }
        elsif ( $ARGV[ $a ] eq "-ph" ) {
            $context->{ 'PRISM_HEIGHT' } = $ARGV[ $a + 1 ];
        }
        elsif ( $ARGV[ $a ] eq "-rx" ) {
            $context->{ 'ROTATIONS' }->[ 0 ] = deg2rad( $ARGV[ $a + 1 ] );
        }
        elsif ( $ARGV[ $a ] eq "-ry" ) {
            $context->{ 'ROTATIONS' }->[ 1 ] = deg2rad( $ARGV[ $a + 1 ] );
        }
        elsif ( $ARGV[ $a ] eq "-rz" ) {
            $context->{ 'ROTATIONS' }->[ 2 ] = deg2rad( $ARGV[ $a + 1 ] );
        }
        elsif ( $ARGV[ $a ] eq "-se" ) {
            $context->{ 'STAR_EVERY' } = $ARGV[ $a + 1 ];
        }
        elsif ( $ARGV[ $a ] eq "-sp" ) {
            $context->{ 'STAR_POINTS' } = $ARGV[ $a + 1 ];
        }
        elsif ( $ARGV[ $a ] eq "-sr" ) {
            $context->{ 'STAR_RADIUS' } = $ARGV[ $a + 1 ];
        }
        elsif ( $ARGV[ $a ] eq "-sx" ) {
            $context->{ 'SCREEN' }->[ 0 ] = $ARGV[ $a + 1 ];
        }
        elsif ( $ARGV[ $a ] eq "-sy" ) {
            $context->{ 'SCREEN' }->[ 1 ] = $ARGV[ $a + 1 ];
        }
        elsif ( $ARGV[ $a ] eq "-sz" ) {
            $context->{ 'SCREEN' }->[ 2 ] = $ARGV[ $a + 1 ];
        }
        else {
            printf "Unknown parameter: %s\n", $ARGV[ $a ];
            exit -1;
        }
    }

    unless ( $context->{ 'OUTFILE' } || $context->{ 'SHOW_CONFIG' } ) {
    
        printf "Missing outfile parameter\n";
        exit -1;
    }

    if ( $context->{ 'LABEL' } ) {
    
        if ( $context->{ 'OUTFILE' } !~ /\.pdf$/ ) {

            printf "Label only available for pdf images\n";
            exit -1;
        }
        elsif ( $context->{ 'RESOLUTION' } < 50 ) {
    
            printf "Resolution must be at least 50 for a label\n";
            exit -1;
        }
        else {

            # the label is added as Postscript so stop the tracer creating a pdf
            $context->{ 'OUTFILE' } .= '.ps';
        }
    }

    ( $context->{ 'TMP_FH' }, $context->{ 'CONFIG' } ) = tempfile();
}

##########################################
# Return a list of icosahedron vertices

sub icosahedronVertices {

    my $side = shift;

    my $size = $side / 2.0;

    my $phi = $size * PHI;

    return [
            [ "v0",    0.0,  $size,   $phi ],
            [ "v1",    0.0,  $size,  -$phi ],
            [ "v2",    0.0, -$size,   $phi ],
            [ "v3",    0.0, -$size,  -$phi ],

            [ "v4",  $size,   $phi,    0.0 ],
            [ "v5",  $size,  -$phi,    0.0 ],
            [ "v6", -$size,   $phi,    0.0 ],
            [ "v7", -$size,  -$phi,    0.0 ],

            [ "v8",   $phi,    0.0,  $size ],
            [ "v9",  -$phi,    0.0,  $size ],
            [ "v10",  $phi,    0.0, -$size ],
            [ "v11", -$phi,    0.0, -$size ],
    ];
}

##########################################
# Return a list of dodecahedron vertices

sub dodecahedronVertices {

    my $side = shift;

    my $size = $side / ( sqrt( 5.0 ) - 1.0 );

    my $phi = $size * PHI;
    my $ph2 = $size / PHI;

    return [
            [ "v0",   $size,  $size, -$size ],
            [ "v1",   $size,  $size,  $size ],
            [ "v2",  -$size,  $size,  $size ],
            [ "v3",  -$size,  $size, -$size ],

            [ "v4",   $size, -$size, -$size ],
            [ "v5",   $size, -$size,  $size ],
            [ "v6",  -$size, -$size,  $size ],
            [ "v7",  -$size, -$size, -$size ],

            [ "v8",       0,   $ph2,   $phi ],
            [ "v9",       0,  -$ph2,   $phi ],
            [ "v10",      0,   $ph2,  -$phi ],
            [ "v11",      0,  -$ph2,  -$phi ],

            [ "v12",   $ph2,   $phi,    0   ],
            [ "v13",  -$ph2,   $phi,    0   ],
            [ "v14",   $ph2,  -$phi,    0   ],
            [ "v15",  -$ph2,  -$phi,    0   ],

            [ "v16",   $phi,      0,   $ph2 ],
            [ "v17",  -$phi,      0,   $ph2 ],
            [ "v18",   $phi,      0,  -$ph2 ],
            [ "v19",  -$phi,      0,  -$ph2 ],
    ];
}

##########################################
# Rotate the face about the X axis

sub xRotateFace {

    my $face = shift;
    my $xrot = shift;

    my $sides = scalar @{ $face } - 2;  # ignore name and color entries

    my $new_face = [ $face->[ 0 ] ];

    for ( my $v = 1; $v < $sides + 1; $v++ ) {

        push @{ $new_face }, [
            $face->[ $v ]->[ 0 ],
            $face->[ $v ]->[ 1 ],
            ( $face->[ $v ]->[ 2 ] * cos( $xrot ) ) + ( $face->[ $v ]->[ 3 ] * sin( $xrot ) ),
            ( $face->[ $v ]->[ 3 ] * cos( $xrot ) ) - ( $face->[ $v ]->[ 2 ] * sin( $xrot ) ),
        ];
    };

    push @{ $new_face }, $face->[ $sides + 1 ];

    return $new_face;
}

##########################################
# Rotate the face about the Y axis

sub yRotateFace {

    my $face = shift;
    my $yrot = shift;
    
    my $sides = scalar @{ $face } - 2;  # ignore name and color entries

    my $new_face = [ $face->[ 0 ] ];

    for ( my $v = 1; $v < $sides + 1; $v++ ) {

        push @{ $new_face }, [
            $face->[ $v ]->[ 0 ],
            ( $face->[ $v ]->[ 1 ] * cos( $yrot ) ) - ( $face->[ $v ]->[ 3 ] * sin( $yrot ) ),
            $face->[ $v ]->[ 2 ],
            ( $face->[ $v ]->[ 1 ] * sin( $yrot ) ) + ( $face->[ $v ]->[ 3 ] * cos( $yrot ) ),
        ];
    }

    push @{ $new_face }, $face->[ $sides + 1 ];

    return $new_face;
}

##########################################
# Rotate the face about the Z axis

sub zRotateFace {

    my $face = shift;
    my $zrot = shift;

    my $sides = scalar @{ $face } - 2;  # ignore name and color entries

    my $new_face = [ $face->[ 0 ] ];

    for ( my $v = 1; $v < $sides + 1; $v++ ) {

        push @{ $new_face }, [
            $face->[ $v ]->[ 0 ],
            ( $face->[ $v ]->[ 1 ] * cos( $zrot ) ) + ( $face->[ $v ]->[ 2 ] * sin( $zrot ) ),
            ( $face->[ $v ]->[ 2 ] * cos( $zrot ) ) - ( $face->[ $v ]->[ 1 ] * sin( $zrot ) ),
            $face->[ $v ]->[ 3 ],
        ];
    }

    push @{ $new_face }, $face->[ $sides + 1 ];

    return $new_face;
}

##########################################
# Rotate the face about 2 axes

sub xyRotateFace { return yRotateFace( xRotateFace( shift, shift ), shift ) }
sub xzRotateFace { return zRotateFace( xRotateFace( shift, shift ), shift ) }
sub yzRotateFace { return zRotateFace( yRotateFace( shift, shift ), shift ) }

##########################################
# Rotate the face about 3 axes

sub xyzRotateFace { return zRotateFace( xyRotateFace( shift, shift, shift ), shift ) }

##########################################
# Rotate the face by the default rotation

sub rotateFace {

    my $context = shift;
    my $face = shift;

    my $new_face;
    $new_face = xRotateFace( $face, $context->{ 'ROTATIONS' }->[ 0 ] );
    $new_face = yRotateFace( $new_face, $context->{ 'ROTATIONS' }->[ 1 ] );
    $new_face = zRotateFace( $new_face, $context->{ 'ROTATIONS' }->[ 2 ] );

    return $new_face;
}

##########################################
# Write image floor

sub writeConfigFloor {
    
    my $context = shift;

    my $fh = $context->{ 'TMP_FH' };
    my $offset = $context->{ 'FLOOR_SIZE' } / -2.0;

    printf $fh "
    object:
        type: plane
        surface: default
        color-name: grey5
        scheme-name: plane
        origin:
            i: $offset
            j: $offset
            k: $context->{ 'FLOOR_Z' }
        axis1:
            i: $context->{ 'FLOOR_SIZE' }
            j: 0
            k: 0
        axis2:
            i: 0
            j: $context->{ 'FLOOR_SIZE' }
            k: 0
";
}

##########################################
# Write the config header details

sub writeConfigHeader {
    
    my $context = shift;
    
    my $fh = $context->{ 'TMP_FH' };
    
    printf $fh "
defaults:
    background:
        red: 0.0
        green: 0.0
        blue: 0.0
    properties:
        anti-alias: $context->{ 'ANTI_ALIAS' }
        diffusion_index: 1.0
        specular_index: 0.3
        specular_exponent: 7.0
        color_scramble: 0.05

screen:
    centre:
        i: $context->{ 'SCREEN' }->[ 0 ]
        j: $context->{ 'SCREEN' }->[ 1 ]
        k: $context->{ 'SCREEN' }->[ 2 ]
    offset:
        i: $context->{ 'OFFSETS' }->[ 0 ]
        j: $context->{ 'OFFSETS' }->[ 1 ]
        k: $context->{ 'OFFSETS' }->[ 2 ]
    eye_dist: $context->{ 'EYE_DIST' }
    height: 10
    width: 10

factor-scheme-defs:
    name: face
        diffusion_index: 0.95
        specular_index: 0.4
        specular_exponent: 5.0
        reflective_index: 0.15
    name: plane
        diffusion_index: 0.9
        specular_index: 0.4
        specular_exponent: 5.0
        reflective_index: 0.2

custom-color-defs:
    name: grey8
        red: 0.80
        green: 0.80
        blue: 0.80
    name: grey5
        red: 0.50
        green: 0.50
        blue: 0.50
    name: red
        red: 0.80
        green: 0.00
        blue: 0.00
    name: green
        red: 0.00
        green: 0.80
        blue: 0.00
    name: blue
        red: 0.00
        green: 0.00
        blue: 0.80
    name: yellow
        red: 0.80
        green: 0.80
        blue: 0.00
    name: cyan
        red: 0.00
        green: 0.80
        blue: 0.80
    name: magenta
        red: 0.80
        green: 0.00
        blue: 0.80
    name: white
        red: 1.00
        green: 1.00
        blue: 1.00
    name: orange
        red: 1.00
        green: 0.65
        blue: 0.00

image:
    height: $context->{ 'IMAGE_SIZE' }
    width: $context->{ 'IMAGE_SIZE' }
    units: IN
    resolution: $context->{ 'RESOLUTION' }

illumination:
    ambience: 0.3
    lamp:
        brightness: 100
        sample_size: 20
        centre:
            i: 0
            j: 0
            k: 10
        radius: 5
    lamp:
        brightness: 100
        sample_size: 20
        centre:
            i: 10
            j: 0
            k: 10
        radius: 5
objects:
";
}

##########################################
# Write image triangles

sub writeTriangleFaces {

    my $context = shift;
    my $polyhedra = shift;

    for( my $p = 0; $p < scalar @{ $polyhedra }; $p++ ) {

        my $polyhedron = $polyhedra->[ $p ];

        for( my $f = 1; $f < scalar @{ $polyhedron }; $f++ ) {

            my $triangle = rotateFace( $context, $polyhedron->[ $f ] );

            my $fh = $context->{ 'TMP_FH' };
    
            my $color = $context->{ 'MONO' } ? "grey8" : $triangle->[ 4 ];

            printf $fh "    object:\n";
            printf $fh "        type: triangle\n";
            printf $fh "        surface: default\n";
            printf $fh "        color-name: %s\n", $color;
            printf $fh "        scheme-name: face\n";
    
            printf $fh "        vertex1:\n";
            printf $fh "            i: %s\n", $triangle->[ 1 ]->[ 1 ];
            printf $fh "            j: %s\n", $triangle->[ 1 ]->[ 2 ];
            printf $fh "            k: %s\n", $triangle->[ 1 ]->[ 3 ];
    
            printf $fh "        vertex2:\n";
            printf $fh "            i: %s\n", $triangle->[ 2 ]->[ 1 ];
            printf $fh "            j: %s\n", $triangle->[ 2 ]->[ 2 ];
            printf $fh "            k: %s\n", $triangle->[ 2 ]->[ 3 ];
    
            printf $fh "        vertex3:\n";
            printf $fh "            i: %s\n", $triangle->[ 3 ]->[ 1 ];
            printf $fh "            j: %s\n", $triangle->[ 3 ]->[ 2 ];
            printf $fh "            k: %s\n", $triangle->[ 3 ]->[ 3 ];
        }
    }
}

##########################################
# Write image planes

sub writePlaneFaces {
    
    my $context = shift;
    my $polyhedra = shift;

    for( my $p = 0; $p < scalar @{ $polyhedra }; $p++ ) {

        my $polyhedron = $polyhedra->[ $p ];

        for( my $f = 1; $f < scalar @{ $polyhedron }; $f++ ) {

            my $plane = rotateFace( $context, $polyhedron->[ $f ] );

            my $color = $context->{ 'MONO' } ? "grey8" : $plane->[ 5 ];

            my $fh = $context->{ 'TMP_FH' };
    
            printf $fh "    object:\n";
            printf $fh "        type: plane\n";
            printf $fh "        surface: default\n";
            printf $fh "        color-name: %s\n", $color;
            printf $fh "        scheme-name: face\n";
    
            printf $fh "        origin:\n";
            printf $fh "            i: %s\n", $plane->[ 2 ]->[ 1 ];
            printf $fh "            j: %s\n", $plane->[ 2 ]->[ 2 ];
            printf $fh "            k: %s\n", $plane->[ 2 ]->[ 3 ];
    
            printf $fh "        axis1:\n";
            printf $fh "            i: %s\n", $plane->[ 3 ]->[ 1 ] - $plane->[ 2 ]->[ 1 ];
            printf $fh "            j: %s\n", $plane->[ 3 ]->[ 2 ] - $plane->[ 2 ]->[ 2 ];
            printf $fh "            k: %s\n", $plane->[ 3 ]->[ 3 ] - $plane->[ 2 ]->[ 3 ];
    
            printf $fh "        axis2:\n";
            printf $fh "            i: %s\n", $plane->[ 1 ]->[ 1 ] - $plane->[ 2 ]->[ 1 ];
            printf $fh "            j: %s\n", $plane->[ 1 ]->[ 2 ] - $plane->[ 2 ]->[ 2 ];
            printf $fh "            k: %s\n", $plane->[ 1 ]->[ 3 ] - $plane->[ 2 ]->[ 3 ];
        }
    }
}

##########################################
# Display the config (if requested)

sub showConfig {

    my $context = shift;

    return unless $context->{ 'SHOW_CONFIG' };

    system( "cat $context->{ 'CONFIG' }" );
    unlink $context->{ 'CONFIG' };

    exit 0;
}

##########################################
# Produce the image

sub processImage {

    my $context = shift;

    my $cmd = RAY_TRACER . " -c $context->{ 'CONFIG' } -f $context->{ 'OUTFILE' }";
    
    $cmd .= " -m" if $context->{ 'SHOW_PROGRESS' };
   
    system( $cmd );
    
    unlink $context->{ 'CONFIG' };

    if ( $context->{ 'LABEL' } ) {

        my $tmp_psfile = $context->{ 'OUTFILE' };
        $context->{ 'OUTFILE' } =~ s/\.ps$//;

        labelImage( $context->{ 'LABEL' }, $tmp_psfile );

        $cmd = sprintf "ps2pdf $tmp_psfile %s", $context->{ 'OUTFILE' };
        system( $cmd );

        unlink $tmp_psfile;
    }
   
    exit 0;
}
 
##########################################
# Normalise the given vector

sub normalise {

    my $a1 = shift;

    my $val = sqrt( 
        ( $a1->[ 1 ] * $a1->[ 1 ] ) +
        ( $a1->[ 2 ] * $a1->[ 2 ] ) +
        ( $a1->[ 3 ] * $a1->[ 3 ] )
    );

    $a1->[ 1 ] /= $val;
    $a1->[ 2 ] /= $val;
    $a1->[ 3 ] /= $val;
}
 
##########################################
# Return the dot product of 2 vectors

sub dot {

    my $a1 = shift;
    my $a2 = shift;

    return
        $a1->[ 1 ] * $a2->[ 1 ] +
        $a1->[ 2 ] * $a2->[ 2 ] +
        $a1->[ 3 ] * $a2->[ 3 ]
    ;
}
 
##########################################
# Create a vector from vtx1 to vtx2

sub vector {

    my $vtx1 = shift;
    my $vtx2 = shift;

    return [
        sprintf( "%s->%s", $vtx2->[ 0 ] , $vtx1->[ 0 ] ),
        $vtx2->[ 1 ] - $vtx1->[ 1 ],
        $vtx2->[ 2 ] - $vtx1->[ 2 ],
        $vtx2->[ 3 ] - $vtx1->[ 3 ],
    ];
}
 
##########################################
# Add 2 vectors

sub addVector {

    my $vr1 = shift;
    my $vr2 = shift;

    return [
        sprintf( "%s+%s", $vr1->[ 0 ] , $vr2->[ 0 ] ),
        $vr1->[ 1 ] + $vr2->[ 1 ],
        $vr1->[ 2 ] + $vr2->[ 2 ],
        $vr1->[ 3 ] + $vr2->[ 3 ],
    ];
}

##########################################
# Move along a vector from vtx1 to vtx2

sub mulVector {

    my $vtx1 = shift;
    my $vtx2 = shift;
    my $dist = shift;

    my $vr = vector( $vtx1, $vtx2 );
    normalise( $vr );

    return [

        $vr->[ 0 ],
        $vr->[ 1 ] * $dist,
        $vr->[ 2 ] * $dist,
        $vr->[ 3 ] * $dist,
    ];
}

##########################################
# Return a normal to the given 2 vectors

sub normal {

    my $face = shift;

    my $a1 = [
        "a1",
        $face->[ 1 ]->[ 1 ] - $face->[ 2 ]->[ 1 ],
        $face->[ 1 ]->[ 2 ] - $face->[ 2 ]->[ 2 ],
        $face->[ 1 ]->[ 3 ] - $face->[ 2 ]->[ 3 ],
    ];

    my $a2 = [
        "a2",
        $face->[ 3 ]->[ 1 ] - $face->[ 2 ]->[ 1 ],
        $face->[ 3 ]->[ 2 ] - $face->[ 2 ]->[ 2 ],
        $face->[ 3 ]->[ 3 ] - $face->[ 2 ]->[ 3 ],
    ];

    return [
        "nrm",
        ( $a1->[ 2 ] * $a2->[ 3 ] ) - ( $a1->[ 3 ] * $a2->[ 2 ] ),
        ( $a1->[ 3 ] * $a2->[ 1 ] ) - ( $a1->[ 1 ] * $a2->[ 3 ] ),
        ( $a1->[ 1 ] * $a2->[ 2 ] ) - ( $a1->[ 2 ] * $a2->[ 1 ] ),
    ];
}

##########################################
# Return the centre of the given face

sub centre {

    my $face = shift;

    my $cen = [ "c", 0, 0, 0 ];

    for ( my $v = 1; $v < scalar @{ $face } - 1; $v++ ) {

        $cen->[ 1 ] += $face->[ $v ]->[ 1 ];
        $cen->[ 2 ] += $face->[ $v ]->[ 2 ];
        $cen->[ 3 ] += $face->[ $v ]->[ 3 ];
    }

    $cen->[ 1 ] /= ( scalar @{ $face } - 2 );
    $cen->[ 2 ] /= ( scalar @{ $face } - 2 );
    $cen->[ 3 ] /= ( scalar @{ $face } - 2 );

    return $cen;
}

##########################################
# Reorient polyhedral faces 

sub fixFaceOrientations {

    my $polyhedra = shift;
    my $centre = shift || [ 'C', 0, 0, 0 ];

    for( my $p = 0; $p < scalar @{ $polyhedra }; $p++ ) {
        
        my $polyhedron = $polyhedra->[ $p ];

        for ( my $f = 1; $f < scalar @{ $polyhedron }; $f++ ) {

            my $face = $polyhedron->[ $f ];

            my $face_centre = centre( $face );
            $face_centre->[ 1 ] -= $centre->[ 1 ];
            $face_centre->[ 2 ] -= $centre->[ 2 ];
            $face_centre->[ 3 ] -= $centre->[ 3 ];

            my $nrm = normal( $face );
            my $dot = dot( $face_centre, $nrm );

            if ( $dot > 0 ) {
                my $label = $face->[ 0 ];
                my $color = $face->[ -1 ];
                $face = [ reverse( @{ $face } ) ];
                $face->[ 0 ] = $label;
                $face->[ -1 ] = $color;
                $polyhedron->[ $f ] = $face;
            }
        }
    }
}
 
##########################################
# Find a valid connection given the dists

sub tryVertexConnections {

    my $vlist = shift;
    my $vertex = shift;
    my $candidates = shift;
    my $side_count = shift;
    my $dists = shift;
    my $keys = shift;

    my $connections = $vlist->[ $vertex ]->[ 4 ];

    for( my $v = 0; $v < scalar @{ $connections }; $v++ ) {

        my $try = $connections->[ $v ];

        # check this vertex is not already in the list of candidates
        my $ok = 1;
        map {
            $ok = 0 if ( $ok && ( $_ == $try ) );
        } @{ $candidates };

        next unless $ok;

        # Check the distances to existing candidates
        $ok = 1;
        for( my $d = 0; $d < scalar @{ $dists }; $d++ ) {
        
            next unless $d < scalar @{ $candidates };
            my $dist = dist( $vlist->[ $try ], $vlist->[ $candidates->[ -1 * ( 1 + $d ) ] ] );
            if ( mod( $dist - $dists->[ $d ] ) > SMALL ) {
                $ok = 0;
                last;
            }
        }

        next unless $ok;
 
        # Does this vertex complete the candidate list...?
        if ( ( scalar @{ $candidates } + 1 ) == $side_count ) {

            $ok = 1;
            for( my $d = 0; $d < scalar @{ $dists }; $d++ ) {
        
                my $dist = dist( $vlist->[ $candidates->[ $d ] ], $vlist->[ $try ] );
                if ( mod( $dist - $dists->[ $d ] ) > SMALL ) {
                    $ok = 0;
                    last;
                }
            }

            if ( $ok ) {
                my $key = join '-', sort( @{ $candidates }, $try );
                $keys->{ $key } = [ @{ $candidates}, $try ] unless $keys->{ $key };
            }
        }
        else {

            tryVertexConnections(
                            $vlist,
                            $try,
                            [ @{ $candidates}, $try ],
                            $side_count,
                            $dists,
                            $keys,
            );
        }
    }

    return 0;
}

##########################################
# Find an n-gon in the vertex list

sub findNgons {

    my $vlist = shift;
    my $side_count = shift;
    my $dists = shift;  # array of vertex separations
    my $color = shift || 'grey8';

    my $keys = {};
    
    for ( my $v1 = 0; $v1 < scalar @{ $vlist } - ( $side_count - 1 ); $v1++ ) {

        tryVertexConnections( $vlist, $v1, [ $v1 ], $side_count, $dists, $keys );
    }

    my $list = [];

    map {
        my @vertices = map {
            $vlist->[ $_ ]
        } @{ $_ };

        push @{ $list }, [
            sprintf( "f%d", scalar @{ $list } ),
            @vertices,
            $color
        ];
    } ( values %{ $keys } );

    return $list;
}
 
##########################################
# Find triangles in the vertex list

sub findTriangles {

    my $vlist = shift;
    my $side = shift;
    my $color = shift || 'grey8';

    return findNgons( $vlist, 3, [ $side ], $color );
}
    
##########################################
# Find squares in the vertex list

sub findSquares {

    my $vlist = shift;
    my $side = shift;
    my $color = shift || 'grey8';

    return findNgons( $vlist, 4, [ $side, $side * sqrt( 2.0 ) ], $color );
}

##########################################
# Find pentagons in the vertex list

sub findPentagons {

    my $vlist = shift;
    my $side = shift;
    my $color = shift || 'grey8';

    return findNgons( $vlist, 5, [ $side, $side * PHI ], $color );
}

##########################################
# Find hexagons in the vertex list

sub findHexagons {

    my $vlist = shift;
    my $side = shift;
    my $color = shift || 'grey8';

    return findNgons( $vlist, 6, [ $side, $side * sqrt( 3.0 ), 2.0 * $side ], $color );
}

##########################################
# Return a list of vertex permutations

sub vertexPerms {

    my $base = shift;
    my $maps = shift;
    my $offset = shift || 0;

    my $list = [];

    for my $map ( @{ $maps } ) {

        my $base_vr = [];
    
        for ( my $p = 0; $p < 3; $p++ ) {
    
            my $ch = substr $map, $p, 1;
    
            $base_vr->[ $p ] =
                  ( $ch eq 'a' ) || (  $ch eq 'A' ) ? $base->[ 0 ] 
                : ( $ch eq 'b' ) || (  $ch eq 'B' ) ? $base->[ 1 ] 
                : ( $ch eq 'c' ) || (  $ch eq 'C' ) ? $base->[ 2 ] 
                :  die "Bad map: '$map'\n"
            ;
        }
    
        for ( my $n = 0; $n < 8; $n++ ) {
    
            my $vr = [];
    
            $vr->[ 1 ] = $base_vr->[ 0 ];
            $vr->[ 2 ] = $base_vr->[ 1 ];
            $vr->[ 3 ] = $base_vr->[ 2 ];
    
            if ( ( $n / 4 ) % 2 ) {
                ( substr( $map, 0, 1 ) =~ m/[ABC]/ ) ?  $vr->[ 1 ] *= -1 : next;
            }
    
            if ( ( $n / 2 ) % 2 ) {
                ( substr( $map, 1, 1 ) =~ m/[ABC]/ ) ?  $vr->[ 2 ] *= -1 : next;
            }
    
            if ( $n % 2 ) {
                ( substr( $map, 2, 1 ) =~ m/[ABC]/ ) ?  $vr->[ 3 ] *= -1 : next;
            }
            
            $vr->[ 0 ] = sprintf "v%d", $offset + scalar @{ $list };

            push @{ $list }, $vr;
        }
    }

    return $list;
}

##########################################
# Break the squares into 2 triangles

sub squares2triangles {

    my $squares = shift;
    my $offset = shift || 0;

    $offset++;

    my $list = [];

    map {
  
        my $color = $_->[ 5 ];

        push @{ $list },
            [ sprintf( "f%d", $offset++), $_->[ 1 ], $_->[ 2 ], $_->[ 3 ], $color ],
            [ sprintf( "f%d", $offset++), $_->[ 1 ], $_->[ 3 ], $_->[ 4 ], $color ],
        ;
    } @{ $squares };

    return $list;
}

##########################################
# Break the pentagons into 3 triangles

sub pentagons2triangles {

    my $pentagons = shift;
    my $offset = shift || 0;

    $offset++;

    my $list = [];

    map {
  
        my $color = $_->[ 6 ];

        push @{ $list },
            [ sprintf( "f%d", $offset++), $_->[ 1 ], $_->[ 2 ], $_->[ 3 ], $color ],
            [ sprintf( "f%d", $offset++), $_->[ 1 ], $_->[ 3 ], $_->[ 4 ], $color ],
            [ sprintf( "f%d", $offset++), $_->[ 1 ], $_->[ 4 ], $_->[ 5 ], $color ],
        ;
    } @{ $pentagons };

    return $list;
}

##########################################
# Break the hexagons into 3 triangles

sub hexagons2triangles {

    my $hexagons = shift;
    my $offset = shift || 0;

    $offset++;

    my $list = [];

    map {
  
        my $color = $_->[ 7 ];

        push @{ $list },
            [ sprintf( "f%d", $offset++), $_->[ 1 ], $_->[ 2 ], $_->[ 3 ], $color ],
            [ sprintf( "f%d", $offset++), $_->[ 1 ], $_->[ 3 ], $_->[ 4 ], $color ],
            [ sprintf( "f%d", $offset++), $_->[ 1 ], $_->[ 4 ], $_->[ 5 ], $color ],
            [ sprintf( "f%d", $offset++), $_->[ 1 ], $_->[ 5 ], $_->[ 6 ], $color ],
        ;
    } @{ $hexagons };

    return $list;
}

##########################################
# Return the distance between 2 points

sub dist {

    my $_v1 = shift;
    my $_v2 = shift;

    return sqrt (
        ( $_v1->[1] - $_v2->[1] ) * ( $_v1->[1] - $_v2->[1] ) +
        ( $_v1->[2] - $_v2->[2] ) * ( $_v1->[2] - $_v2->[2] ) +
        ( $_v1->[3] - $_v2->[3] ) * ( $_v1->[3] - $_v2->[3] )
    );
}

##########################################
# Return the absolute value

sub mod {

    my $_v = shift;

    return ( -1.0 * $_v ) if ( $_v < 0 );

    return $_v;
}

##########################################
# Setup vertex connections

sub setupVertexConnections {

    my $vlist = shift;
    my $side = shift;

    map {
        $_->[ 4 ] ||= [];
    } @{ $vlist };

    for ( my $v1 = 0; $v1 < scalar @{ $vlist } - 1; $v1++ ) {

        for ( my $v2 = $v1 + 1; $v2 < scalar @{ $vlist }; $v2++ ) {

            next if ( mod( dist( $vlist->[ $v1 ], $vlist->[ $v2 ] ) - $side ) > SMALL );

            push @{ $vlist->[ $v1 ]->[ 4 ] }, $v2;
            push @{ $vlist->[ $v2 ]->[ 4 ] }, $v1;
        }
    }
}

##########################################
# Describe a polyhedron

sub dumpVertices {

    my $vlist = shift;

    map {

        printf "%-4s %0.4f %0.4f %0.4f [",
            $_->[ 0 ], $_->[ 1 ], $_->[ 2 ], $_->[ 3 ];

        my $sep = '';
        map {
            printf "%s%s", $sep, $_;
            $sep = ', ';
        } @{ $_->[ 4 ] };

        printf "]\n";

    } @{ $vlist };
}

##########################################
# Describe a polyhedron

sub describePolyhedron {

    my $polyhedron = shift;

    printf "Polyhedron: %s\n", $polyhedron->[ 0 ];
    printf "     Faces: %d\n", scalar @{ $polyhedron } - 1;

    for ( my $f = 1; $f < scalar @{ $polyhedron }; $f++ ) {

        my $face = $polyhedron->[ $f ];

        printf "%s: ", $face->[ 0 ];
    
        for ( my $v = 1; $v < scalar @{ $face } - 1 ; $v++ ) {

            printf "%s %s", ( $v > 1 ) ? ',' : '', $face->[ $v ]->[ 0 ];
        }
        printf "\n";
    }
}

##########################################
# Do the polyhedra have a common edge

sub hasCommonEdge {

    my $polyhedron1 = shift;
    my $polyhedron2 = shift;

    # For the moment just assume that any 2 common vertices
    # will constitute a common edge

    my $shared = 0;

    for ( my $v1 = 1; $v1 < scalar @{ $polyhedron1 } - 1; $v1++ ) {
    
        for ( my $v2 = 1; $v2 < scalar @{ $polyhedron2 } - 1; $v2++ ) {

            if ( $polyhedron1->[ $v1 ]->[ 0 ] eq $polyhedron2->[ $v2 ]->[ 0 ] ) {
                $shared++;
                return 1 if ( $shared == 2 );
                last;
            }
        }
    }

    return 0;
}

##########################################
# Show label

sub labelImage {

    my $label = shift;
    my $outfile = shift;

    open( my $fh, '>>', $outfile ) or return;

    printf $fh "
        gsave
        180 13 translate
    
        /Tahoma-Bold findfont 12 scalefont setfont
    
        /label ($label) def
        /label_wid label stringwidth pop def
    
        label_wid -2 div 5 sub -6 moveto
        0 20 rlineto
        label_wid 10 add 0 rlineto
        0 -20 rlineto
        closepath
    
        gsave
        0 setgray fill
        grestore
        1 setgray stroke
    
        1 1 0 setrgbcolor
        label_wid -2 div 0 moveto
        label show
    
        grestore
    ";

    close $fh;
}

##########################################
# Return the value phi
sub phi {
    
    return PHI;
}

##########################################
# Print out the help text

sub usage {

    printf "
        $0 -o <out file> [opts...]

            where madatory parameters are:
                -o              the mandatory output file

            and options:
                -?              display config details only

                -no-anti-alias  turn off anti-aliasing (on by default)
                -mono           ignore face colorings and render in grey

                -e              distance of the eye from the screen (default 500)
                -f              floor z value
                -h              show this help text
                -i              size of the image in inches (default 5)
                -l              label for the image
                -r              set the display resolution (default 50)
                -s              size of the floor (default 10)
                -ox             image offset along the x axis (default 0)
                -oy             image offset along the y axis (default 0)
                -oz             image offset along the z axis (default 0)
                -rx             rotation about the x axis (default 0)
                -ry             rotation about the y axis (default 0)
                -rz             rotation about the z axis (default 0)
                -sx             screen offset along the x axis (default 40)
                -sy             screen offset along the y axis (default 40)
                -sz             screen offset along the z axis (default 45)

            star prism options:
                -sp             number of start points (default 7)
                -sr             radius of the star (default 4)
                -se             skip count for the star (default 3)
                -sc             draw the crossed version of the prism
                -ph             prism height (default 4)
    \n";

    exit 0;
}
