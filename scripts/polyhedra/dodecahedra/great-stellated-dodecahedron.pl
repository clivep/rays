#!/usr/bin/perl

use warnings;
use strict;

use Math::Trig;

################################################################################
#
#   great-stellated-dodecahedron.pl
#
#       parameters detailed in usage()
#
################################################################################

do '../utils/utilities.pl';

##########################################
# Setup the working environment

my $context = setupContext();
$context->{ 'OFFSETS' } = [ 0, 0, -2.2 ];

processParameters( $context );

##########################################
# Setup the figure

my $side = 2.7;

my $vlist = dodecahedronVertices( $side );

setupVertexConnections( $vlist, $side * phi() );

my $pentagons = findPentagons( $vlist, $side * phi(), "red" );

my $t = 0;
my $triangles = [];
map {

    my $pentagon = $_;

    for( my $v = 0; $v < 5; $v++ ) {

        my $vtx1 = $pentagon->[ $v + 1 ];

        my $vtx2 = mulVector( $vtx1, $pentagon->[ 1 + ( $v + 2 ) % 5 ], $side * phi() );
        my $vtx3 = mulVector( $vtx1, $pentagon->[ 1 + ( $v + 3 ) % 5 ], $side * phi() );

        push @{ $triangles }, [
            sprintf( "f%d", $t++ ),
            $vtx1,
            addVector( $vtx1, $vtx2 ),
            addVector( $vtx1, $vtx3 ),
            "green",
        ];
    }

} @{ $pentagons };

my $polyhedra = [
        [ "p1",
            @{ $triangles },
        ]
];

fixFaceOrientations( $polyhedra );

##########################################
# Write out the config file

writeConfigHeader( $context );

$context->{ 'FLOOR_Z' } = -1 * $side * sqrt( 2.0 );
writeConfigFloor( $context );

writeTriangleFaces( $context, $polyhedra );

##########################################
# Display the config if requested

showConfig( $context );

##########################################
# Run the ray-tracer to produce the image

processImage( $context );
