#!/usr/bin/perl

use warnings;
use strict;

use Math::Trig;

################################################################################
#
#   dodecahedron.pl
#
#       parameters detailed in usage()
#
################################################################################

do '../utils/utilities.pl';

##########################################
# Setup the working environment

my $context = setupContext();
$context->{ 'OFFSETS' } = [ 0, 0, -2 ];

processParameters( $context );

##########################################
# Setup the figure

my $side = 2.5;
my $vlist = dodecahedronVertices( $side );

setupVertexConnections( $vlist, $side );

my $pentagons = findPentagons( $vlist, $side, "red" );

my $polyhedra = [
        [ "p1",
            @{ pentagons2triangles( $pentagons ) },
        ]
];

fixFaceOrientations( $polyhedra );

##########################################
# Write out the config file

writeConfigHeader( $context );

$context->{ 'FLOOR_Z' } = -1.0 * $side * sqrt( 2.0 );
writeConfigFloor( $context );

writeTriangleFaces( $context, $polyhedra );

##########################################
# Display the config if requested

showConfig( $context );

##########################################
# Run the ray-tracer to produce the image

processImage( $context );


