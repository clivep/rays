#!/usr/bin/perl

use warnings;
use strict;

use Math::Trig;

################################################################################
#
#   compound4-a.pl
#
#       parameters detailed in usage()
#
################################################################################

do '../utils/utilities.pl';

##########################################
# Setup the working environment

my $context = setupContext();
$context->{ 'OFFSETS' } = [ 0, 0, -2 ];

processParameters( $context );

##########################################
# Setup the figure

my $side = 2.5;

my $vlist = dodecahedronVertices( $side );

setupVertexConnections( $vlist, $side );

my $pentagons = findPentagons( $vlist, $side );

my $polyhedron1 = [ "p1" ];

my $f = 0;
map {
    
    push @{ $polyhedron1 },
                [ sprintf( "f%d", $f++), $_->[ 1 ], $_->[ 2 ], $_->[ 3 ], "red" ],
                [ sprintf( "f%d", $f++), $_->[ 1 ], $_->[ 3 ], $_->[ 4 ], "red" ],
                [ sprintf( "f%d", $f++), $_->[ 1 ], $_->[ 4 ], $_->[ 5 ], "red" ],
    ;

} @{ $pentagons };

my $polyhedron2 = [ "p2" ];

$f = 0;
map {
    
    my $ang = pi / 2.0;

    push @{ $polyhedron2 },
                zRotateFace( [ sprintf( "f%d", $f++), $_->[ 1 ], $_->[ 2 ], $_->[ 3 ], "green" ], $ang ),
                zRotateFace( [ sprintf( "f%d", $f++), $_->[ 1 ], $_->[ 3 ], $_->[ 4 ], "green" ], $ang ),
                zRotateFace( [ sprintf( "f%d", $f++), $_->[ 1 ], $_->[ 4 ], $_->[ 5 ], "green" ], $ang ),
    ;

} @{ $pentagons };

my $polyhedra = [ $polyhedron1, $polyhedron2 ];

fixFaceOrientations( $polyhedra );

##########################################
# Write out the config file

writeConfigHeader( $context );

$context->{ 'FLOOR_Z' } = -1.0 * $side * sqrt( 2.0 );
writeConfigFloor( $context );

writeTriangleFaces( $context, $polyhedra );

##########################################
# Display the config if requested

showConfig( $context );

##########################################
# Run the ray-tracer to produce the image

processImage( $context );


