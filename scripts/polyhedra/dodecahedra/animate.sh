#!/bin/sh

######################################################################
#
#   animate.sh <script>
#
#   Repeatedly call the supplied <script> to render the image at
#   different angles. Produce an animated gif from this series
#   of images.
#
######################################################################

##############################
# Setup some defaults

usage="Usage: $0 [-res <resolution>] [-mono] <script-name>"

resolution=200

##############################
# Parse the command-line opts

while [ "$1" ]; do

    if [ "$1" = '-h' ]; then
        echo $usage
        exit 0
    fi

    if [ "$1" = '-res' -a -n "$2" ]; then
        resolution=$2
        shift
    fi

    if [ "$1" = '-mono' ]; then
        monochrome='-mono'
    fi

    script=$1
    shift
done

if [ -z "$script" ]; then
    echo $usage
    exit -1
fi

if [ ! -f "$script" ]; then
    echo "Could not find $script"
    exit -1
fi

image=`echo $script | sed -e 's/.*\///' -e 's/\.[^\.]*$/.gif/'`

dest=animations/$image
mkdir animations > /dev/null 2>&1

work_dir='tmp-images'

steps=2

mkdir $work_dir > /dev/null 2>&1

rot() {
    ang="000$1"
    ang=`echo $ang | sed -e's/.*\(...\)$/\1/'`

    echo "`date` ang: $ang"

    $script -o $work_dir/tmp_$ang.jpg -r $resolution -rx $ang -ry $ang -rz $ang $monochrome
}

ang=0
while [ $ang -lt 360 ];
do
    rot $ang
    ang=`expr $ang \+ $steps`
done

convert -delay 20 -loop 0 $work_dir/tmp*.jpg $dest
rm tmp-images/tmp*.jpg

echo Done
