#!/usr/bin/perl

use warnings;
use strict;

use Math::Trig;

################################################################################
#
#   small-stellated-dodecahedron.pl
#
#       parameters detailed in usage()
#
################################################################################

do '../utils/utilities.pl';

##########################################
# Setup the working environment

my $context = setupContext();
$context->{ 'OFFSETS' } = [ 0, 0, -2 ];

processParameters( $context );

##########################################
# Setup the figure

my $side = 4;

my $vlist = icosahedronVertices( $side );

setupVertexConnections( $vlist, $side );

my $pentagons = findPentagons( $vlist, $side, "red" );

my $t = 0;
my $triangles = [];
map {

    my $pentagon = $_;

    for( my $v = 0; $v < 5; $v++ ) {

        my $vtx1 = $pentagon->[ $v + 1 ];

        my $vtx2 = mulVector( $vtx1, $pentagon->[ 1 + ( $v + 2 ) % 5 ], $side / phi() );
        my $vtx3 = mulVector( $vtx1, $pentagon->[ 1 + ( $v + 3 ) % 5 ], $side / phi() );

        push @{ $triangles }, [
            sprintf( "f%d", $t++ ),
            $vtx1,
            addVector( $vtx1, $vtx2 ),
            addVector( $vtx1, $vtx3 ),
            "green",
        ];
    }

} @{ $pentagons };

my $polyhedra = [
        [ "p1",
            @{ $triangles },
        ]
];

fixFaceOrientations( $polyhedra );

##########################################
# Write out the config file

writeConfigHeader( $context );

$context->{ 'FLOOR_Z' } = -0.5 * phi() * $side;
writeConfigFloor( $context );

writeTriangleFaces( $context, $polyhedra );

##########################################
# Display the config if requested

showConfig( $context );

##########################################
# Run the ray-tracer to produce the image

processImage( $context );
