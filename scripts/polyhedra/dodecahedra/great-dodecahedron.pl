#!/usr/bin/perl

use warnings;
use strict;

use Math::Trig;

################################################################################
#
#   great-dodecahedron.pl
#
#       parameters detailed in usage()
#
################################################################################

do '../utils/utilities.pl';

##########################################
# Setup the working environment

my $context = setupContext();
$context->{ 'OFFSETS' } = [ 0, 0, -2 ];

processParameters( $context );

##########################################
# Define the vertices

my $size = 1.8;

my $phi = $size * phi();
my $ph2 = $size / phi();

my $v0  = [ "v1",     0.0,  $size,   $phi ];
my $v1  = [ "v2",     0.0,  $size,  -$phi ];
my $v2  = [ "v3",     0.0, -$size,   $phi ];
my $v3  = [ "v4",     0.0, -$size,  -$phi ];

my $v4  = [ "v5",   $size,   $phi,    0.0 ];
my $v5  = [ "v6",   $size,  -$phi,    0.0 ];
my $v6  = [ "v7",  -$size,   $phi,    0.0 ];
my $v7  = [ "v8",  -$size,  -$phi,    0.0 ];

my $v8  = [ "v9",    $phi,    0.0,  $size ];
my $v9  = [ "v10",  -$phi,    0.0,  $size ];
my $v10 = [ "v11",   $phi,    0.0, -$size ];
my $v11 = [ "v11",  -$phi,    0.0, -$size ];

##########################################
# Define the faces

my @pentagons;

push @pentagons, [ "f1",  $v0,  $v8,  $v5,  $v7,  $v9, "red" ];
push @pentagons, [ "f1",  $v3, $v10,  $v4,  $v6, $v11, "red" ];

push @pentagons, [ "f1",  $v1,  $v3,  $v7,  $v9,  $v6, "green" ];
push @pentagons, [ "f1",  $v0,  $v2,  $v5, $v10,  $v4, "green" ];

push @pentagons, [ "f1",  $v0,  $v2,  $v7, $v11,  $v6, "blue" ];
push @pentagons, [ "f1",  $v1,  $v3,  $v5,  $v8,  $v4, "blue" ];

push @pentagons, [ "f1",  $v1, $v10,  $v5,  $v7, $v11, "yellow" ];
push @pentagons, [ "f1",  $v2,  $v8,  $v4,  $v6,  $v9, "yellow" ];

push @pentagons, [ "f1",  $v2,  $v5,  $v3, $v11,  $v9, "magenta" ];
push @pentagons, [ "f1",  $v0,  $v6,  $v1, $v10,  $v8, "magenta" ];

push @pentagons, [ "f1",  $v2,  $v7,  $v3, $v10,  $v8, "cyan" ];
push @pentagons, [ "f1",  $v0,  $v4,  $v1, $v11,  $v9, "cyan" ];

my $polyhedron = [ "p1" ];

my $f = 0;
map {
    
    push @{ $polyhedron },
                [ sprintf( "f%d", $f++), $_->[ 1 ], $_->[ 2 ], $_->[ 3 ], $_->[ 6 ] ],
                [ sprintf( "f%d", $f++), $_->[ 1 ], $_->[ 3 ], $_->[ 4 ], $_->[ 6 ] ],
                [ sprintf( "f%d", $f++), $_->[ 1 ], $_->[ 4 ], $_->[ 5 ], $_->[ 6 ] ],
    ;

} @pentagons;

my $polyhedra = [ $polyhedron ];

fixFaceOrientations( $polyhedra );

##########################################
# Write out the config file

writeConfigHeader( $context );

$context->{ 'FLOOR_Z' } = -1.0 * $size * sqrt( 2.0 );
writeConfigFloor( $context );

writeTriangleFaces( $context, $polyhedra );

##########################################
# Display the config if requested

showConfig( $context );

##########################################
# Run the ray-tracer to produce the image

processImage( $context );
