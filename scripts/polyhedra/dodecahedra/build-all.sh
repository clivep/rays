#!/bin/sh -x

mkdir images > /dev/null 2>&1

compound-dodecahedra-a.pl -r 200 -o images/compound-dodecahedra-a.pdf -l 'Compound of 2 Dodecahedra'
compound-dodecahedra-b.pl -r 200 -o images/compound-dodecahedra-b.pdf -l 'Compound of 2 Dodecahedra'

dodecahedron.pl -r 200 -o images/dodecahedron.pdf -l 'Dodecahedron'

great-dodecahedron.pl -r 200 -o images/great-dodecahedron.pdf -l 'Great Dodecahedron'

small-stellated-dodecahedron.pl -r 200 -rz 60 -o images/small-stellated-dodecahedron.pdf -l 'Small Stellated Dodecahedron'
great-stellated-dodecahedron.pl -r 200 -rz 30 -o images/great-stellated-dodecahedron.pdf -l 'Great Stellated Dodecahedron'
