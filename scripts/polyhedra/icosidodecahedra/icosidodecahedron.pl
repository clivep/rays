#!/usr/bin/perl

use warnings;
use strict;

use Math::Trig;

################################################################################
#
#   icosidodecahedron.pl
#
#       parameters detailed in usage()
#
################################################################################

do '../utils/utilities.pl';

##########################################
# Setup the working environment

my $context = setupContext();
$context->{ 'OFFSETS' } = [ 0, 0, -2 ];

processParameters( $context );

##########################################
# Define the vertices

my $size = 2;

# Vertices are the even permutations of the base vectors
my $vlist = [];

push @{ $vlist }, @{ vertexPerms( 
    [ 0.0,  0.0, $size * phi() ],
    [ 'abC', 'Cab', 'bCa' ],
) };

push @{ $vlist }, @{ vertexPerms( 
    [ $size * 0.5, $size * phi() / 2.0, $size * ( 1.0 + phi() ) / 2.0 ],
    [ 'ABC', 'CAB', 'BCA' ],
    scalar @{ $vlist }
) };

setupVertexConnections( $vlist, $size );

##########################################
# Define the faces

my $pentagons = findPentagons( $vlist, $size, "red" );
my $triangles = findTriangles( $vlist, $size, "yellow" );

my $polyhedra = [
    [ "p1", @{ pentagons2triangles( $pentagons ) }, @{ $triangles } ]
];

fixFaceOrientations( $polyhedra );

##########################################
# Write out the config file

writeConfigHeader( $context );

$context->{ 'FLOOR_Z' } = -$size * phi();
writeConfigFloor( $context );

writeTriangleFaces( $context, $polyhedra );

##########################################
# Display the config if requested

showConfig( $context );

##########################################
# Run the ray-tracer to produce the image

processImage( $context );
