#!/usr/bin/perl

use warnings;
use strict;

use Math::Trig;

################################################################################
#
#   truncated-icosahedron.pl
#
#       parameters detailed in usage()
#
################################################################################

do '../utils/utilities.pl';

##########################################
# Setup the working environment

my $context = setupContext();
$context->{ 'OFFSETS' } = [ 0, 0, -2 ];

processParameters( $context );

##########################################
# Setup the figure

my $side = 1.3;

my $vlist = [];

my $size = $side / 2.0;

push @{ $vlist }, @{ vertexPerms( 
    [ 0.0, $size, $size * 3.0 * phi() ],
    [ 'aBC', 'CaB', 'BCa' ],
) };

push @{ $vlist }, @{ vertexPerms( 
    [ $size * 2.0, $size * ( 1.0 + 2.0 * phi() ), $size * phi() ],
    [ 'ABC', 'CAB', 'BCA' ],
    scalar @{ $vlist }
) };

push @{ $vlist }, @{ vertexPerms( 
    [ $size, $size * ( 2.0 + phi() ), $size * 2.0 * phi() ],
    [ 'ABC', 'CAB', 'BCA' ],
    scalar @{ $vlist }
) };

setupVertexConnections( $vlist, $side );

##########################################
# Define the faces

my $pentagons = findPentagons( $vlist, $side, "yellow" );
my $hexagons = findHexagons( $vlist, $side, "red" );

my $polyhedra = [
    [ "p1",
        @{ pentagons2triangles( $pentagons ) },
        @{ hexagons2triangles( $hexagons ) },
    ]
];

fixFaceOrientations( $polyhedra );

##########################################
# Write out the config file

writeConfigHeader( $context );

$context->{ 'FLOOR_Z' } = -1.5 * phi() * $side;
writeConfigFloor( $context );

writeTriangleFaces( $context, $polyhedra );

##########################################
# Display the config if requested

showConfig( $context );

##########################################
# Run the ray-tracer to produce the image

processImage( $context );

