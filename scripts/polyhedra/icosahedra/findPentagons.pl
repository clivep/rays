#!/usr/bin/perl

use warnings;
use strict;

use Math::Trig;

################################################################################
#
#   icosahedron.pl
#
#       parameters detailed in usage()
#
################################################################################

do '../utils/utilities.pl';

##########################################
# Setup the working environment

##########################################
# Define the vertices

my $size = 2;

my $phi = $size * ( 1.0 + sqrt( 5.0 ) ) / 2.0;

my $vlist = [
    [ "v0",    0.0,  $size,   $phi ],
    [ "v1",    0.0,  $size,  -$phi ],
    [ "v2",    0.0, -$size,   $phi ],
    [ "v3",    0.0, -$size,  -$phi ],

    [ "v4",  $size,   $phi,    0.0 ],
    [ "v5",  $size,  -$phi,    0.0 ],
    [ "v6", -$size,   $phi,    0.0 ],
    [ "v7", -$size,  -$phi,    0.0 ],

    [ "v8",   $phi,    0.0,  $size ],
    [ "v9",  -$phi,    0.0,  $size ],
    [ "v10",  $phi,    0.0, -$size ],
    [ "v11", -$phi,    0.0, -$size ],
];

my $small = 0.0000001;

my $side = 2.0 * $size;
my $long = 2.0 * $phi;

my %keys;

for ( my $v1 = 0; $v1 < scalar @{ $vlist }; $v1++ ) {

    for ( my $v2 = 0; $v2 < scalar @{ $vlist }; $v2++ ) {

        next if ( $v2 == $v1 );

        my $d1 =  dist( $vlist->[ $v1 ], $vlist->[ $v2 ] );
        next if ( mod( $d1 - $side ) > $small );

        for ( my $v3 = 0; $v3 < scalar @{ $vlist }; $v3++ ) {

            next if ( $v3 == $v1 );
            next if ( $v3 == $v2 );

            my $d2 =  dist( $vlist->[ $v3 ], $vlist->[ $v2 ] );
            next if ( mod( $d2 - $side ) > $small );

            my $d3 =  dist( $vlist->[ $v3 ], $vlist->[ $v1 ] );
            next if ( mod( $d3 - $long ) > $small );

            for ( my $v4 = 0; $v4 < scalar @{ $vlist }; $v4++ ) {
    
                next if ( $v4 == $v1 );
                next if ( $v4 == $v2 );
                next if ( $v4 == $v3 );
    
                my $d4 =  dist( $vlist->[ $v4 ], $vlist->[ $v3 ] );
                next if ( mod( $d4 - $side ) > $small );
    
                my $d5 =  dist( $vlist->[ $v4 ], $vlist->[ $v2 ] );
                next if ( mod( $d5 - $long ) > $small );
    
                my $d6 =  dist( $vlist->[ $v4 ], $vlist->[ $v1 ] );
                next if ( mod( $d6 - $long ) > $small );
    
                for ( my $v5 = 0; $v5 < scalar @{ $vlist }; $v5++ ) {
        
                    next if ( $v5 == $v1 );
                    next if ( $v5 == $v2 );
                    next if ( $v5 == $v3 );
                    next if ( $v5 == $v4 );
        
                    my $d7 =  dist( $vlist->[ $v5 ], $vlist->[ $v4 ] );
                    next if ( mod( $d7 - $side ) > $small );
        
                    my $d8 =  dist( $vlist->[ $v5 ], $vlist->[ $v3 ] );
                    next if ( mod( $d8 - $long ) > $small );
        
                    my $d9 =  dist( $vlist->[ $v5 ], $vlist->[ $v2 ] );
                    next if ( mod( $d9 - $long ) > $small );
        
                    my $d10 =  dist( $vlist->[ $v5 ], $vlist->[ $v1 ] );
                    next if ( mod( $d10 - $side ) > $small );

                    my @arr = sort ( $v1, $v2, $v3, $v4, $v5 );
                    my $key = join '-', @arr;

                    unless ( $keys{ $key } ) {

                        $keys{ $key } = 1;

                        my $face = [
                             "f",
                             $vlist->[ $v1 ], $vlist->[ $v2 ], $vlist->[ $v3 ], $vlist->[ $v4 ], $vlist->[ $v5 ]
                        ];

                        my $nrm = normal( $face );
                        my $cen = centre( $face );
                        my $dot = dot( $cen, $nrm );

                        my $note = " ";

                        if ( $dot < 0 ) {

                            $note = "r";
                            $face = [
                                "f",
                                $vlist->[ $v5 ], $vlist->[ $v4 ], $vlist->[ $v3 ], $vlist->[ $v2 ], $vlist->[ $v1 ]
                            ];
                            $nrm = normal( $face );
                        }

                        normalise( $nrm );
                        printf "$note %d-%d-%d-%d-%d (%0.4f,%0.4f,%0.4f)\n",
                                                        $v1, $v2, $v3, $v4, $v5,
                                                        $nrm->[ 1 ], $nrm->[ 2 ], $nrm->[ 3 ];
                    }
                }
            }
        }
    }
}

exit;

sub dist {

    my $_v1 = shift;
    my $_v2 = shift;

    return sqrt (
        ( $_v1->[1] - $_v2->[1] ) * ( $_v1->[1] - $_v2->[1] ) +
        ( $_v1->[2] - $_v2->[2] ) * ( $_v1->[2] - $_v2->[2] ) +
        ( $_v1->[3] - $_v2->[3] ) * ( $_v1->[3] - $_v2->[3] )
    );
}

sub mod {

    my $_v = shift;

    return ( -1.0 * $_v ) if ( $_v < 0 );

    return $_v;
}

