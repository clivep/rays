#!/usr/bin/perl

use warnings;
use strict;

use Math::Trig;

################################################################################
#
#   compound-icosahedra-5.pl
#
#       parameters detailed in usage()
#
################################################################################

do '../utils/utilities.pl';

##########################################
# Setup the working environment

my $context = setupContext();
$context->{ 'OFFSETS' } = [ 0, 0, -2 ];

processParameters( $context );

##########################################
# Define the vertices

my $side = 2;

# Vertices are the even permutations of the base vectors
my $vlist = [];

push @{ $vlist }, @{ vertexPerms( 
    [ 0.0,  0.0, $side * phi() ],
    [ 'abC', 'Cab', 'bCa' ],
) };

push @{ $vlist }, @{ vertexPerms( 
    [ $side * 0.5, $side * phi() / 2.0, $side * ( 1.0 + phi() ) / 2.0 ],
    [ 'ABC', 'CAB', 'BCA' ],
    scalar @{ $vlist }
) };

my $val = $side * ( ( sqrt( 3.0 ) / 2.0 ) + ( sqrt( 5.0 + 2.0 * sqrt( 5.0 ) ) / 2.0 ) );
$val = 5.236068;
setupVertexConnections( $vlist, $val );

dumpVertices( $vlist );
printf ">>$val\n";
exit;
##########################################
# Define the faces

my $pentagons = findPentagons( $vlist, $side, "red" );
my $triangles = findTriangles( $vlist, $side, "yellow" );

my $polyhedra = [
    [ "p1", @{ pentagons2triangles( $pentagons ) }, @{ $triangles } ]
];

fixFaceOrientations( $polyhedra );

##########################################
# Write out the config file

writeConfigHeader( $context );

$context->{ 'FLOOR_Z' } = -$side * phi();
writeConfigFloor( $context );

writeTriangleFaces( $context, $polyhedra );

##########################################
# Display the config if requested

showConfig( $context );

##########################################
# Run the ray-tracer to produce the image

processImage( $context );
