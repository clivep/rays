#!/usr/bin/perl

use warnings;
use strict;

use Math::Trig;

################################################################################
#
#   rhombicuboctahedron.pl
#
#       parameters detailed in usage()
#
################################################################################

do '../utils/utilities.pl';

##########################################
# Setup the working environment

my $context = setupContext();
$context->{ 'OFFSETS' } = [ 0, 0, -2 ];

processParameters( $context );

##########################################
# Setup the figure

my $side = 2.0; # 2.5;

my $vlist = [];

my $size = $side / 2.0;

push @{ $vlist }, @{ vertexPerms( 
    [ $size, $size, $size * ( 1.0 + sqrt( 2.0 ) ) ],
    [ 'ABC', 'BCA', 'CAB' ],
) };

setupVertexConnections( $vlist, $side );

my $triangles = findTriangles( $vlist, $side, "blue" );
my $squares = findSquares( $vlist, $side, "red" );

# Squares adjacent to triangles to be yellow
map {

    my $triangle = $_;
    map {
        $_->[ 5 ] = 'yellow' if hasCommonEdge( $_, $triangle );   
    } @{ $squares };

} @{ $triangles };

my $polyhedra = [
        [ "p1",
            @{ $triangles },
            @{ squares2triangles( $squares, scalar @{ $triangles } ) },
        ]
];

fixFaceOrientations( $polyhedra );

##########################################
# Write out the config file

writeConfigHeader( $context );

$context->{ 'FLOOR_Z' } = ( -1.0 - sqrt( 2.0 ) ) * $side / 2.0;
writeConfigFloor( $context );

writeTriangleFaces( $context, $polyhedra );

##########################################
# Display the config if requested

showConfig( $context );

##########################################
# Run the ray-tracer to produce the image

processImage( $context );

