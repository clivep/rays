#!/bin/sh -x

mkdir images > /dev/null 2>&1

rhombicuboctahedron.pl -r 200 -rz 25 -rx 35 -o images/rhombicuboctahedron.pdf -l 'Rhombicuboctahedron'
