#!/usr/bin/perl

use strict;
use warnings;

my $size = 300;
my $max_depth = 6;

my $colors = [
        [ 0.8, 0.8, 0.8 ],
        [ 0.8, 0.8, 0.8 ],
        [ 0.8, 0.8, 0.8 ],
        [ 0.8, 0.8, 0.8 ]
];

my $xoff = $size / -2.0;
my $yoff = $size / -3.0;

my $v1x = $xoff + 0;            my $v1y = $yoff + 0;
my $v2x = $xoff + $size;        my $v2y = $yoff + 0;
my $v3x = $xoff + $size / 2.0;  my $v3y = $yoff + $size * sqrt( 3.0 ) / 2.0;

printf "objects:\n";

my $skip = 1;

triangle(
        $v1x, $v1y, 0.0,
        $v2x, $v2y, 0.0,
        $v3x, $v3y, 0.0,
        3, $size, $max_depth
);

exit 0;

sub triangle {

    my (
        $v1x, $v1y, $v1z,
        $v2x, $v2y, $v2z,
        $v3x, $v3y, $v3z,
        $color, $size, $depth,
    ) = @_;

    my $object =  "
        object:
            type: triangle
            vertex1:
                i: $v1x
                j: $v1y
                k: $v1z
            vertex2:
                i: $v2x
                j: $v2y
                k: $v2z
            vertex3:
                i: $v3x
                j: $v3y
                k: $v3z
            surface: default
            scheme-name: tet
            color-name: tet
    "; 

    $object =~ s/\n    /\n/g;
    printf "$object\n" unless ( $skip );

    $skip = 0;

    $depth--;
    return if ( $depth < 1 );

    my $n1x = ( $v1x + $v2x ) / 2; my $n1y = ( $v1y + $v2y ) / 2; my $n1z = ( $v1z + $v2z ) / 2;
    my $n2x = ( $v2x + $v3x ) / 2; my $n2y = ( $v2y + $v3y ) / 2; my $n2z = ( $v2z + $v3z ) / 2;
    my $n3x = ( $v3x + $v1x ) / 2; my $n3y = ( $v3y + $v1y ) / 2; my $n3z = ( $v3z + $v1z ) / 2;

    my $cx = ( $v1x + $v2x + $v3x ) / 3;
    my $cy = ( $v1y + $v2y + $v3y ) / 3;
    my $cz = ( $v1z + $v2z + $v3z ) / 3;

    $size = $size / 2.0;
    my $height = 0.7 * $size * sqrt( 2.0 ) / sqrt( 3.0 );

    my $normal = cross(
                    [ $v1x - $v2x, $v1y - $v2y, $v1z - $v2z ],
                    [ $v1x - $v3x, $v1y - $v3y, $v1z - $v3z ]
    );
    normalise( $normal );

    my $n4x = $cx + $normal->[0] * $height;
    my $n4y = $cy + $normal->[1] * $height;
    my $n4z = $cz + $normal->[2] * $height;

    $color++;
    triangle( $n3x, $n3y, $n3z, $n2x, $n2y, $n2z, $n1x, $n1y, $n1z, $color++ % 4, $size, $depth );
    triangle( $n1x, $n1y, $n1z, $n2x, $n2y, $n2z, $n4x, $n4y, $n4z, $color++ % 4, $size, $depth );
    triangle( $n2x, $n2y, $n2z, $n3x, $n3y, $n3z, $n4x, $n4y, $n4z, $color++ % 4, $size, $depth );
    triangle( $n3x, $n3y, $n3z, $n1x, $n1y, $n1z, $n4x, $n4y, $n4z, $color++ % 4, $size, $depth );

    triangle( $v1x, $v1y, $v1z, $n1x, $n1y, $n1z, $n3x, $n3y, $n3z, $color, $size, $depth );
    triangle( $v2x, $v2y, $v2z, $n2x, $n2y, $n2z, $n1x, $n1y, $n1z, $color, $size, $depth );
    triangle( $v3x, $v3y, $v3z, $n3x, $n3y, $n3z, $n2x, $n2y, $n2z, $color, $size, $depth );
}

sub cross {

    my $vr1 = shift;
    my $vr2 = shift;

    return [
            ( $vr1->[1] * $vr2->[2] ) - ( $vr1->[2] * $vr2->[1] ),
            ( $vr1->[2] * $vr2->[0] ) - ( $vr1->[0] * $vr2->[2] ),
            ( $vr1->[0] * $vr2->[1] ) - ( $vr1->[1] * $vr2->[0] ),
    ];
}

sub normalise {

    my $vr = shift;

    my $d = sqrt( ( $vr->[0] * $vr->[0] ) + ( $vr->[1] * $vr->[1] ) + ( $vr->[2] * $vr->[2] ) );

    $vr->[0] = $vr->[0] / $d;
    $vr->[1] = $vr->[1] / $d;
    $vr->[2] = $vr->[2] / $d;
}
