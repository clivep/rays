#!/usr/bin/perl

use warnings;
use strict;

use constant    PI  =>  3.14159;

my $sqrt2 = sqrt( 2.0 );

my $s = 1000;

my $v1 = [ 0, 0, 0 ];
my $v2 = [ 0, $s, 0 ];
my $v3 = [ $s, $s, 0 ];
my $v4 = [ $s, 0, 0 ];

my $fr = 0.75;                      # side length reduction
my $ang = 75 * ( 2.0 * PI ) / 360;  # angle of new faces to base plane

my $ns = $s * $fr;      # new side length

my $a = [   # vr from v1 to v2
    $v2->[ 0 ] - $v1->[ 0 ],
    $v2->[ 1 ] - $v1->[ 1 ],
    $v2->[ 2 ] - $v1->[ 2 ],
];

my $b = [   # vr from v1 to v4
    $v2->[ 0 ] - $v4->[ 0 ],
    $v2->[ 1 ] - $v4->[ 1 ],
    $v2->[ 2 ] - $v4->[ 2 ],
];

my $c = [   # centre of base plane
     ( $v1->[ 0 ] + $v2->[ 0 ] + $v3->[ 0 ] + $v4->[ 0 ] ) / 4.0,
     ( $v1->[ 1 ] + $v2->[ 1 ] + $v3->[ 1 ] + $v4->[ 1 ] ) / 4.0,
     ( $v1->[ 2 ] + $v2->[ 2 ] + $v3->[ 2 ] + $v4->[ 2 ] ) / 4.0,
];

my $n = [ # normal to base plane
    ( $a->[ 1 ] * $b->[ 2 ] ) - ( $a->[ 2 ] * $b->[ 1 ] ),
    ( $a->[ 2 ] * $b->[ 0 ] ) - ( $a->[ 0 ] * $b->[ 2 ] ),
    ( $a->[ 0 ] * $b->[ 1 ] ) - ( $a->[ 1 ] * $b->[ 0 ] ),
];
normalise( $n );

my $cv1 = [   # vr from centre to v1
    $v1->[ 0 ] - $c->[ 0 ],
    $v1->[ 1 ] - $c->[ 1 ],
    $v1->[ 2 ] - $c->[ 2 ],
];
normalise( $cv1 );

my $cv2 = [   # vr from centre to v2
    $v2->[ 0 ] - $c->[ 0 ],
    $v2->[ 1 ] - $c->[ 1 ],
    $v2->[ 2 ] - $c->[ 2 ],
];
normalise( $cv2 );

my $cv3 = [   # vr from centre to v3
    $v3->[ 0 ] - $c->[ 0 ],
    $v3->[ 1 ] - $c->[ 1 ],
    $v3->[ 2 ] - $c->[ 2 ],
];
normalise( $cv3 );

my $cv4 = [   # vr from centre to v4
    $v4->[ 0 ] - $c->[ 0 ],
    $v4->[ 1 ] - $c->[ 1 ],
    $v4->[ 2 ] - $c->[ 2 ],
];
normalise( $cv4 );

my $cm12 = [   # vr from centre to midpoint of v1 and v2
    ( ( $v1->[ 0 ] + $v2->[ 0 ] ) / 2.0 ) - $c->[ 0 ],
    ( ( $v1->[ 1 ] + $v2->[ 1 ] ) / 2.0 ) - $c->[ 1 ],
    ( ( $v1->[ 2 ] + $v2->[ 2 ] ) / 2.0 ) - $c->[ 2 ],
];
normalise( $cm12 );

my $cm14 = [   # vr from centre to midpoint of v1 and v4
    ( ( $v1->[ 0 ] + $v4->[ 0 ] ) / 2.0 ) - $c->[ 0 ],
    ( ( $v1->[ 1 ] + $v4->[ 1 ] ) / 2.0 ) - $c->[ 1 ],
    ( ( $v1->[ 2 ] + $v4->[ 2 ] ) / 2.0 ) - $c->[ 2 ],
];
normalise( $cm14 );

my $cm32 = [   # vr from centre to midpoint of v3 and v2
    ( ( $v3->[ 0 ] + $v2->[ 0 ] ) / 2.0 ) - $c->[ 0 ],
    ( ( $v3->[ 1 ] + $v2->[ 1 ] ) / 2.0 ) - $c->[ 1 ],
    ( ( $v3->[ 2 ] + $v2->[ 2 ] ) / 2.0 ) - $c->[ 2 ],
];
normalise( $cm32 );

my $cm34 = [   # vr from centre to midpoint of v3 and v4
    ( ( $v3->[ 0 ] + $v4->[ 0 ] ) / 2.0 ) - $c->[ 0 ],
    ( ( $v3->[ 1 ] + $v4->[ 1 ] ) / 2.0 ) - $c->[ 1 ],
    ( ( $v3->[ 2 ] + $v4->[ 2 ] ) / 2.0 ) - $c->[ 2 ],
];
normalise( $cm34 );

my $h = $ns * sin( $ang );
my $w = $ns * cos( $ang );

# face 1
my $f1v1 = [
    $c->[ 0 ] + ( $h * $n->[ 0 ] ) + ( $cm14->[ 0 ] * $ns / $sqrt2 ),
    $c->[ 1 ] + ( $h * $n->[ 1 ] ) + ( $cm14->[ 1 ] * $ns / $sqrt2 ),
    $c->[ 2 ] + ( $h * $n->[ 2 ] ) + ( $cm14->[ 2 ] * $ns / $sqrt2 ),
];
my $f1v2 = [
    $c->[ 0 ] + ( $h * $n->[ 0 ] ) + ( $cm12->[ 0 ] * $ns / $sqrt2 ),
    $c->[ 1 ] + ( $h * $n->[ 1 ] ) + ( $cm12->[ 1 ] * $ns / $sqrt2 ),
    $c->[ 2 ] + ( $h * $n->[ 2 ] ) + ( $cm12->[ 2 ] * $ns / $sqrt2 ),
];
my $f1v3 = [
    $c->[ 0 ] + ( $w * $cv1->[ 0 ] ) + ( $cm12->[ 0 ] * $ns / $sqrt2 ),
    $c->[ 1 ] + ( $w * $cv1->[ 1 ] ) + ( $cm12->[ 1 ] * $ns / $sqrt2 ),
    $c->[ 2 ] + ( $w * $cv1->[ 2 ] ) + ( $cm12->[ 2 ] * $ns / $sqrt2 ),
];
my $f1v4 = [
    $c->[ 0 ] + ( $w * $cv1->[ 0 ] ) + ( $cm14->[ 0 ] * $ns / $sqrt2 ),
    $c->[ 1 ] + ( $w * $cv1->[ 1 ] ) + ( $cm14->[ 1 ] * $ns / $sqrt2 ),
    $c->[ 2 ] + ( $w * $cv1->[ 2 ] ) + ( $cm14->[ 2 ] * $ns / $sqrt2 ),
];

# face 2
my $f2v1 = [
    $c->[ 0 ] + ( $h * $n->[ 0 ] ) + ( $cm12->[ 0 ] * $ns / $sqrt2 ),
    $c->[ 1 ] + ( $h * $n->[ 1 ] ) + ( $cm12->[ 1 ] * $ns / $sqrt2 ),
    $c->[ 2 ] + ( $h * $n->[ 2 ] ) + ( $cm12->[ 2 ] * $ns / $sqrt2 ),
];
my $f2v2 = [
    $c->[ 0 ] + ( $h * $n->[ 0 ] ) + ( $cm32->[ 0 ] * $ns / $sqrt2 ),
    $c->[ 1 ] + ( $h * $n->[ 1 ] ) + ( $cm32->[ 1 ] * $ns / $sqrt2 ),
    $c->[ 2 ] + ( $h * $n->[ 2 ] ) + ( $cm32->[ 2 ] * $ns / $sqrt2 ),
];
my $f2v3 = [
    $c->[ 0 ] + ( $w * $cv2->[ 0 ] ) + ( $cm32->[ 0 ] * $ns / $sqrt2 ),
    $c->[ 1 ] + ( $w * $cv2->[ 1 ] ) + ( $cm32->[ 1 ] * $ns / $sqrt2 ),
    $c->[ 2 ] + ( $w * $cv2->[ 2 ] ) + ( $cm32->[ 2 ] * $ns / $sqrt2 ),
];
my $f2v4 = [
    $c->[ 0 ] + ( $w * $cv2->[ 0 ] ) + ( $cm12->[ 0 ] * $ns / $sqrt2 ),
    $c->[ 1 ] + ( $w * $cv2->[ 1 ] ) + ( $cm12->[ 1 ] * $ns / $sqrt2 ),
    $c->[ 2 ] + ( $w * $cv2->[ 2 ] ) + ( $cm12->[ 2 ] * $ns / $sqrt2 ),
];

# face 3
my $f3v1 = [
    $c->[ 0 ] + ( $h * $n->[ 0 ] ) + ( $cm32->[ 0 ] * $ns / $sqrt2 ),
    $c->[ 1 ] + ( $h * $n->[ 1 ] ) + ( $cm32->[ 1 ] * $ns / $sqrt2 ),
    $c->[ 2 ] + ( $h * $n->[ 2 ] ) + ( $cm32->[ 2 ] * $ns / $sqrt2 ),
];
my $f3v2 = [
    $c->[ 0 ] + ( $h * $n->[ 0 ] ) + ( $cm34->[ 0 ] * $ns / $sqrt2 ),
    $c->[ 1 ] + ( $h * $n->[ 1 ] ) + ( $cm34->[ 1 ] * $ns / $sqrt2 ),
    $c->[ 2 ] + ( $h * $n->[ 2 ] ) + ( $cm34->[ 2 ] * $ns / $sqrt2 ),
];
my $f3v3 = [
    $c->[ 0 ] + ( $w * $cv3->[ 0 ] ) + ( $cm34->[ 0 ] * $ns / $sqrt2 ),
    $c->[ 1 ] + ( $w * $cv3->[ 1 ] ) + ( $cm34->[ 1 ] * $ns / $sqrt2 ),
    $c->[ 2 ] + ( $w * $cv3->[ 2 ] ) + ( $cm34->[ 2 ] * $ns / $sqrt2 ),
];
my $f3v4 = [
    $c->[ 0 ] + ( $w * $cv3->[ 0 ] ) + ( $cm32->[ 0 ] * $ns / $sqrt2 ),
    $c->[ 1 ] + ( $w * $cv3->[ 1 ] ) + ( $cm32->[ 1 ] * $ns / $sqrt2 ),
    $c->[ 2 ] + ( $w * $cv3->[ 2 ] ) + ( $cm32->[ 2 ] * $ns / $sqrt2 ),
];

# face 4
my $f4v1 = [
    $c->[ 0 ] + ( $h * $n->[ 0 ] ) + ( $cm34->[ 0 ] * $ns / $sqrt2 ),
    $c->[ 1 ] + ( $h * $n->[ 1 ] ) + ( $cm34->[ 1 ] * $ns / $sqrt2 ),
    $c->[ 2 ] + ( $h * $n->[ 2 ] ) + ( $cm34->[ 2 ] * $ns / $sqrt2 ),
];
my $f4v2 = [
    $c->[ 0 ] + ( $h * $n->[ 0 ] ) + ( $cm14->[ 0 ] * $ns / $sqrt2 ),
    $c->[ 1 ] + ( $h * $n->[ 1 ] ) + ( $cm14->[ 1 ] * $ns / $sqrt2 ),
    $c->[ 2 ] + ( $h * $n->[ 2 ] ) + ( $cm14->[ 2 ] * $ns / $sqrt2 ),
];
my $f4v3 = [
    $c->[ 0 ] + ( $w * $cv4->[ 0 ] ) + ( $cm14->[ 0 ] * $ns / $sqrt2 ),
    $c->[ 1 ] + ( $w * $cv4->[ 1 ] ) + ( $cm14->[ 1 ] * $ns / $sqrt2 ),
    $c->[ 2 ] + ( $w * $cv4->[ 2 ] ) + ( $cm14->[ 2 ] * $ns / $sqrt2 ),
];
my $f4v4 = [
    $c->[ 0 ] + ( $w * $cv4->[ 0 ] ) + ( $cm34->[ 0 ] * $ns / $sqrt2 ),
    $c->[ 1 ] + ( $w * $cv4->[ 1 ] ) + ( $cm34->[ 1 ] * $ns / $sqrt2 ),
    $c->[ 2 ] + ( $w * $cv4->[ 2 ] ) + ( $cm34->[ 2 ] * $ns / $sqrt2 ),
];

printf "   h: %f\n", $h;
printf "   w: %f\n", $w;
printf "   c: %f %f %f\n", $c->[ 0 ], $c->[ 1 ], $c->[ 2 ];
printf "   n: %f %f %f\n", $n->[ 0 ], $n->[ 1 ], $n->[ 2 ];
printf "f1v1: %f %f %f pop moveto\n", $f1v1->[ 0 ], $f1v1->[ 1 ], $f1v1->[ 2 ];
printf "f1v2: %f %f %f pop lineto\n", $f1v2->[ 0 ], $f1v2->[ 1 ], $f1v2->[ 2 ];
printf "f1v3: %f %f %f pop lineto\n", $f1v3->[ 0 ], $f1v3->[ 1 ], $f1v3->[ 2 ];
printf "f1v4: %f %f %f pop lineto\n", $f1v4->[ 0 ], $f1v4->[ 1 ], $f1v4->[ 2 ];
printf "f2v1: %f %f %f pop moveto\n", $f2v1->[ 0 ], $f2v1->[ 1 ], $f2v1->[ 2 ];
printf "f2v2: %f %f %f pop lineto\n", $f2v2->[ 0 ], $f2v2->[ 1 ], $f2v2->[ 2 ];
printf "f2v3: %f %f %f pop lineto\n", $f2v3->[ 0 ], $f2v3->[ 1 ], $f2v3->[ 2 ];
printf "f2v4: %f %f %f pop lineto\n", $f2v4->[ 0 ], $f2v4->[ 1 ], $f2v4->[ 2 ];
printf "f3v1: %f %f %f pop moveto\n", $f3v1->[ 0 ], $f3v1->[ 1 ], $f3v1->[ 2 ];
printf "f3v2: %f %f %f pop lineto\n", $f3v2->[ 0 ], $f3v2->[ 1 ], $f3v2->[ 2 ];
printf "f3v3: %f %f %f pop lineto\n", $f3v3->[ 0 ], $f3v3->[ 1 ], $f3v3->[ 2 ];
printf "f3v4: %f %f %f pop lineto\n", $f3v4->[ 0 ], $f3v4->[ 1 ], $f3v4->[ 2 ];
printf "f4v1: %f %f %f pop moveto\n", $f4v1->[ 0 ], $f4v1->[ 1 ], $f4v1->[ 2 ];
printf "f4v2: %f %f %f pop lineto\n", $f4v2->[ 0 ], $f4v2->[ 1 ], $f4v2->[ 2 ];
printf "f4v3: %f %f %f pop lineto\n", $f4v3->[ 0 ], $f4v3->[ 1 ], $f4v3->[ 2 ];
printf "f4v4: %f %f %f pop lineto\n", $f4v4->[ 0 ], $f4v4->[ 1 ], $f4v4->[ 2 ];

exit;

sub normalise {

    my $vr = shift;

    my $len = sqrt (
            ( $vr->[ 0 ] * $vr->[ 0 ] ) +
            ( $vr->[ 1 ] * $vr->[ 1 ] ) +
            ( $vr->[ 2 ] * $vr->[ 2 ] )
    );

    $vr->[ 0 ] = $vr->[ 0 ] / $len;
    $vr->[ 1 ] = $vr->[ 1 ] / $len;
    $vr->[ 2 ] = $vr->[ 2 ] / $len;
}
