#!/usr/bin/perl
 
use warnings;
use strict;
 
my $loop_max = [ 0, 4 ];
my $sample_max = 4;
 
sub displayLoops {
 
    my $count = shift;
 
    while ( $count > $sample_max ) {
 
        my $new_max = 2 * $loop_max->[ -1 ];
        push @{ $loop_max }, $new_max;
        $sample_max += $new_max;
        printf "new max:  $new_max\n";
    }
 
    # the outermost loop must have as many entries as the
    # previous loops' max
    my $loop_pnt = scalar( @{ $loop_max } ) - 1;
 
    printf "$count = ";
    while ( ( $count > 0 ) && ( $loop_pnt > 0 ) ) {
   
        my $current_loop_count = $loop_max->[ $loop_pnt - 1 ];
        $count -= $current_loop_count;
 
        while (
            ( $count > 0 ) &&
            ( $current_loop_count < $loop_max->[ $loop_pnt ] )
        ) {
            $current_loop_count++;
            $count--;
        }
 
        printf "%d + ", $current_loop_count;
 
        $loop_pnt--;
    }
 
    printf " 0\n";
}
 
displayLoops( $ARGV[0] );
 
#for( my $a = 1; $a < 40; $a++ ) { displayLoops( $a ) }
 
__END__
 
            4   8   16
            00001   1   1
            00010   2   2
            00011   3   3
            00100   4   4
            00101   5   0 + 5
            00110   6   0 + 6
            00111   7   0 + 7
            01000   8   0 + 8
            01001   9   1 + 8
            01010  10   2 + 8
            01011  11   3 + 8
            01100  12   4 + 8
            01101  13   0 + 4 + 9
            01110  14   0 + 4 + 10
            01111  15   0 + 4 + 11
            10000  16   0 + 4 + 12
            10001  17   0 + 4 + 13
            10010  18   0 + 4 + 14
            10011  19   0 + 4 + 15
            10100  20   0 + 4 + 16
            10101  21   0 + 5 + 16
            10110  22   0 + 6 + 16
            10111  23   0 + 7 + 16
            11000  24   0 + 8 + 16
            11001  25   1 + 8 + 16
            11010  26   2 + 8 + 16
            11011  27   3 + 8 + 16
            11100  28   4 + 8 + 16
